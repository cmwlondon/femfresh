<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Meta Data All in One Place
    |--------------------------------------------------------------------------
    */

    '/' => [
        'title' => 'femfresh | pH perfect intimate skincare',
        'desc'  => 'femfresh feminine washes and wipes are pH balanced to the unique pH level of intimate vulval skin.',
        'image' => 'femfresh.jpg',
        'pagetype' => 'website',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'feminine hygiene, feminine products, intimate skincare, pH balanced, what is femfresh',
        'indexable' => true
    ],

    'our-range' => [
        'title' => 'femfresh products are pH balanced to care for intimate skin',
        'desc'  => 'femfresh washes and feminine wipes are perfectly balanced to care for the unique pH of intimate skin.',
        'image' => 'femfresh.jpg',
        'pagetype' => 'product.group',
        'keywords' => 'femfresh,intimate,skincare,wipes,wash,hygiene,thrush,cystitis,feminine,pH-balance',
        'keyphrases' => 'femfresh wash, femfresh wipes, best feminine hygiene, feminine products, femfresh products',
        'indexable' => true
    ],

    'our-range/daily-intimate-wash' => [
        'title' => 'femfresh products | daily intimate wash',
        'desc'  => 'femfresh daily intimate wash has a unique formula that\'s pH balanced for intimate skin.',
        'image' => 'daily-intimate-wash.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'femfresh washes, female intimate wash, feminine hygiene wash, intimate wash, femfresh review',
        'indexable' => true
    ],

    'our-range/ultimate-care-pure-wash' => [
        'title' => 'femfresh products | ultimate care pure wash',
        'desc'  => 'femfresh pure intimate wash has a pH balance that\'s specially designed to care for intimate skin.',
        'image' => 'ultimate-care-pure-wash.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'femfresh washes, female intimate wash, feminine hygiene wash, intimate wash, femfresh review',
        'indexable' => true
    ],

    'our-range/ultimate-care-active-wash' => [
        'title' => 'femfresh products | ultimate care active wash',
        'desc'  => 'femfresh active intimate wash is pH balanced to care for intimate vulval skin and perfect for post-workout intimate cleansing.',
        'image' => 'ultimate-care-active-wash.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'femfresh washes, female intimate wash, feminine hygiene wash, intimate wash, femfresh review',
        'indexable' => true
    ],

    'our-range/ultimate-care-soothing-wash' => [
        'title' => 'femfresh products | ultimate care soothing wash',
        'desc'  => 'femfresh soothing intimate wash is pH balanced for intimate skin, unlike some soaps and shower gels which can upset the natural pH of vulval skin.',
        'image' => 'ultimate-care-soothing-wash.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'femfresh washes, female intimate wash, feminine hygiene wash, intimate wash, femfresh review',
        'indexable' => true
    ],

    'our-range/freshening-and-soothing-wipes' => [
        'title' => 'femfresh products | freshening and soothing wipes',
        'desc'  => 'femfresh intimate wipes are specially designed to care for the pH of intimate skin on-the-go.',
        'image' => 'freshening-and-soothing-wipes.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'femfresh wipes, femfresh wipe, intimate wipes, femfresh review, feminine hygiene',
        'indexable' => true
    ],

    'our-range/ultimate-care-active-fresh-deodorant' => [
        'title' => 'femfresh products | ultimate care active fresh deodorant',
        'desc'  => 'femfresh deodorant is a pH balanced feminine deodorant that\'s specially designed to care for intimate skin.',
        'image' => 'ultimate-care-active-fresh-deodorant.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'femfresh review, feminine hygiene products, best feminine hygiene, femfresh products, intimate skincare',
        'indexable' => true
    ],

    'our-range/freshness-deodorant' => [
        'title' => 'femfresh products | freshness deodorant',
        'desc'  => 'femfresh deodorant is a pH balanced feminine deodorant that\'s specially designed to care for intimate skin.',
        'image' => 'freshness-deodorant.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,cystitis,feminine,pH-balance,thrush,hygiene',
        'keyphrases' => 'femfresh review, feminine hygiene products, best feminine hygiene, femfresh products, intimate skincare',
        'indexable' => true
    ],

    'our-range/2-in-1-shower-and-shave-cream' => [
        'title' => 'femfresh products | 2in1 shower and shave cream',
        'desc'  => 'femfresh intimate shave cream is balanced to the unique pH level of intimate skin and is designed to use when shaving pubic hair.',
        'image' => '2-in-1-shower-and-shave-cream.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,shaving,vaginal,feminine,pH-balance,hygiene,razor,epilator',
        'keyphrases' => 'femfresh review, feminine hygiene products, best feminine hygiene, femfresh products, intimate shaving',
        'indexable' => true
    ],

    'our-range/post-shave-balm' => [
        'title' => 'femfresh products | post shave balm',
        'desc'  => 'femfresh intimate post shave balm is balanced to the unique pH level of intimate skin and is designed to use to soothe skin after shaving pubic hair.',
        'image' => 'post-shave-balm.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,shaving,vaginal,feminine,pH-balance,hygiene,razor,wax',
        'keyphrases' => 'femfresh review, feminine hygiene products, best feminine hygiene, femfresh products, intimate shaving',
        'indexable' => true
    ],

    'our-range/re-balance-powder' => [
        'title' => 'femfresh products | re-balance powder',
        'desc'  => 'femfresh intimate powder has a talc-free formula and is pH balanced for use on intimate skin.',
        'image' => 're-balance-powder.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,feminine,pH-balance,hygiene,talc',
        'keyphrases' => 'femfresh review, feminine hygiene products, best feminine hygiene, femfresh products, intimate skincare',
        'indexable' => true
    ],

    'our-range/body-wash' => [
        'title' => 'femfresh products | 0% wash',
        'desc'  => 'Give your intimate skin the gentle loving care it deserves, with femfresh 0% wash.',
        'image' => 'body-wash.jpg',
        'pagetype' => 'product',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,feminine,pH-balance,hygiene,talc',
        'keyphrases' => 'femfresh review, feminine hygiene products, best feminine hygiene, femfresh products, intimate skincare',
        'indexable' => true
    ],

    'about-femfresh' => [
        'title' => 'about femfresh | pH balanced washes and wipes',
        'desc'  => 'femfresh is a collection of pH balanced washes and wipes designed to care for the unique pH of vulval skin.',
        'image' => 'femfresh.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,feminine,pH-balance,hygiene,wipes,washes',
        'keyphrases' => 'femfresh products, female intimate health, best feminine hygiene, feminine products, what is femfresh',
        'indexable' => true
    ],

    'knowledge-hub' => [
        'title' => 'Everything you need to know about the vulva and vagina',
        'desc'  => 'In the femfresh knowledge hub you\'ll find all you need to know about the intimate area - from intimate anatomy to expert gynaecologist advice.',
        'image' => 'femfresh.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,feminine,pH-balance,hygiene,wipes,washes',
        'keyphrases' => 'femfresh products, female intimate health, intimate skin, vaginal health, vulva care',
        'indexable' => true
    ],

    'knowledge-hub/gynae-advice' => [
        'title' => 'Gynaecologist Dr Sara Matthews, answers your top questions on female intimate health',
        'desc'  => 'What is femfresh? What is pH balance? Vaginal douching? Follow advice and tips from the femfresh in-house gynaecologist.',
        'image' => 'dr-sara.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,feminine,pH-balance,hygiene,gynaecologist,dermatologist',
        'keyphrases' => 'vaginal health, vaginal pH-balance, female intimate health, gynaecologist advice, vagina expert',
        'indexable' => true
    ],

    'knowledge-hub/interactive-quiz' => [
        'title' => 'Do you know your vulva from your vagina?',
        'desc'  => 'Take the femfresh intimate anatomy quiz to test how much you really know about your intimate parts.',
        'image' => 'question-mark.jpg',
        'pagetype' => 'website',
        'keywords' => 'femfresh,intimate,skincare,cleansing,vaginal,feminine,pH-balance,hygiene,anatomy,gynaecologist,vulva',
        'keyphrases' => 'vaginal health, female intimate health, feminine hygiene pregnancy, intimate skincare, feminine hygiene',
        'indexable' => true
    ],

    'knowledge-hub/article/irritation-down-there' => [
        'title' => 'femfresh article | intimate irritation',
        'desc'  => 'Vaginal dryness or vaginal irritation and itching isn\'t always the sign of a serious intimate problem. Soothing intimate skin can be as simple as switching to an intimate wash.',
        'image' => 'irritation-down-there.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,articles,shaving,periods',
        'keyphrases' => 'female intimate health, feminine care advice, vagina irritation, thrush symptoms, vagina dryness and itching',
        'indexable' => true
    ],

    'knowledge-hub/article/periods-dos-and-donts' => [
        'title' => 'femfresh article | period myths',
        'desc'  => '"You can\'t get pregnant on your period" - everyone has heard at least one period myth and here we bust three of the top myths to help sort fact from fiction.',
        'image' => 'periods-dos-and-donts.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,articles,shaving,periods',
        'keyphrases' => 'female intimate health, feminine care advice, vagina irritation, thrush symptoms, vagina dryness and itching',
        'indexable' => true
    ],

    'knowledge-hub/article/intimate-sweating-no-sweat' => [
        'title' => 'femfresh article | intimate sweating',
        'desc'  => 'Intimate sweating is perfectly normal and the femfresh washes are the pH-perfect partner for your post-gym shower.',
        'image' => 'intimate-sweating-no-sweat.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,articles,shaving,periods',
        'keyphrases' => 'female intimate health, feminine care advice, vagina irritation, thrush symptoms, vagina dryness and itching',
        'indexable' => true
    ],

    'knowledge-hub/article/pubic-hair-to-shave-or-not-to-shave' => [
        'title' => 'femfresh article | shaving and pubic hair',
        'desc'  => 'It\'s completly up to you if you choose to shape your pubic hair or leave it natural. If you shave your pubic hair then pH balanced products are best suited for the skin down there as it has a different pH level to the rest of your body.',
        'image' => 'pubic-hair-to-shave-or-not-to-shave.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,articles,shaving,pubes',
        'keyphrases' => 'intimate shaving, pubic hair, waxing bikini line, shaving pubes, intimate grooming',
        'indexable' => true
    ],

    'knowledge-hub/article/some-informational-bits-about-your-intimate-bits' => [
        'title' => 'femfresh article | feminine hygiene routine',
        'desc'  => 'Sex, peeing, sweat and what we wash with can all upset the natural balance of intimate skin. Follow these tips to keep your intimate skin pH balanced and happy.',
        'image' => 'some-informational-bits-about-your-intimate-bits.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,articles,shaving,periods',
        'keyphrases' => 'female intimate health, feminine care advice, vagina irritation, thrush symptoms, vagina dryness and itching',
        'indexable' => true
    ],

    'knowledge-hub/article/intimate-wipes-vs-baby-wipes' => [
        'title' => 'femfresh article | why use intimate wipes?',
        'desc'  => 'If you\'re going to use a wipe on your intimate area, try to use one that\'s balanced to the unique pH of vulval skin.',
        'image' => 'intimate-wipes-vs-baby-wipes.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,articles,shaving,periods',
        'keyphrases' => 'female intimate health, feminine care advice, vagina irritation, thrush symptoms, vagina dryness and itching',
        'indexable' => true
    ],

    'knowledge-hub/about-ph-balance' => [
        'title' => 'Why should we care about pH balance?',
        'desc'  => 'Dermatologist Dr Clare Pattersson explains why pH balance is worth thinking about when it comes to female intimate health.',
        'image' => 'dr-clare.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,pH-balance,expert,dermatologist',
        'keyphrases' => 'female intimate health, dermatologist advice, derma skin advice, intimate skincare, pH balance',
        'indexable' => true
    ],

    'contact' => [
        'title' => 'Contact the femfresh team',
        'desc'  => 'Got a question about femfresh? Get in touch via our online enquiry or call our care team on 0800 121 6080.',
        'image' => 'femfresh.jpg',
        'pagetype' => 'website',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,pH-balance,contact,email',
        'keyphrases' => 'femfresh website, get in touch with femfresh, contact femfresh, femfresh product query',
        'indexable' => true
    ],

    'knowledge-hub/faqs' => [
        'title' => 'femfresh | frequently asked questions',
        'desc'  => 'What is femfresh? Why would I use an intimate wipe? What is douching? We answer your must know questions about femfresh and intimate skincare.',
        'image' => 'femfresh.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,pH-balance,expert,faqs',
        'keyphrases' => 'female intimate health, what is femfresh?, femfresh hygiene products, best feminine hygiene, femfresh questions',
        'indexable' => true
    ],

    'knowledge-hub/she-talks' => [
        'title' => 'femfresh She Talks video series',
        'desc'  => 'Watch the new SheTalks video series as Nush Cope gets intimate with the intimate area.',
        'image' => 'she-talks.jpg',
        'pagetype' => 'article',
        'keywords' => 'femfresh,intimate,pH-balance,feminine,sex,periods,grooming,vaginal,vulva,video',
        'keyphrases' => 'femfresh video, chessie kingg, em sheldon, twice the health',
        'indexable' => true
    ],

    'terms-and-conditions' => [
        'title' => 'femfresh | website terms and conditions',
        'desc'  => 'Terms and conditions for the femfresh.co.uk website',
        'image' => 'femfresh.jpg',
        'pagetype' => 'website',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,pH-balance,expert,faqs',
        'keyphrases' => 'femfresh website, website terms and conditions, ',
        'indexable' => true
    ],

    'privacy-policy' => [
        'title' => 'femfresh | website privacy policy',
        'desc'  => 'Privacy policy for the femfresh.co.uk website',
        'image' => 'femfresh.jpg',
        'pagetype' => 'website',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,pH-balance',
        'keyphrases' => 'femfresh website, website privacy policy',
        'indexable' => true
    ],

    'cookie-notice' => [
        'title' => 'femfresh | website terms and conditions',
        'desc'  => 'Cookie notice for the femfresh.co.uk website',
        'image' => 'femfresh.jpg',
        'pagetype' => 'website',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,pH-balance',
        'keyphrases' => 'femfresh website, website cookie notice',
        'indexable' => true
    ],

    'third-party-information-collection' => [
        'title' => 'femfresh | third party information collection',
        'desc'  => 'Third party information collection for the femfresh.co.uk website',
        'image' => 'femfresh.jpg',
        'pagetype' => 'website',
        'keywords' => 'femfresh,intimate,cystitis,cleansing,vaginal,feminine,hygiene,pH-balance',
        'keyphrases' => 'femfresh website, website third party information collection',
        'indexable' => false
    ]

];
