<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Article Data All in One Place
    |--------------------------------------------------------------------------
    */

    'irritation-down-there' => [
        'title' => 'Irritation down there?',
        'main_menu' => 'Irritation<br/>down there?',
        'mobile_menu' => 'Irritation down there?',
        'image' => 'irritation-down-there.jpg'
    ],
    'periods-dos-and-donts' => [
        'title' => 'Periods - dos and donts',
        'main_menu' => 'Periods - dos<br/>and donts',
        'mobile_menu' => 'Periods - dos and donts',
        'image' => 'periods-dos-and-donts.jpg'
    ],
    'intimate-sweating-no-sweat' => [
        'title' => 'Intimate sweating? No sweat!',
        'main_menu' => 'Intimate sweating?<br/>No sweat!',
        'mobile_menu' => 'Intimate sweating? No sweat!',
        'image' => 'intimate-sweating.jpg'
    ],
    'pubic-hair-to-shave-or-not-to-shave' => [
        'title' => 'Pubic hair - to shave or not to shave?',
        'main_menu' => 'Pubic hair - to shave<br/>or not to shave?',
        'mobile_menu' => 'Pubic hair - to shave or not to shave?',
        'image' => 'pubic-hair.jpg'
    ],
    'some-informational-bits-about-your-intimate-bits' => [
        'title' => 'Some informational bits about your intimate bits',
        'main_menu' => 'Some informational bits<br/>about your intimate bits',
        'mobile_menu' => 'Some informational bits about your intimate bits',
        'image' => 'informational-bits.jpg'
    ],
    'intimate-wipes-vs-baby-wipes' => [
        'title' => 'Intimate wipes vs baby wipes',
        'main_menu' => 'Intimate wipes<br/>vs baby wipes',
        'mobile_menu' => 'Intimate wipes vs baby wipes',
        'image' => 'intimate-wipes-vs-baby-wipes.jpg'
    ]
];
