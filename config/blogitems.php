<?php
/*
expert advice carousel items /expert-advice

expert advice index page: article/video carousel -> //resources/view/main/components/_dr_jackies_expert_advice.blade.php

controller: //app/Http/Controllers/Main/PageController.php @ article

article page
article templates: (//resources/view/main/knowledge/blog/*.blade.php)
url -> blade name 
/knowledge-hub/[array key] -> //resources/view/main/knowledge/blog/[array key].blade.php

for example:
/knowledge-hub/period-care -> //resources/view/main/knowledge/blog/period-care.blade.php

images: //public/images/blog/

video: //public/video/expert-advice/

url/id => [
    'type' => 'article' / 'video'

    type = article:
    'title' => <string> carousel item HTML caption **NOT <ttile></title> content**

    'main_menu' => <string>main menu title (not used at the  moment)
    'mobile_menu' => <string>mobile menu title (not used at the  moment)
    'thumbnail' => <string> /images/blog/[name]-thumbnail.jpg' image displayed in carousel
    'image' => <string> '/images/blog/[name]-image.jpg' image displayed in article page

    type = video:
    'title' => <string> title displayed in carousel and on video popup
    'thumbnail' => <string> '/images/blog/[name]-video-thumbnail.jpg' image displayed in carousel
    'mp4' => <string> '/video/expert-advice/[name].mp4 video file to play in popup
    'poster' => <stribg> '/images/blog/[name]-video-poster.jpg' video popup poster image
] 

video items open in-page pop-up video player

article items open new blog page - array key is the page 'slug' and blade template name
*/
/*updated 13th May 2021 */
return [
    '5-common-feminine-hygiene-myths' => [
        'type' => 'article',
        'title' => '5 Common feminine hygiene myths',
        'meta' => [
            'title' => '5 Common feminine hygiene myths',
            'desc' => '5 Common feminine hygiene myths',
            'keywords' => 'Common feminine hygiene myths,expert,advice',
            'keyphrases' => '5 common feminine hygiene myths, expert advice'
        ],
        'main_menu' => '5 Common feminine hygiene myths',
        'mobile_menu' => '5 Common feminine hygiene myths',
        'thumbnail' => '/images/blog/common-feminine-hygiene-myths-thumbnail.jpg',
        'image' => '/images/blog/common-feminine-hygiene-myths-image.jpg'
    ],
    'smear-tests-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Smear Tests',
        'thumbnail' => '/images/blog/smear-tests-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/stream-test-1kbps.mp4',
        'poster' => '/images/blog/smear-tests-video-poster.jpg'
    ],
    'dont-fear-the-smear' => [
        'type' => 'article',
        'title' => 'Don’t fear the smear!',
        'meta' => [
            'title' => 'Don’t fear the smear!',
            'desc' => 'Don’t fear the smear!',
            'keywords' => 'smear,expert,advice',
            'keyphrases' => 'smear test, expert advice'
        ],
        'main_menu' => 'Don’t fear the smear!',
        'mobile_menu' => 'Don’t fear the smear!',
        'thumbnail' => '/images/blog/dont-fear-the-smear-thumbnail.jpg',
        'image' => '/images/blog/dont-fear-the-smear-image.jpg'
    ],
    'sti-myths-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on STIs',
        'thumbnail' => '/images/blog/sti-myths-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/sti-myths-video-1kbps.mp4',
        'poster' => '/images/blog/sti-myths-video-poster.jpg'
    ],
    'busting-sti-myths' => [
        'type' => 'article',
        'title' => 'Busting Myths on STI\'s',
        'meta' => [
            'title' => 'Busting Myths on STI\'s',
            'desc' => 'Dr Frankie on Busting Myths on STI\'s',
            'keywords' => 'sti,myths,expert,advice',
            'keyphrases' => 'busting sti myths,expert advice'
        ],
        'main_menu' => 'Busting Myths on STI\'s',
        'mobile_menu' => 'Busting Myths on STI\'s',
        'thumbnail' => '/images/blog/busting-sti-myths-thumbnail.jpg',
        'image' => '/images/blog/busting-sti-myths-image.jpg'
    ],
    'xitem-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Vaginal Odour',
        'thumbnail' => '/images/blog/xitem-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/xitem-video-1kbps.mp4',
        'poster' => '/images/blog/xitem-video-poster.jpg'
    ],
    'vaginal-odour-and-bacterial-vaginosis' => [
        'type' => 'article',
        'title' => 'Vaginal Odour &amp; Bacterial Vaginosis',
        'meta' => [
            'title' => 'Vaginal Odour & Bacterial Vaginosis',
            'desc' => 'Vaginal Odour & Bacterial Vaginosis',
            'keywords' => 'vaginal,odour,bacterial,vaginosis,expert,advice',
            'keyphrases' => 'vaginal odour,bacterial vaginosis,expert advice'
        ],
        'main_menu' => 'Vaginal Odour & Bacterial Vaginosis',
        'mobile_menu' => 'Vaginal Odour & Bacterial Vaginosis',
        'thumbnail' => '/images/blog/bacterial-vaginosis-thumbnail.jpg',
        'image' => '/images/blog/bacterial-vaginosis-image.jpg'
    ],
    'vulvodynia-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Vulvodynia',
        'thumbnail' => '/images/blog/vulvodynia-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/vulvodynia-video-1kbps.mp4',
        'poster' => '/images/blog/vulvodynia-video-poster.jpg'
    ],
    'the-truth-around-vulvodynia' => [
        'type' => 'article',
        'title' => 'The truth around vulvodynia',
        'meta' => [
            'title' => 'The truth around vulvodynia',
            'desc' => 'The truth around vulvodynia',
            'keywords' => 'truth, vulvodynia',
            'keyphrases' => 'truth around vulvodynia'
        ],
        'main_menu' => 'The truth around vulvodynia',
        'mobile_menu' => 'The truth around vulvodynia',
        'thumbnail' => '/images/blog/vulvodynia1-thumbnail.jpg',
        'image' => '/images/blog/vulvodynia1-image.jpg'
    ],
    'uti-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on UTIs',
        'thumbnail' => '/images/blog/uti-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/utis-1kbps.mp4',
        'poster' => '/images/blog/uti-poster.jpg'
    ],
    'myths-surrounding-utis' => [
        'type' => 'article',
        'title' => 'Myths surrounding UTIs',
        'meta' =>[
            'title' => 'Myths surrounding UTIs',
            'desc' => 'Myths surrounding UTIs',
            'keywords' => 'Myths, UTIs',
            'keyphrases' => 'Myths surrounding UTIs'
        ],
        'main_menu' => 'Myths surrounding UTIs',
        'mobile_menu' => 'Myths surrounding UTIs',
        'thumbnail' => '/images/blog/myths-thumbnail.jpg',
        'image' => '/images/blog/myths-image.jpg'
    ],

    'intimate-care-after-sex' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Intimate care after sex',
        'thumbnail' => '/images/blog/intimate-care-after-sex-thumbnail.jpg',
        'mp4' => '/video/expert-advice/intimate-care-after-sex-1kbps.mp4',
        'poster' => '/images/blog/intimate-care-after-sex-poster.jpg'
    ],

    'intimate-care-and-sex' => [
        'type' => 'article',
        'title' => 'Intimate care and sex',
        'meta' =>[
            'title' => 'Intimate care and sex',
            'desc' => 'Intimate care and sex',
            'keywords' => 'Intimate care, sex',
            'keyphrases' => 'Intimate care and sex'
        ],
        'main_menu' => 'Intimate care and sex',
        'mobile_menu' => 'Intimate care and sex',
        'thumbnail' => '/images/blog/icsex-thumbnail.jpg',
        'image' => '/images/blog/icsex-image.jpg'
    ],

    'period-care-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Period Care',
        'thumbnail' => '/images/blog/period-care-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/period-care-video-1kbps.mp4',
        'poster' => '/images/blog/period-care-video-poster.jpg'
    ],
    
    'period-care' => [
        'type' => 'article',
        'title' => 'Period care 101',
        'meta' =>[
            'title' => 'Period care 101',
            'desc' => 'Period care 101',
            'keywords' => 'Period care',
            'keyphrases' => 'Period care 101'
        ],
        'main_menu' => 'Period care 101',
        'mobile_menu' => 'Period care 101',
        'thumbnail' => '/images/blog/peca-thumbnail.jpg',
        'image' => '/images/blog/peca-image.jpg'
    ],

    'vaginal-odour-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Vaginal Odour',
        'meta' =>[
            'title' => 'Dr Frankie on Vaginal Odour',
            'desc' => 'Dr Frankie on Vaginal Odour',
            'keywords' => 'vaginal,odour',
            'keyphrases' => 'vaginal odour'
        ],
        'thumbnail' => '/images/blog/vaginal-odour-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/vaginal-odour-video-1kbps.mp4',
        'poster' => '/images/blog/vaginal-odour-video-poster.jpg'
    ],

    'vaginal-odour' => [
        'type' => 'article',
        'title' => 'The Basics on Vaginal Odour',
        'meta' =>[
            'title' => 'The Basics on Vaginal Odour',
            'desc' => 'The Basics on Vaginal Odour',
            'keywords' => 'vaginal odour',
            'keyphrases' => 'basics on vaginal odour'
        ],
        'main_menu' => 'The Basics on Vaginal Odour',
        'mobile_menu' => 'The Basics on Vaginal Odour',
        'thumbnail' => '/images/blog/vaod-thumbnail.jpg',
        'image' => '/images/blog/vaod-image.jpg'
    ],

    'vaginal-dryness' => [
        'type' => 'article',
        'title' => 'The Basics on Vaginal Dryness',
        'meta' =>[
            'title' => 'The Basics on Vaginal Dryness',
            'desc' => 'The Basics on Vaginal Dryness',
            'keywords' => 'vaginal dryness',
            'keyphrases' => 'vaginal dryness'
        ],
        'main_menu' => 'The Basics on Vaginal Dryness',
        'mobile_menu' => 'The Basics on Vaginal Dryness',
        'thumbnail' => '/images/blog/vadr-thumbnail.jpg',
        'image' => '/images/blog/vadr-image.jpg'
    ],

    'menopause-video' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Menopause',
        'thumbnail' => '/images/blog/menopause-video-thumbnail.jpg',
        'mp4' => '/video/expert-advice/menopause-video-1kbps.mp4',
        'poster' => '/images/blog/menopause-video-poster.jpg'
    ],
    
    'menopause' => [
        'type' => 'article',
        'title' => 'Menopause and your intimate area',
        'meta' =>[
            'title' => 'Menopause and your intimate area',
            'desc' => 'Menopause and your intimate area',
            'keywords' => 'menopause,intimate area',
            'keyphrases' => 'menopause and your intimate area'
        ],
        'main_menu' => 'Menopause and your intimate area',
        'mobile_menu' => 'Menopause and your intimate area',
        'thumbnail' => '/images/blog/menopause-thumbnail.jpg',
        'image' => '/images/blog/menopause-image.jpg'
    ],

    'microbiome' => [
        'type' => 'article',
        'title' => 'What is Skin Microbiome?',
        'meta' =>[
            'title' => 'What is Skin Microbiome?',
            'desc' => 'What is Skin Microbiome?',
            'keywords' => 'skin,microbiome',
            'keyphrases' => 'skin microbiome'
        ],
        'main_menu' => 'What is Skin Microbiome?',
        'mobile_menu' => 'What is Skin Microbiome?',
        'thumbnail' => '/images/blog/microbiome-thumbnail.jpg',
        'image' => '/images/blog/microbiome-image.jpg'
    ],

    'ui2' => [
        'type' => 'video',
        'title' => 'Dr Frankie on Urinary Incontinence',
        'thumbnail' => '/images/blog/ui2-thumbnail.jpg',
        'mp4' => '/video/expert-advice/ui2-1kbps.mp4',
        'poster' => '/images/blog/ui2-poster.jpg'
    ],

    'urinary-incontinence' => [
        'type' => 'article',
        'title' => 'Urinary Incontinence? More normal than you think',
        'meta' =>[
            'title' => 'Urinary Incontinence? More normal than you think',
            'desc' => 'Urinary Incontinence',
            'keywords' => 'urinary,incontinence',
            'keyphrases' => 'urinary incontinence'
        ],
        'main_menu' => 'Urinary Incontinence? More normal than you think',
        'mobile_menu' => 'Urinary Incontinence? More normal than you think',
        'thumbnail' => '/images/blog/ui-thumb.jpg',
        'image' => '/images/blog/ui-image.jpg'
    ],

    'vid1' => [
        'type' => 'video',
        'title' => 'Dr Frankie on pH Balance',
        'thumbnail' => '/images/blog/dr-frankie-poster2.jpg',
        'mp4' => '/video/expert-advice/dr-frankie-ph-balance_512bps.mp4',
        'poster' => '/images/blog/ph-balance-poster-portrait.jpg'
    ],

    'vid2' => [
        'type' => 'video',
        'title' => 'Dr Frankie on intimate hygiene',
        'thumbnail' => '/images/blog/dr-frankie-poster1.jpg',
        'mp4' => '/video/expert-advice/dr-frankie.mp4',
        'poster' => '/images/blog/dr-frankie-poster-portrait.jpg'
    ],

    'ph-balance' => [
        'type' => 'article',
        'title' => 'pH Balance on Intimate Skin',
        'meta' =>[
            'title' => 'pH Balance on Intimate Skin',
            'desc' => 'pH Balance on Intimate Skin',
            'keywords' => 'ph,balance,intimate,skin',
            'keyphrases' => 'pH Balance on Intimate Skin'
        ],
        'main_menu' => 'pH Balance on Intimate Skin',
        'mobile_menu' => 'pH Balance on Intimate Skin',
        'thumbnail' => '/images/blog/ph-balance-thumb.jpg',
        'image' => '/images/blog/image3.jpog'
    ],

    'vagina-dialogue' => [                                                        
        'type' => 'article',
        'title' => 'The Vagina Dialogue: Keeping your Lady Garden fresh',
        'meta' =>[
            'title' => 'The Vagina Dialogue: Keeping your Lady Garden fresh',
            'desc' => 'The Vagina Dialogue: Keeping your Lady Garden fresh',
            'keywords' => 'vagina,fresh',
            'keyphrases' => 'Keeping your Lady Garden fresh'
        ],
        'main_menu' => 'The Vagina Dialogue: Keeping your Lady Garden fresh',
        'mobile_menu' => 'The Vagina Dialogue: Keeping your Lady Garden fresh',
        'thumbnail' => '/images/blog/thumb1.jpg',
        'image' => '/images/blog/image1.jpog'
    ],

    'cleansing' => [
        'type' => 'article',
        'title' => 'Cleansing your Vagina 101',
        'meta' =>[
            'title' => 'Cleansing your Vagina 101',
            'desc' => 'Cleansing your Vagina 101',
            'keywords' => 'cleansing,vagina',
            'keyphrases' => 'cleansing your vagina'
        ],
        'main_menu' => 'Cleansing your Vagina 101',
        'mobile_menu' => 'Cleansing your Vagina 101',
        'thumbnail' => '/images/blog/thumb2.jpg',
        'image' => '/images/blog/image2.jpog'
    ],

    'listening' => [
        'type' => 'article',
        'title' => 'Listening to your vagina',
        'meta' =>[
            'title' => 'Listening to your vagina',
            'desc' => 'Listening to your vagina',
            'keywords' => 'vagina',
            'keyphrases' => 'listening to your vagina'
        ],
        'main_menu' => 'Listening to your vagina',
        'mobile_menu' => 'Listening to your vagina',
        'thumbnail' => '/images/blog/thumb3.jpg',
        'image' => '/images/blog/image3.jpog'
    ]
];
