<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Main\PageController@index')->name('home');

Route::group(['prefix' => 'our-range'], function () {
	Route::get('/', 'Main\PageController@range')->name('range');

	Route::get('/daily-intimate-wash', 'Main\PageController@range')->name('daily-intimate-wash');
	Route::get('/body-wash', 'Main\PageController@range')->name('body-wash');
	// Route::get('/ultimate-care-pure-wash', 'Main\PageController@range')->name('ultimate-care-pure-wash');
	Route::get('/ultimate-care-active-wash', 'Main\PageController@range')->name('ultimate-care-active-wash');
	Route::get('/ultimate-care-soothing-wash', 'Main\PageController@range')->name('ultimate-care-soothing-wash');
	Route::get('/daily-wipes', 'Main\PageController@range')->name('25-wipes');
	Route::get('/0percent-sensitive-wipes', 'Main\PageController@range')->name('10-wipes');
	//Route::get('/10-freshening-and-soothing-wipes', 'Main\PageController@range')->name('10-wipes');
	Route::get('/ultimate-care-active-fresh-deodorant', 'Main\PageController@range')->name('ultimate-care-active-fresh-deodorant');
	Route::get('/freshness-deodorant', 'Main\PageController@range')->name('freshness-deodorant');

	/*
	// shaving range
	Route::get('/2-in-1-shower-and-shave-cream', 'Main\PageController@range')->name('2-in-1-shower-shave-cream');
	
	Route::get('/post-shave-balm', 'Main\PageController@range')->name('post-shave-balm');
	*/
	
	Route::get('/re-balance-powder', 'Main\PageController@range')->name('re-balance-powder');

	Route::get('/external-comfort-gel', 'Main\PageController@range')->name('external-comfort-gel');
	Route::get('/odour-eliminating-gel', 'Main\PageController@range')->name('odour-eliminating-gel');
});

Route::get('/about-femfresh', 'Main\PageController@about')->name('about');

Route::group(['prefix' => 'knowledge-hub'], function () {
	Route::get('/', 'Main\PageController@knowledge')->name('knowledge');
	Route::get('/interactive-quiz', 'Main\PageController@knowledge')->name('quiz');
	//Route::get('/articles', 'Main\PageController@knowledge')->name('articles');
	Route::get('/about-ph-balance', 'Main\PageController@knowledge')->name('about-ph-balance');

	Route::get('/article/{slug}', 'Main\PageController@article')->name('article');
	// pubic-hair-to-shave-or-not-to-shave
	// Route::get('/faqs', 'Main\PageController@knowledge')->name('faqs');

	Route::get('/she-talks', 'Main\PageController@knowledge')->name('she-talks');
	Route::get('/she-talks', 'Main\PageController@knowledge')->name('videos'); // removing this route breaks the site

	// expert advice blog post parameter routing
	Route::get('/{blogkey}', 'Main\PageController@blog')->name('blogpost');

	/*
	// expert advice blog post explicit url routing
	Route::get('/vagina-dialogue', 'Main\PageController@blog')->name('vagina-dialogue');
	Route::get('/cleansing', 'Main\PageController@blog')->name('cleansing');
	Route::get('/listening', 'Main\PageController@blog')->name('listening');
	Route::get('/ph-balance', 'Main\PageController@blog')->name('ph-balance');
	*/
});
Route::get('/expert-advice', 'Main\PageController@expertadvice')->name('drfrankie');
Route::post('/expert-advice', 'Main\PageController@expertadvice_submit');

Route::get('/faqs', 'Main\PageController@faqs')->name('faqs');

Route::get('/contact', 'Main\PageController@contact')->name('contact');
Route::post('/contact', 'Main\PageController@contact_submit');
Route::get('/cookie-notice', 'Main\PageController@misc')->name('cookie-notice');
Route::get('/privacy-policy', 'Main\PageController@misc')->name('privacy-policy');
Route::get('/third-party-information-collection', 'Main\PageController@misc')->name('third-party');
Route::get('/terms-and-conditions', 'Main\PageController@misc')->name('terms');

// Route::get('/search', 'Main\PageController@search')->name('search');

// update sitemap.xml
Route::get('/sitemap', 'Main\MiscController@sitemap')->name('sitemap');

// flush page cache
Route::get('/flush', 'Main\MiscController@flush');

Route::get('/mailtest', 'Main\PageController@mailtest')->name('mailtest');
Route::get('/instacheck', 'Main\PageController@instacheck')->name('instacheck');
// Route::get('/purge', 'Main\MiscController@purge');
// Route::get('/instagram', 'Auto\InstagramController@fetch')->name('instagram');

/*
Route::get('/phpinfo', function() {
	//echo phpinfo();
	dd(curl_version());
});
*/
