var app_version = new Date().getTime();
var app_live = true;

/**
 * Trace (console.log)
 */
function trace(msg, toScreen) {
	if(window.console) {
		console.log(msg);
	} else if ( typeof( jsTrace ) != 'undefined') {
		jsTrace.send(msg);
	} else {
		//alert(msg);
	}
	if(toScreen) {
		$("#app-debug").html(msg);
	}
}

function TrackEventGA(Category, Action, Label, Value) {
  "use strict";
  if (app_live && typeof (gtag) !== "undefined") {
    gtag('event', Action, {
      'event_category': Category,
      'event_label': Label,
      'value': Value
    });
  } else if (app_live && typeof (_gaq) !== "undefined") {
    _gaq.push(['_trackEvent', Category, Action, Label, Value]);
  } else if (app_live && typeof (ga) !== "undefined") {
    ga('send', 'event', Category, Action, Label, Value);
  } else {
    trace("GA: " + Category + "," + Action + "," + Label + "," + Value);
  }
}

function scrollToAnchor(aid,closest){}
/*
function scrollToAnchor(aid,closest){
  trace("Scrolling to: " + aid);
	var aTag = $("#"+ aid +"");
  if (aTag.length != 0) {
    if (closest != undefined) {
      aTag = aTag.closest(closest);
    }
    // If we have a cookie disclaimer and menu we need to factor that on to our scroll.
    var pos = aTag.offset().top;
    pos -= $("#cookieDisclaimer").height();
    pos -= $(".page-top").height();
    pos -= $(".page-top").find(".logo").height()/2;
  	$('html,body').animate({scrollTop: pos},'fast');
  }
}
*/

function refreshStyles() {
	$.each($('link[toRefresh]'), function(item) {
		if ($(this).attr("toRefresh") != undefined) {
			var newURL = $(this).attr("toRefresh") + "?" + Math.round(Math.random() * 100000);
			$(this).attr("href",newURL);
		}
	});
}

function isFirefox() {
	return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
}

function clearCookie() {
  window.Events.$emit('clearCookie');
}

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Shuffles array in place.
 * @param {Array} a items The array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function todaysDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	return dd+'-'+mm+'-'+yyyy;
}

if (!String.prototype.includes) {
  String.prototype.includes = function(search, start) {
    'use strict';
    if (typeof start !== 'number') {
      start = 0;
    }
    
    if (start + search.length > this.length) {
      return false;
    } else {
      return this.indexOf(search, start) !== -1;
    }
  };
}

if (!Array.prototype.map)
{
  Array.prototype.map = function(fun /*, thisArg */)
  {
    "use strict";

    if (this === void 0 || this === null)
      throw new TypeError();

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== "function")
      throw new TypeError();

    var res = new Array(len);
    var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
    for (var i = 0; i < len; i++)
    {
      // NOTE: Absolute correctness would demand Object.defineProperty
      //       be used.  But this method is fairly new, and failure is
      //       possible only if Object.prototype or Array.prototype
      //       has a property |i| (very unlikely), so use a less-correct
      //       but more portable alternative.
      if (i in t)
        res[i] = fun.call(thisArg, t[i], i, t);
    }

    return res;
  };
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function getScrollPercent() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    var fixer = (iOS) ? 58 : 0;
    var h = document.documentElement, 
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';
    //var clientHeight = document.body.getBoundingClientRect().height;
    console.log(h[sh],h.clientHeight);
    if (h[sh] <= h.clientHeight) {
      return 100;
    }
    return (h[st]||b[st]) / ((h[sh]-fixer||b[sh]-fixer) - h.clientHeight) * 100;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}