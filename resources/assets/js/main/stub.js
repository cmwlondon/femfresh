/**
 * stub.js
 */
define(

  ['jquery','vue','instagram', (pageViewJS === '') ? undefined : pageViewJS],

  function($,Vue,instagram,PageView) {

    function Me() {
      this.init();
    }

    Me.prototype = ({

      cookieDisclaimer:false,
      cookieName:'FemfreshDisclaimerCookie',

      init() {

        window.Events = new Vue({});

        // If we have an external page, load it.
        if(typeof PageView !== 'undefined') {
            window.pageView = new PageView();
        }

        $(window).on("resize",()=>{
          this.resizeEvent();
        });
        this.resizeEvent();

        $(window).on("scroll",()=>{
          this.scrollEvent();
        });
        this.scrollEvent();
        
        $(".page-top").find("li a.expander").bind("mouseover",(e) => {this.openSubMenu(e);});
        $(".page-top").find("li a.expander").bind("mouseout",(e) => {this.closeSubMenu(e);});

        $(".submenu").bind("mouseover",(e) => {this.openSubMenu(e);});
        $(".submenu").bind("mouseout",(e) => {this.closeSubMenu(e);});

        $("#hamburger").bind("click",(e) => {this.openMobileMenu(e);});
        $("#mobile-menu-close").bind("click",(e) => {this.closeMobileMenu(e);});

        $(".mobile-menu").find("menu.lev1 li.expander a.lev1 span").bind("click",(e) => {this.openMobileLev2(e);});

        $(".mobile-menu").find("menu.lev1 li.expander menu.lev2 li.expander a.lev2 span").bind("click",(e) => {this.openMobileLev3(e);});

        // $("a.range-anchor, a.btn-anchor").bind("click",(e) => {this.checkAnchorLink(e);});

        $(".bTrack").bind("click",(e) => {this.trackClick(e);});

        $(".revert-top").bind("click",(e) => {
          scrollToAnchor("top");
        });

        // Cookie disclaimer
        $(".cookie-disclaimer .btn-close").bind("click",() => {this.closeCookieDisclaimer()});
        this.cookieDisclaimer = !this.checkCookie();
        this.updateDisclaimerStyles();
        window.clearCookie = () => {
          this.clearCookie();
        }

        // If we have the anchor in the url we had better scroll down.
        var baseUrlParts = window.location.href.split("#");
        if (baseUrlParts.length > 1) {
          console.log('stub scrollToAnchor');
          scrollToAnchor(baseUrlParts[1]);
        }
      },

      openSubMenu(e) {
        clearTimeout(this.menuTimeoutID);
        var sub = $(e.currentTarget).data("submenu");
        $("#head-group").attr("data-show-submenu",sub);
      },

      closeSubMenu(e) {
        this.menuTimeoutID = setTimeout(() => {
          $("#head-group").attr("data-show-submenu","");
        },250);
      },

      openMobileMenu(e) {
        $("body").attr("data-mobile-menu","open");
      },

      closeMobileMenu(e) {
        $("body").attr("data-mobile-menu","closed");
        $(".mobile-menu").attr("data-lev2","");
        $(".mobile-menu").attr("data-lev3","");
      },

      openMobileLev2(e) {
        e.preventDefault();
        e.stopPropagation();
        var sub = $(e.currentTarget).parent().data("id");
        sub = ($(".mobile-menu").attr("data-lev2") == sub) ? "" : sub;
        $(".mobile-menu").attr("data-lev2",sub);
      },

      openMobileLev3(e) {
        e.preventDefault();
        e.stopPropagation();
        var sub = $(e.currentTarget).parent().data("id");
        sub = ($(".mobile-menu").attr("data-lev3") == sub) ? "" : sub;
        $(".mobile-menu").attr("data-lev3",sub);
      },

      checkAnchorLink(e) {
        var baseTargetParts = $(e.currentTarget).attr("href").split("#");
        var baseUrlParts = window.location.href.split("#");
        if ((baseUrlParts[0] == baseTargetParts[0]) || (baseTargetParts[0] == "" && baseTargetParts.length > 1)) {
          // Its this page.
          e.preventDefault();
          e.stopPropagation();
          this.closeMobileMenu();
          scrollToAnchor(baseTargetParts[1]);
          window.location.href = "#"+baseTargetParts[1];
        }
      },

      trackClick(e) {
        e.stopPropagation();
        var label = $(e.currentTarget).attr("data-track");
        var type = $(e.currentTarget).attr("data-type");
        TrackEventGA('external-link',type,label,1);
      },

      resizeEvent() {

        if (window.pageView != undefined) {
          if(typeof window.pageView.resizeEvent === 'function') {
            window.pageView.resizeEvent();
          } else {
            //trace("Resize function not found on page view");
          }
        }
        window.Events.$emit('resize');
      },

      scrollEvent() {
        window.Events.$emit('scroll');
        $("body").toggleClass("hasScrolled",($(window).scrollTop() > $(window).height()));
      },


      updateDisclaimerStyles: function() {
        /*
        if (this.cookieDisclaimer) {
          $("body").attr("data-disclaimer","true");
        } else {
          $("body").attr("data-disclaimer","false");
        }
        */
      },
      closeCookieDisclaimer: function(e) {
        this.cookieDisclaimer = false;
        this.updateDisclaimerStyles();
        this.setCookie();
      },
      setCookie: function() {
        var expiresDays = 1/24;
        var d = new Date();
        d.setTime(d.getTime() + (expiresDays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = this.cookieName + "=policy;"+expires+";path=/";
      },
      getCookie: function() {
        var name = this.cookieName + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
          c = c.substring(1);
        if (c.indexOf(name) == 0)
          return c.substring(name.length, c.length);
        }
        return "";
      },
      checkCookie: function() {
        var user = this.getCookie();
        if (user != "")
          return true;
        else
          return false;
      },
      clearCookie: function() {
        var d = new Date();
        var expires = "expires=" + d.toGMTString();
        document.cookie = this.cookieName + "=policy;"+expires+";path=/";
        this.cookieDisclaimer = true;
        this.updateDisclaimerStyles();
      }

    });

    

    return Me;
  }
);
