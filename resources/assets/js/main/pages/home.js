/**
 * home.js
 */
define(

  ['jquery','slick','vue','ScrollMagic','sm-debug','sm-plugin-gsap','TweenMax','TimelineMax'],

  function($, slick, Vue, ScrollMagic, SMGSAP, SMDebug, TweenMax, TimelineMax) {

    function Me() {
      this.init();
    }

    Me.prototype = ({
      init() {

      // header carousel
      $(".test-slick").slick({
          autoplay: true,
          autoplaySpeed: 4e3,
          arrows: !1,
          dots: !0,
          fade: !0,
          infinite: !0,
          swipe: !0,
          touchMove: !0
      });

      // video carousel element
      /* */
          $(".popupVideo").on("click", (function(o) {
            o.preventDefault();
            let t = $(this)
              , s = t.attr("data-title")
              , i = t.attr("data-source")
              , n = t.attr("data-poster")
              , vw = t.attr("data-vw")
              , vh = t.attr("data-vh")
              , vr = vw / vh;
            let windowSize = {
              "w" : $(window).width(),
              "h" : $(window).height(),
              "a" : $(window).width() / $(window).height()
            };

            // vr = vw / vh
            // vw = vr * vh
            // vh = vw / vr

            if ( vh > windowSize.h ) {
              vh = (parseInt(windowSize.h, 10) - 250);
              vw = vh * vr;

              if (vw > windowSize.w) {
                vw = (parseInt(windowSize.w, 10) - 30);
                vh = vw / vr;
              }

              vh = vh + "px";
              vw = vw + "px";
            }

            newVideoTag = $("<video></video>"),
            newVideoTag.attr({
                preload: "metadata",
                controls: "",
                playsinline: "",
                poster: n
            }).addClass("waiting");
            let a = $("<source></source>").attr({
                type: "video/mp4",
                src: i
            })

            if (vw !== "" && vh !== "") {
              newVideoTag.css({"height" : vh});  
              // $(".modal .modal-dialog").css({"max-width" : vw,"max-height" : vh});
              $(".modal .modal-dialog").css({"max-width" : vw});
            } 
            
            newVideoTag.append(a),
            $(".modal-body").empty().append(newVideoTag),
            $("#videopopup").css({
                display: "flex"
            }),
            $("#videoCaption").text(s),
            $("body").addClass("modal-open");

        })),

        $("#videopopup button.close").on("click", (function(o) {
            o.preventDefault(),
            $("body").removeClass("modal-open"),
            $("#videopopup").hide(),
            $(".modal-body").empty()
        }
        )),
        $(".modal-backdrop").on("click", (function(o) {
            o.preventDefault(),
            $("body").removeClass("modal-open"),
            $("#videopopup").hide(),
            $(".modal-body").empty()
        }
        )),
        $(".modal video").on("click", (function(e) {
            // alert("video"),
            e.stopPropagation()
        }
        ))

        /*
        $("#questions .outer-fix .inner-fix .advice > li").on("click", (function(o) {
            o.preventDefault(),
            $(this).hasClass("open") ? $(this).removeClass("open") : ($("#questions .outer-fix .inner-fix .advice > li.open").removeClass("open"),
            $(this).addClass("open"))
        }
        )),

        $(".expander > .btn").on("click", (function(o) {
            o.preventDefault();
            var t = $(this).parents(".expander");
            $(this).hasClass("opener") ? t.addClass("open") : t.removeClass("open")
        }
        ))
        */
        /* */

        var controller = new ScrollMagic.Controller();

        TweenMax.staggerTo('.init', 0.5, {className: "+=started"}, 0.2);
        TweenMax.to('.init-slow', 1, {className: "+=started"});

        var love_it_tween = TweenMax.to('.el_love_it', 1, {className: "+=shifted"});
        var love_it_scene = new ScrollMagic.Scene({
          triggerElement: '#trigger_head_middle',
          duration: 800
        })
        .setTween(love_it_tween)
        .offset(100)
        //.addIndicators()
        .addTo(controller);

        // var head_text_tween = TweenMax.to('#el_head_text', 1, {className: "+=shifted"});
        // var head_text_scene = new ScrollMagic.Scene({
        //   triggerElement: '#trigger_head_middle',
        //   duration:800
        // })
        // .setTween(head_text_tween)
        // .offset(200)
        // //.addIndicators()
        // .addTo(controller);

        var head_button_tween = TweenMax.to('#el_head_button', 1, {className: "+=shifted"});
        var head_button_scene = new ScrollMagic.Scene({
          triggerElement: '#trigger_head_middle',
          duration: 800
        })
        .setTween(head_button_tween)
        //.addIndicators()
        .addTo(controller);


        // Section 2
        for (var i = 1; i <= 4; i++) {
          var range_title_tween = TweenMax.to('#range_shift_title_'+i, 1, {className: "+=shifted"});
          var range_title_scene = new ScrollMagic.Scene({
            triggerElement: '.block-range[data-id="'+i+'"]',
           // duration: 400
          })
          .setTween(range_title_tween)
           .offset(-400)
          //.addIndicators()
          .addTo(controller);

          var range_text_tween = TweenMax.to('#range_shift_text_'+i, 1, {className: "+=shifted"});
          var range_text_scene = new ScrollMagic.Scene({
            triggerElement: '.block-range[data-id="'+i+'"]',
           // duration: 400
          })
          .setTween(range_text_tween)
           .offset(-300)
          //.addIndicators()
          .addTo(controller);

          //var range_btn_tween = TweenMax.to('#range_shift_btn_'+i, 1, {className: "+=shifted"});
          new ScrollMagic.Scene({
            triggerElement: '.block-range[data-id="'+i+'"]',
           // duration: 400
          })
          .setTween(TweenMax.to('#range_shift_btn_'+i, 1, {className: "+=shifted"}))
          .offset(-200)
          //.addIndicators()
          .addTo(controller);
        }

        /*
        var sara_tween = TweenMax.to('#sara_title', 1, {className: "+=shifted"});
        var sara_scene = new ScrollMagic.Scene({
          triggerElement: '#trigger_sara_top',
         // duration: 800
        })
        .setTween(sara_tween)
        .offset(-200)
        //.addIndicators()
        .addTo(controller);

        var sara_button = TweenMax.to('#sara_button', 1, {className: "+=shifted"});
        var sara_button_scene = new ScrollMagic.Scene({
          triggerElement: '#sara_button',
        })
        .setTween(sara_button)
        .triggerHook(0.9)
        //.addIndicators()
        .addTo(controller);
        */

        /*
        var ph_button = TweenMax.to('#ph_button', 1, {className: "+=shifted"});
        var ph_button_scene = new ScrollMagic.Scene({
          triggerElement: '#ph_button',
        })
        .setTween(ph_button)
        .triggerHook(0.9)
        //.addIndicators()
        .addTo(controller);

        var ph_tween = TweenMax.to('#ph_title', 1, {className: "+=shifted"});
        var ph_scene = new ScrollMagic.Scene({
          triggerElement: '#trigger_ph_top',
          //duration: 800
        })
        .setTween(ph_tween)
        .offset(-200)
        //.addIndicators()
        .addTo(controller);
        */

        /*
        var quiz_button = TweenMax.to('#quiz_button', 1, {className: "+=shifted"});
        var quiz_button_scene = new ScrollMagic.Scene({
          triggerElement: '#quiz_button',
        })
        .setTween(quiz_button)
        .triggerHook(0.9)
        //.addIndicators()
        .addTo(controller);
        */
      }
    });

    return Me;
  }
);