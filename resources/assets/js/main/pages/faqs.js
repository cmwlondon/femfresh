/**
 * faqs.js
 */
define(

  ['jquery','vue'],

  function($,Vue) {

    function Me() {
      this.faqVues = [];
      this.resizeTimeoutID = 0;
      this.searchIndex = [];

      this.init();
    }

    Me.prototype = ({
      init() {
        this.searchIndex = [];
        this.prepSearch();

        $(".faq-list li").bind("click", (e) => {this.toggleFAQ(e)});

        $("#faqs-open-all").bind("click", (e) => {this.openAllFAQs(e)});
        $("#faqs-close-all").bind("click", (e) => {this.closeAllFAQs(e)});

        $("#bSearch").bind("click", (e) => {this.performSearch(e)});
      },
    
      toggleFAQ(e)
      {
        var me = $(e.currentTarget);
        var current = me.attr("data-state");
        me.attr("data-state",  (current == "open") ? "closed" : "open");
      },

      openAllFAQs(e)
      {
        $.each(this.faqVues, (f,item) => {
          item.open();
        });
      },

      closeAllFAQs(e)
      {
        $.each(this.faqVues, (f,item) => {
          item.close();
        });
      },

      prepSearch() {
        var parent = this;
        $.each($(".faq"), (idx,item) => {
          var faq = new Vue({
            el: item,
            data: {
               show: false,
               filtered: false,
               maxHeight: 0,
               styleHeight: "0px",
               group: ""
            },
            mounted: function() {
              this.getHiddenElementHeight($(this.$el).find(".ans"));
              this.group = $(this.$el).closest(".faq-list").attr("data-group");
            },
            methods: {
              toggle (event) {
                this.show = !this.show;
                this.checkShow();
              },
              open() {
                this.show = true;
                this.checkShow();
              },
              close() {
                this.show = false;
                this.checkShow();
              },
              getHiddenElementHeight(element) {
                var tempId = 'tmp-'+Math.floor(Math.random()*99999);//generating unique id just in case
                $(element).clone()
                .css('position','absolute')
                .css('max-height','5000px')
                .appendTo($(element).parent())
                .css('opacity','0')
                .addClass(tempId);
                
                var h = $('.'+tempId).height()
                $('.'+tempId).remove()
                this.maxHeight = (h + 50);
              },
              checkShow () {
                if (this.show) {
                  this.styleHeight = `${this.maxHeight}px`;
                } else {
                  this.styleHeight = "0px";
                }
              },
              handleResize (event) {
                this.getHiddenElementHeight($(this.$el).find(".ans"));
                this.checkShow();
              }
            }
          });

          this.faqVues[idx] = faq;

          // Prep Our Search


          var rawContent = $(item).html();
          var tidyContent = rawContent.replace(/(<([^>]+)>)/ig,"").toLowerCase();
          
          this.searchIndex[idx] = {"raw" : rawContent, "tidy" : tidyContent, "found" : 0}
        }); // End of the loop

        var search = new Vue({
            el: '#searchGroup',
            data: {
              terms: "",
              count: parent.faqVues.length,
              store: []
            },
            mounted: function() {
              $.each($(".faq-list li h4, .faq-list li .ans p, .faq-list li .ans ul li"), (idx,item) => {
                $(item).attr("data-id",idx);
                this.store[idx] = $(item).html();
              });

            },
            methods: {
              doSearch (event) {
                //trace("Do Search:" + this.terms);
                this.count = 0;
                if (this.terms != "") {
                  var filtered = parent.searchIndex.map(this.isKeyWord);
                  $.each(filtered, (f,item) => {
                    if (item.found == 0) {
                      parent.faqVues[f].filtered = true;
                    } else {
                      parent.faqVues[f].filtered = false;
                      this.count ++;
                    }
                  });

                  // Delay highlighting our terms because they don't exist straight away.
                  setTimeout(() => {
                    var termsArray = this.terms.split(" ");
                    $.each(termsArray, (t,term) => {
                      this.highlight_words(term,".faq-list li h4, .faq-list li .ans p, .faq-list li .ans ul li");
                    });
                  }, 500);
                 
                  $("#faqsWrapper").addClass("searching");
                  $("#bReset").removeClass("hidden");
                  scrollToAnchor("faqs");
                } else {
                  $.each(parent.faqVues, (f,item) => {
                    item.filtered = false;
                    item.show = false;
                    item.checkShow();
                  });
                  $("#faqsWrapper").removeClass("searching");
                  this.count = parent.faqVues.length;
                  $("#bReset").addClass("hidden");
                  scrollToAnchor("searchGroup");
                }

                console.log("count" + this.count);

                if (this.count == 0) {
                  setTimeout(() => {
                    $("#noResults").removeClass("hidden");
                    $("#noResults").addClass("filtered-enter-active");
                    
                  }, 500);
                } else {
                  $("#noResults").addClass("hidden");
                  $("#noResults").removeClass("filtered-enter-active");
                }

                // Check the section headings to see if they contain items, if not hide them.
                $.each($("h2.heading"), (h,heading) => {
                  var group = $(heading).attr("data-group");
                  var countFound = 0;
                  $.each(parent.faqVues, (f,item) => {
                    if (parent.faqVues[f].group == group && parent.faqVues[f].filtered == false) {
                      countFound++;
                    }
                  });
                  $(heading).toggleClass("hidden",(countFound == 0));
                  $('#search-categories .cats li[data-group="'+group+'"]').toggleClass("hidden",(countFound == 0));
                  //trace($('#search-categories .cats li[data-group="'+group+'"]'));
                });



                parent.openAllFAQs();

                window.location.href = "#faqs";
              },
              resetSearch(e) {
                this.terms = "";
                this.doSearch(e);
                parent.closeAllFAQs();
              },
              isKeyWord (element) {
                var termsArray = this.terms.toLowerCase().split(" ");
                var count = 0;
                $.each(termsArray, (t,term) => {
                  count += this.occurrences(element.tidy, term, false);
                });
                element.found = count;
                return element;
              },
              occurrences(string, subString, allowOverlapping) {
                  string+=""; subString+="";
                  if(subString.length<=0) return string.length+1;

                  var n=0, pos=0;
                  var step=(allowOverlapping)?(1):(subString.length);

                  while(true){
                      pos=string.indexOf(subString,pos);
                      if(pos>=0){ n++; pos+=step; } else break;
                  }
                  return(n);
              },
              highlight_words(word, element) {
                if(word) {
                  $(element).each((idx,item) => {
                    $(item).html(this.store[parseInt($(item).data("id"))]);
                  });

                  word = word.replace(/\W/g, '');
                  var str = word.split(" ");

                  $.each($(str), (idx,term) => {
                    var textNodes = $(element).contents().filter(function() { return this.nodeType === 3 });
                    $.each(textNodes, function(idx,node) {
                      var content = $(node).text();
                      var regex = new RegExp(term, "gi");
                      content = content.replace(regex, '<span>' + term + '</span>');
                      $(this).replaceWith(content);
                    });
                  });
                }
              }
            }
        });
      }

      ,getMaxAnswerHeight() {
        var mh = 0;
        $.each($(".outline-box"), (idx,item) => {
          mh = Math.max(mh,this.getHiddenElementHeight($(item).find(".ans")));
        });
        return mh;
      }

      ,resizeEvent() {
        clearTimeout(this.resizeTimeoutID);
        this.resizeTimeoutID = setTimeout(() => {
          $.each(this.faqVues,(idx,item) => {
            item.handleResize();
          });
        },100);
      }

    });

    return Me;
  }
);