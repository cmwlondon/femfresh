/**
 * contactForm.js
 */
define(

  ['jquery'],

  function($) {

    function ContactForm() {
      this.init();
    }

    ContactForm.prototype = ({
      "init" : function () {

        // country selection dropdown
        $("#country").on("change", function(e) {
            var n = $(this).val();
            console.log("country change: %s", n),
            "other" === n ? $("div.cond_country").removeClass("hide") : $("div.cond_country").addClass("hide")
        }),
      	
      	// show/hide form fields in contact us form based on 'reason for enquiry' value
      	$('#reason').on('change',function(e) {
      		console.log("reason change: %s",  );
      		var reasonSelection = $(this).val();

      		$('div.conditionals').addClass('hide');

      		switch ( reasonSelection ) {
      			case "" : {
              $('#bSubmit').attr({"disabled" : true});
      			} break;
      			case "product" : {
              $('#bSubmit').attr({"disabled" : false});
      				$('div.cond_product').removeClass('hide');		
      			} break;
      			case "company" : {
              $('#bSubmit').attr({"disabled" : false});
      				$('div.cond_company').removeClass('hide');
      			} break;
      			case "complaint" : {
              $('#bSubmit').attr({"disabled" : false});
      				$('div.cond_complaint').removeClass('hide');
      			} break;
      		}
      	});
      }
    });

    return ContactForm;
  }
);