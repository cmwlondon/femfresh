/**
 * quiz.js
 */
define(

  ['jquery','vue','axios','social'],

  function($,Vue, axios,Social) {

    function Me() {
      axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

      /**
       * Next we will register the CSRF Token as a common header with Axios so that
       * all outgoing HTTP requests automatically have it attached. This is just
       * a simple convenience so we don't have to attach every token manually.
       */

      let token = document.head.querySelector('meta[name="csrf-token"]');

      if (token) {
          axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
      } else {
          console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
      }

      var socialManager = new Social();

      this.init();
    }

    Me.prototype = ({
      init() {
        var game = new Vue({
            el: '#gameVue',
            data: {
              isReady: false,
              state: 0,
              answers: [],
              correct: [2,1,2,1,2,3,1,1,3,1,2],
              score: 0,
              average_score: 0,
              isDiagrams: false
            },
            mounted: function() {
              this.resetGame();
              this.isReady = true;
              window.endGame = () => {this.endGame()};
            },
            methods: {
              resetGame() {
                this.score = 0;
                this.answers = [0,0,0,0,0,0,0,0,0,0,0];
                $(".answer-group li").removeClass("selected");
              },
              proceed (e) {
                this.state++;
                scrollToAnchor("gameVue");
                if (this.state == 23) {
                  this.saveScore();
                }
              },
              selectAnswer(e) {
                
                var btn_id = parseInt($(e.currentTarget).data("id"));
                var q_id = parseInt($(e.currentTarget).parent().data("question"));

                if (this.answers[q_id-1] == 0) {
                  var btn = $(e.currentTarget).addClass("selected");
                  this.answers[q_id-1] = btn_id;
                  this.calcScore();

                  setTimeout(() => {
                    this.proceed();
                  },500);
                }
              },
              computedResult: function (q_id) {
                return (this.answers[q_id-1] == this.correct[q_id-1]) ? 'Correct' : 'Incorrect';
              },
              calcScore: function() {
                this.score = 0;
                $.each(this.correct, (i,c) => {
                  if (c == this.answers[i]) {
                    this.score++;
                  }
                });
              },
              doRetry: function(e) {
                this.resetGame();
                this.state = 1;
                scrollToAnchor("gameVue");
              },
              doDiagrams: function(e) {
                this.isDiagrams = true;
                scrollToAnchor("gameVue");
              },
              doChallenge: function(e) {
                window.Events.$emit('shareFacebook',{'ref':'Quiz Challenge'});
              },
              closeDiagrams: function(e) {
                this.isDiagrams = false;
              },
              saveScore: function() {
                var data = {'answers':JSON.stringify(this.answers), 'score':this.score};
                var url = '/api/quiz';
                axios.post(url, data, {})
                .then((response) => {
                  if (response.status == 200) {
                    console.log("Score tracked successfully");
                  } else {
                    
                  }
                })
                .catch((error) => {
                  
                });
              },
              endGame: function() {
                this.answers = [1,1,1,1,1,1,1,1,1,1,1];
                this.state = 23;
                this.calcScore();
                this.saveScore();
                scrollToAnchor("gameVue");
              }
            },
            computed: {

            }
        });
      }
    });

    return Me;
  }
);