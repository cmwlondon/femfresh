/**
 * videos.js
 */
define(

  ['jquery','vue'],

  function($,Vue) {

    function Me() {
      this.init();
    }

    Me.prototype = ({
      init() {
        
        

        var videoVue = new Vue({
          el: '#videosVue',
            data: {
              player:null,
              selectedVideo: toplay,
              status: "paused",
              videos: {
                "1" : {
                  "ref": "tcBN8M6HlpY",
                  "image": "video-1-anatomy",
                  "name": "anatomy"
                },
                "2" : {
                  "ref": "IzmGHBUZtXc",
                  "image": "video-2-fitness",
                  "name": "fitness"
                },
                "3" : {
                  "ref": "ToaqVnyAkkA",
                  "image": "video-3-grooming",
                  "name": "grooming"
                },
                "4" : {
                  "ref": "8mL7sQeSL7s",
                  "image": "video-4-periods",
                  "name": "periods"
                },
                "5" : {
                  "ref": "D7Bmp_ewCvs",
                  "image": "video-5-sex",
                  "name": "sex&stuff"
                }
              }
            },
            mounted: function() {
              // // Load the IFrame Player API code asynchronously.
              // https://developers.google.com/youtube/iframe_api_reference
              var tag = document.createElement('script');
              tag.src = 'https://www.youtube.com/player_api';
              var firstScriptTag = document.getElementsByTagName('script')[0];
              firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


              // Replace the 'ytplayer' element with an <iframe> and
              // YouTube player after the API code downloads.
              window.onYouTubePlayerAPIReady = () => {
                //trace("YouTube player ready");
                this.player = new YT.Player('ytplayer', {
                  height: '100%',
                  width: '100%',
                  videoId: this.videos[this.selectedVideo].ref,
                  playerVars: {
                    color: 'white',
                    modestbranding: 1,
                    rel:0,
                    showinfo:0
                  },
                  events: {
                    'onReady': this.onPlayerReady,
                    'onStateChange': this.onPlayerStateChange
                  }
                });
              }

              window.onpopstate = (e) => {
                  if(e.state){
                    var urlParams = new URLSearchParams(window.location.search);
                    console.log(urlParams);
                    this.selectedVideo = urlParams.get('play');
                    this.status = "paused";
                    this.player.cueVideoById(this.videos[this.selectedVideo].ref, 0, "hd720")

                    // If we have the anchor in the url we had better scroll down.
                    var baseUrlParts = window.location.href.split("#");
                    if (baseUrlParts.length > 1) {
                      scrollToAnchor(baseUrlParts[1]);
                    }
                  }
              };
            },
            methods: {
              onPlayerReady(e) {
                //trace("Player is ready");
              },
              onPlayerStateChange(e) {
                //trace("Player state change:" + e.data);
                if (e.data == YT.PlayerState.ENDED) {
                  this.status = "paused";                  
                }
              },
              selectVideo(e) {
                //trace("Fired Select Video");
                this.selectedVideo = $(e.currentTarget).attr("data-id");
                this.status = "paused";
                this.player.cueVideoById(this.videos[this.selectedVideo].ref, 0, "hd720")
                window.history.pushState(this.selectedVideo, $("title").html(), $('link[rel="canonical"]').html()+'?play='+this.selectedVideo+'#videos');
              },
              playVideo(e) {
                //trace("Fired Play Video");
                this.status = "playing";
                this.player.playVideo();
              }
            }
        });

      }
    });

    return Me;
  }
);