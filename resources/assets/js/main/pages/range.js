/**
 * range.js
 */
define(

  ['jquery','slick'],

  function($, slick) {

    function Me() {
      this.init();

      var initSlide = 0;

      if ($('.productSlick').length !== 0) {
        console.log('carousel');

        $(".productSlick")
        .on("init", (function(o, t) {
        }))
        .slick({
          "slidesToShow" : 1,
          "slidesToScroll" : 1,
          "autoplay" : true,
          "autoplaySpeed" : 2000,
          "arrows" : false,
          "dots" : true
        })
      }

      // range component + main menu entries
      $('a.range-anchor').on('click',function(e){
        e.preventDefault();
        // \#([a-z]+)
        let currentPage = window.location.href.split('#');
        let targetHref = $(this).attr('href').split('#');
        let anchorTarget = targetHref[1];


        if( currentPage[0] === targetHref[0]){
          // scroll down to correct item
          
          let anchorOffset = $('#' + anchorTarget).offset().top;
          // get browser width to determine if top menu is in mobile or desktop mode
          let wWidth = $(window).width();
          // set scroll offset based on top menu height
          let topBarHeight = (wWidth > 700) ? 140 : 80;
          anchorOffset = anchorOffset - topBarHeight;

          $('html,body').animate({ "scrollTop" : anchorOffset }, "fast");

          window.location.href = "#" + anchorTarget;
        }
        if( currentPage[0] !== targetHref[0]){
          // go to new page
          window.location.href = targetHref[0] + '#' + targetHref[1];
        }
      })

      /*
      $(window).bind('hashchange', () => {
        //trace("Hash changed");
        var baseUrlParts = window.location.href.split("#");
        //trace(baseUrlParts);
        if (baseUrlParts.length > 1) {
          initSlide = (baseUrlParts[1] == "10") ? 1 : 0;
          $('.slick-products').slick('slickGoTo',initSlide);
          $(".button-switcher").attr("data-show",(initSlide == 0) ? "25" : "10");
        }
      });

      var baseUrlParts = window.location.href.split("#");
      if (baseUrlParts.length > 1) {
        if (baseUrlParts[1] == "10") {
          initSlide = 1;
        }
      }
      */

      $(".button-switcher").attr("data-show",(initSlide == 0) ? "25" : "10");

      $('.slick-products').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        nextArrow: $(".slick-arrow.right"),
        prevArrow: $(".slick-arrow.left"),
        initialSlide:initSlide
      });

      $('.slick-products').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $(".button-switcher").attr("data-show",(nextSlide == 0) ? "25" : "10");
      });
    }

    Me.prototype = ({
      init() {
        $(".container.product .inner-fix .faqs ul li").bind("click", (e) => {this.toggleFAQ(e)});
        $(".container.product .inner-fix .how-to-use ul.node > li").bind("click", (e) => {this.toggleHTU(e)});
      },
      
      toggleFAQ(e)
      {
        var current = $(".container.product .inner-fix .faqs ul").attr("data-reveal");
        var id = $(e.currentTarget).attr("data-id");
        $(".container.product .inner-fix .faqs ul").attr("data-reveal",  (current == id) ? "" : id);
      },

      toggleHTU(e)
      {
        var current = $(".container.product .inner-fix .how-to-use ul.node").attr("data-reveal");
        var id = $(e.currentTarget).attr("data-id");
        $(".container.product .inner-fix .how-to-use ul.node").attr("data-reveal",  (current == id) ? "" : id);
      }
    });

    return Me;
  }
);