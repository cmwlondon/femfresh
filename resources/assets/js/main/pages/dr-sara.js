/**
 * dr-sara.js
 */
define(

  ['jquery','slick','axios'],

  function($, slick, axios) {

    function Me() {
      axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

      /**
       * Next we will register the CSRF Token as a common header with Axios so that
       * all outgoing HTTP requests automatically have it attached. This is just
       * a simple convenience so we don't have to attach every token manually.
       */

      let token = document.head.querySelector('meta[name="csrf-token"]');

      if (token) {
          axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
      } else {
          console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
      }

      this.init();
    }

    Me.prototype = ({
      init() {
        /* */
            $(".blogslick")
            .on("init", (function(o, t) {
                $("#videopopup").addClass("portrait"),
                $(".popupVideo").on("click", (function(o) {
                    o.preventDefault();
                    let t = $(this)
                      , s = t.attr("data-title")
                      , i = t.attr("data-source")
                      , n = t.attr("data-poster");
                    // console.log("title:'%s' source:'%s' poster:'%s'", s, i, n),
                    newVideoTag = $("<video></video>"),
                    newVideoTag.attr({
                        preload: "metadata",
                        controls: "",
                        playsinline: "",
                        poster: n
                    }).addClass("waiting");
                    let a = $("<source></source>").attr({
                        type: "video/mp4",
                        src: i
                    });
                    newVideoTag.append(a),
                    $(".modal-body").empty().append(newVideoTag),
                    $("#videopopup").css({
                        display: "flex"
                    }),
                    $("#videoCaption").text(s),
                    $("body").addClass("modal-open")
                }
                ))
            }
            )).slick({
                arrows: !0,
                dots: !1,
                swipe: !0,
                touchMove: !0,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: !0,
                responsive: [{
                    breakpoint: 960,
                    settings: "unslick"
                }],
                init: function() {
                    // console.log("slickslider init")
                }
            }),
            $("#videopopup button.close").on("click", (function(o) {
                o.preventDefault(),
                $("body").removeClass("modal-open"),
                $("#videopopup").hide(),
                $(".modal-body").empty()
            }
            )),
            $(".modal-backdrop").on("click", (function(o) {
                o.preventDefault(),
                $("body").removeClass("modal-open"),
                $("#videopopup").hide(),
                $(".modal-body").empty()
            }
            )),
            $(".modal video").on("click", (function(e) {
                // alert("video"),
                e.stopPropagation()
            }
            )),
            $("#questions .outer-fix .inner-fix .advice > li").on("click", (function(o) {
                o.preventDefault(),
                $(this).hasClass("open") ? $(this).removeClass("open") : ($("#questions .outer-fix .inner-fix .advice > li.open").removeClass("open"),
                $(this).addClass("open"))
            }
            )),
            $(".expander > .btn").on("click", (function(o) {
                o.preventDefault();
                var t = $(this).parents(".expander");
                $(this).hasClass("opener") ? t.addClass("open") : t.removeClass("open")
            }
            ))
        /* */
        /*
        $('.slick-tips').slick({
          dots: true,
          infinite: false,
          speed: 300,
          slidesToShow: 1,
          nextArrow: $(".arrow.right"),
          prevArrow: $(".arrow.left")
        });

        $('.slick-tips').on('beforeChange', function(event, slick, currentSlide, nextSlide){
          $(".tip-title").html('tip ' + (nextSlide+1));
          $(".slick-wrapper").attr("data-show",(nextSlide+1));
        });

        $("#expert-advice .outer-fix .inner-fix .advice li").bind("click", (e) => {this.toggleAdvice(e)});

        $("#question-submit").bind("click",(e) => {this.submitQuestion(e);});
        */
      },
    
      /*
      toggleAdvice(e) {
        var current = $("#expert-advice .outer-fix .inner-fix .advice").attr("data-reveal");
        var id = $(e.currentTarget).attr("data-id");
        $("#expert-advice .outer-fix .inner-fix .advice").attr("data-reveal",  (current == id) ? "" : id);
      },
      */

      submitQuestion(e) {
        var question = $("#question").val();
        if (question == "") {
          alert('Please provide a question');
        } else {
          $(".ask-more .result-row").empty();
          var data = {'question':question};
          var url = '/api/question';
          axios.post(url, data, {})
          .then((response) => {
            if (response.status == 200) {
              $(".ask-more .result-row").append("<p>"+response.data.message+"</p>");
            } else {
              
            }
          })
          .catch((error) => {
            
          });
        }
      }
    });

    return Me;
  }
);