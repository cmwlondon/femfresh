/**
 * ph-balance.js
 */
define(

  ['jquery'],

  function($) {

    function Me() {
      this.init();
    }

    Me.prototype = ({
      init() {
        this.currItem = parseInt($("#ph-app").attr("data-item"));
        $("#ph-app").find(".arrow.up").bind("click",(e) => {this.shiftUp(e);});
        $("#ph-app").find(".arrow.down").bind("click",(e) => {this.shiftDown(e);});
      },

      shiftUp(e)
      {
        if (this.currItem > 1) {
          this.currItem--;
        } else {

        }
        this.updatePosition();
      },

      shiftDown(e)
      {
        if (this.currItem < 7) {
          this.currItem++;
        } else {

        }
        this.updatePosition();
      },

      updatePosition() {
        $("#ph-app").attr("data-item",this.currItem);
      }
    });

    return Me;
  }
);