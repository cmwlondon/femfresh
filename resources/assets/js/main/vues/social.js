/**
 * social.js
 */
define(

  ['vue'],

  function(Vue) {

    function Me() {
      this.init();
    }

    Me.prototype = ({
      init() {
        new Vue({
          el: '#socialManager',
          data: {
            fbAppId: 0,
            shareURL: site_url + "/knowledge-hub/interactive-quiz",
            caption: 'Sharing Caption'
          },
          mounted() {
            this.fbAppId = $('meta[property="fb:app_id"]').attr("content");

            window.Events.$on('shareFacebook', (e) => this.shareFacebook(e));
            window.Events.$on('shareTwitter', (e) => this.shareTwitter(e));
            window.Events.$on('fb:initializing', function() { 
              // Do something before FB.Init is called
                //trace("Initialising Facebook");
              });
            window.Events.$on('fb:initialized', function() { 
              // Do something before FB.Init is called
                //trace("Initialised Facebook");
              });
            this.initFacebook();
          },
          methods: {
            initFacebook() {
              window.fbInit = window.fbInit || false;
              if (window.fbInit) {
                 window.Events.$emit('fb:initialized');
              } else {
                // Aync Init
                window.fbAsyncInit = () => {
                  window.fbInit = true;
                  window.Events.$emit('fb:initializing');
                  FB.init({
                    appId      : this.fbAppId,
                    xfbml      : true,
                    version    : 'v2.9'
                  });
                  FB.AppEvents.logPageView();
                  window.Events.$emit('fb:initialized');
                };

                (function(d, s, id){
                   var js, fjs = d.getElementsByTagName(s)[0];
                   if (d.getElementById(id)) {return;}
                   js = d.createElement(s); js.id = id;
                   js.src = "//connect.facebook.net/en_US/sdk.js";
                   fjs.parentNode.insertBefore(js, fjs);
                 }(document, 'script', 'facebook-jssdk'));
              }
            },
            shareFacebook(event) {
              var share_url = this.shareURL;
              TrackEventGA('Social', 'Facebook', event.ref, 1);
              FB.ui(
                {
                  method: 'feed',
                  link: share_url,
                  caption: this.caption
                },
                function(response) {}
              ); 

            },
            shareTwitter(event) {
              // var share_url = 'https://twitter.com/intent/tweet?text=';
              // share_url += encodeURIComponent(twitter_msg);
              // share_url += '&url='+this.shareURL+ '/' + event.ref
              // window.open(share_url,'_blank');

              // TrackEventGA('Social', 'Twitter', product.title, 1);
            }
          }
        })
      }
    });

    return Me;
  }  
);
