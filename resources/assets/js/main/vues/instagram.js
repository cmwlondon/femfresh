/**
 * instagram.js
 */
define(

  ['vue'],

  function(Vue) {

    return new Vue({
      el: '#instagram',
      data: {
        gramImages: [1,2,3,4],
        gramBank: []
      },
      mounted: function() {
        if (instagram_bank > 4) {
          for (var b = 5; b <= instagram_bank; b++) {
            this.gramBank.push(b);
          } 
          shuffle(this.gramBank);

          setInterval(() => {
            this.changeInstagramImage();
          },5000);

          setTimeout(() => {
            this.fixDynamicImages();
          },2000);
        }
      },
      methods: ({
        changeInstagramImage() {
          var slot = getRandomInt(0,this.gramImages.length-1);
          var newImage = this.gramBank.shift();
          var oldImage = this.gramImages[slot];
          this.gramImages[slot] = newImage;
          this.gramBank.push(oldImage);
          //trace(slot+","+oldImage+","+newImage);
          $('.instagram .block[data-id="'+(slot + 1)+'"]').attr("data-show",newImage);
        },
        fixDynamicImages() {
          $('.gram img[src=""]').each(function(idx) {
            $(this).attr("src",$(this).data("src"));
          });
        }
      })
    })
  }
);
