/**
 * main.js
 * contents of main.js with extra configuration
 */
require.config({

    baseUrl: "/js/main/",

    urlArgs: "bust=" + (new Date()).getTime(), // halt caching of .js files for development

    waitSeconds: 0, // disable script loading timeout

    paths: {
        /**
         * Libraries
         */
        'jquery':          '//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min',
        'vue':             '//cdnjs.cloudflare.com/ajax/libs/vue/2.2.6/vue.min',
        'axios':           '//cdnjs.cloudflare.com/ajax/libs/axios/0.16.2/axios.min',
        'async':           '/js/libs/requirejs/plugins/async',
        'es6-promise':     '//cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min',
        'es6-promise-auto':'//cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min',

        'ScrollMagic':      '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min',
        'sm-debug':         '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min',
        'sm-plugin-gsap':   '../libs/scrollmagic/animation.gsap.min',
        'TweenMax':         '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min',
        'TimelineMax':      '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TimelineMax.min',

        'slick':            '../libs/slick/slick.min',
        'instagram':        'vues/instagram.min',
        'social':           'vues/social.min',

        'common':          '../libs/common.min',

        'stub':            'stub.min'
    },


    /**
     * Use shim for non AMD (Asynchronous Module Definition) compatible scripts
     */
    shim: {
        'vue': {
            exports: 'Vue'
        },
        'fancybox': {
          deps  : ['jquery']
          ,exports : 'fancybox'
        },
        'es6-promise': {
            exports: 'es6promise'
        },
        'es6-promise-auto': {
            exports: 'es6promiseauto'
        },
        'ScrollMagic': {
            exports: 'ScrollMagic'
        },
        'sm-debug': {
            deps:['ScrollMagic'],
            exports: 'SMDebug'
        },
        'sm-plugin-gsap': {
            deps:['ScrollMagic'],
            exports: 'SMGSAP'
        },
        'TimelineMax': {
            deps:['TweenMax'],
            exports: 'TimelineMax'
        }
    }
});

/**
 * Load the init module
 */
require(['stub','jquery','common'],
    function(Stub,$,Common) {
      window.Stub = new Stub();
    }
);