<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Links out to product listings on websites
    |--------------------------------------------------------------------------
    |
    */

    // Route::get('/freshening-and-soothing-wipes-daily', 'Main\PageController@range')->name('25-wipes');
    // Route::get('/freshening-and-soothing-wipes-sensitive', 'Main\PageController@range')->name('10-wipes');

    'daily-intimate-wash' => 'http://www.boots.com/femfresh-intimate-hygiene-daily-intimate-wash-250ml-10131120',
    'ultimate-care-pure-wash' => 'http://www.boots.com/femfresh-ultimate-care-pure-and-fresh-gel-wash-250ml-10144852',
    'ultimate-care-active-fresh-wash' => 'http://www.boots.com/femfresh-ultimate-care-active-fresh-wash-250ml-10131121',
    'ultimate-care-soothing-wash' => 'http://www.boots.com/femfresh-ultimate-care-soothing-wash-250ml-10131123',

    '10-wipes' => 'https://www.superdrug.com/Toiletries/Feminine-Care/Feminine-Wash-%26-Wipes/Femfresh-Wipes-25-Pack/p/252285', // daily wipes
    '25-wipes' => 'https://www.superdrug.com/Toiletries/Washing-%26-Bathing/Wet-Wipes/Femfresh-0%25-Feminine-Intimate-Wipes/p/802784', // 0% sensitive wipes

    'ultimate-care-active-fresh-deodorant' => 'http://www.boots.com/femfresh-active-fresh-deo-spray-10171514',
    'freshness-deodorant' => 'http://www.boots.com/femfresh-deodorant-spray-125ml-10041138',
    '2-in-1-shower-shave-cream' => 'http://www.boots.com/femfresh-shower-and-shave-cream-200ml-10228169',
    'post-shave-balm' => 'http://www.boots.com/femfresh-post-shave-balm-50ml-10228171',
    're-balance-powder' => '',
    'body-wash' => 'https://www.superdrug.com/Toiletries/Feminine-Care/Feminine-Wash/Femfresh-Sensitive-Skin-Intimate-Wash-250ml/p/776626',

    'odour-eliminating-gel' => 'https://www.superdrug.com/Health/Women%27s-Intimate-Health-Care/Bacterial-Vaginosis/Femfresh-Solutions-Odour-Eliminating-Gel/p/802787',
    'external-comfort-gel' => 'https://www.superdrug.com/Health/Women%27s-Intimate-Health-Care/Vaginal-Dryness-Treatment/Femfresh-Solutions-External-Comfort-Gel/p/802786',

];
