<!doctype html>
<html class="no-js" lang="en">
	<head>
		<!-- OneTrust Cookies Consent Notice start for femfresh.co.uk -->
		<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="bdafb636-91f9-4d1c-8843-339149b83cda" ></script>
		<script type="text/javascript">
		function OptanonWrapper() { }
		</script>
		<!-- OneTrust Cookies Consent Notice end for femfresh.co.uk -->
		@include('main.layouts.partials._analytics')

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{{ $meta['title'] }}}</title>
		<meta name="description" content="{{{ $meta['desc'] }}}" />
		<meta name="keywords" content="{{{ $meta['keywords'] }}}"/>
		<meta name="keyphrases" content="{{{ $meta['keyphrases'] }}}"/>
		
		@if (isset($pageViewCSS) && count($pageViewCSS) > 0)
	        @foreach($pageViewCSS as $stylesheet)
	            @if (App::environment() == 'local')
	            	<link rel="stylesheet" href="{{{$stylesheet}}}?{{{$version}}}" toRefresh="{{{$stylesheet}}}"/>
	            @else
	            	<link rel="stylesheet" href="{{{$stylesheet}}}?{{{$version}}}"/>
	            @endif
	        @endforeach
	    @endif

		<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="en-gb, English" />
		<meta http-equiv="Content-Style-Type" content="text/css" />

		<!-- Chrome Frame -->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<!-- For Facebook -->
		<meta property="og:locale" content="en_GB" />
		<meta property="og:type" content="{{{$meta['pagetype']}}}" />
		<meta property="og:title" content="{{{$meta['title']}}}" />
		<meta property="og:description" content="{{{$meta['desc']}}}" />
		<meta property="og:url" content="{{{$meta['link']}}}" />
		<meta property="og:site_name" content="femfresh"/>
		<meta property="og:image" content="{{{$meta['image']}}}?{{{$version}}}" />
		
		<meta property="fb:app_id" content="{{{$meta['fbid']}}}"/>

		<meta name="format-detection" content="telephone=no">
		
		@if (isset($nocache))
		<meta name="robots" content="noindex, nofollow, noarchive"/>
		@endif
		
		@if (isset($canonical))
		<link rel="canonical" href="{{{ $canonical }}}">
		@else
		<link rel="canonical" href="{{{ url()->current() }}}">
		@endif
		
		@include('main.layouts.partials._favicon')
	</head>

	<body class="{{{ $section }}}" data-mobile-menu="closed">
		<a id="top"></a>

		@include('main.layouts.partials._main-menu')

		<div id="page">
			@yield('content')

			@include('main.layouts.partials._footer')
		</div>

		<div class="revert-top"><img src="/images/arrow-to-top.svg" alt="Go to top" title="Top of page"/></div>

		@yield('components')

		<script>
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
			@if (isset($requireMaps))
				var requireMaps = "{{ $requireMaps }}";
			@endif
		</script>

		{!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main.min.js?1']) !!}
	  <div id="videopopup" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: none;" aria-hidden="true">
	  	<div class="modal-backdrop"></div>
	    <div class="modal-dialog" role="document">
	      <div class="modal-content">

	        <div class="modal-header">
	          <h5 class="modal-title" id="videoCaption">Modal title</h5>
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            <span aria-hidden="true">×</span>
	          </button>
	        </div>

	        <div class="modal-body">
	          {{-- <video preload="metadata" controls="controls" playsinline="" class="waiting"><source type="video/mp4" src="/video/home/brand-video.mp4"></video> --}}
	        </div>

	        {{-- <div class="modal-footer"></div> --}}
	      </div>
	    </div>
	  </div>
	</body>
</html>
