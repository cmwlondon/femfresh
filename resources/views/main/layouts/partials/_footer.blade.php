<footer class="container bgw">
	<div class="inner-fix">
		<div class="logo-block">
			<div class="logo">
				<a href="{!! route('home') !!}"><img src="/images/logo.svg?{{{$version}}}" alt="femfresh"/></a>
				<p>&copy; femfresh 2020</p>
			</div>
		</div>

		<div class="menu-block">
			<div class="social">
				@include('main.layouts.partials._social')
			</div>
			<menu>
				<li><a href="{!! route('home') !!}">Home</a></li>
				<li><a href="{!! route('range') !!}">Our Products</a></li>
				<li><a href="{!! route('about') !!}">About femfresh</a></li>
				<li><a href="{!! route('knowledge') !!}">Knowledge hub</a></li>
				{{-- <li><a href="{!! route('contact') !!}">Contact us</a></li> --}}
				<li><a href="{!! route('faqs') !!}">FAQs</a></li>
				<li><a href="{!! route('terms') !!}">Terms &amp; Conditions</a></li>
				<li><a href="{!! route('privacy-policy') !!}">Privacy Policy</a></li>
				<li><a href="{!! route('cookie-notice') !!}">Cookie Notice</a></li>
				<li><a href="http://www.churchdwight.co.uk" target="_blank">Church &amp; Dwight</a></li>
			</menu>
<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->
		</div>
	</div>
</footer>