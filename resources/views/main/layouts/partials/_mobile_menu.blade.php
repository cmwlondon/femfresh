<div class="mobile-menu" data-lev2="" dta-lev3="">
	<div class="outer-fix">
		<menu class="lev1">
			<li class="expander"><a href="{!! route('range') !!}" class="lev1" data-id="range">our range<span></span></a>
				<menu class="lev2" data-submenu="range">
					<li class="expander"><a href="{!! route('range') !!}#washes" class="lev2 range-anchor" data-id="washes">washes<span></span></a>
						<menu class="lev3" data-tertiary-menu="washes">
							<li><a href="{!! route('daily-intimate-wash') !!}">daily intimate wash</a></li>
							{{-- <li><a href="{!! route('ultimate-care-pure-wash') !!}">ultimate care pure wash</a></li> --}}
							<li><a href="{!! route('ultimate-care-active-wash') !!}">ultimate care active wash</a></li>
							<li><a href="{!! route('ultimate-care-soothing-wash') !!}">ultimate care soothing wash</a></li>
							<li><a href="{!! route('body-wash') !!}">0% wash</a></li>
						</menu>
					</li>
					<li class="expander"><a href="{!! route('range') !!}#wipes" class="lev2 range-anchor" data-id="wipes">wipes<span></span></a>
						<menu class="lev3" data-tertiary-menu="wipes">
							<li><a href="{!! route('25-wipes') !!}">daily wipes</a></li>
							<li><a href="{!! route('10-wipes') !!}">0% sensitive wipes</a></li>
						</menu>
					</li>
					<li class="expander"><a href="{!! route('range') !!}#sprays" class="lev2 range-anchor" data-id="sprays">sprays<span></span></a>
						<menu class="lev3" data-tertiary-menu="sprays">
							<li><a href="{!! route('ultimate-care-active-fresh-deodorant') !!}">ultimate care active fresh deodorant</a></li>
							<li><a href="{!! route('freshness-deodorant') !!}">freshness deodorant</a></li>
						</menu>
					</li>

					<li class="expander"><a href="{!! route('range') !!}#solutions" class="lev2 range-anchor" data-id="solutions">Femfresh Solutions&trade;<span></span></a>
						<menu class="lev3" data-tertiary-menu="solutions">
							<li><a href="{!! route('external-comfort-gel') !!}">External Comfort Gel</a></li>
							<li><a href="{!! route('odour-eliminating-gel') !!}">Odour Eliminating Gel</a></li>
						</menu>
					</li>

					<li><a href="{!! route('re-balance-powder') !!}" class="lev2" data-id="powder">powder<span></span></a></li>
				</menu>
			</li>

			<li><a href="{!! route('about') !!}" class="lev1" data-id="about">about femfresh</a><span></span></li>

			<li><a href="{!! route('drfrankie') !!}" class="lev1" data-id="expert-advice">expert advice</a><span></span></li>

			<li class="expander"><a href="{!! route('knowledge') !!}" class="lev1" data-id="knowledge">knowledge hub<span></span></a>
				<menu class="lev2" data-submenu="knowledge">
					<li><a href="{!! route('knowledge') !!}#articles" class="lev2" data-id="articles">the big questions<span></span></a>
					<li><a href="{!! route('about-ph-balance') !!}" class="lev2">about pH-balance</a></li>
					<li><a href="/knowledge-hub/interactive-quiz" class="lev2">quiz – vulva or vagina?</a></li>
				</menu>
			</li>

			<li><a href="{!! route('faqs') !!}" class="lev1" data-id="faqs">faqs</a><span></span></li>

			<li><a href="{!! route('contact') !!}" class="lev1" data-id="contact">contact us</a><span></span></li>
		</menu>
	</div>
</div>>
