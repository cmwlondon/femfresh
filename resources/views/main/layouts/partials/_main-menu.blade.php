@include('main.layouts.partials._disclaimer')

<div id="head-group" data-show-submenu="">
	<div class="page-top">

		<div class="outer-fix">
			<a href="/"><img src="/images/logo.svg?{{{$version}}}" class="logo" alt="femfresh logo"/></a>
			<menu>
				<li><a href="{!! route('range') !!}" class="expander {!! (($section == 'our-range') ? 'active' : '') !!}" data-submenu="range">our range</a></li>
				<li><a href="{!! route('about') !!}" class="no-expand {!! (($section == 'about-femfresh') ? 'active' : '') !!}">about femfresh</a></li>
				<li><a href="{!! route('drfrankie') !!}" class="no-expand {!! (($section == 'expert-advice') ? 'active' : '') !!}">expert advice</a></li>
				<li><a href="{!! route('knowledge') !!}" class="expander {!! (($section == 'knowledge-hub') ? 'active' : '') !!}" data-submenu="knowledge-hub">knowledge hub</a></li>
				<li><a href="{!! route('faqs') !!}" class="no-expand {!! (($section == 'faqs') ? 'active' : '') !!}">faqs</a></li>
				<li><a href="{!! route('contact') !!}" class="no-expand {!! (($section == 'contact') ? 'active' : '') !!}">contact us</a></li>
			</menu>

			@include('main.layouts.partials._hamburger')
		</div>
	</div>

	@include('main.layouts.partials._mobile_menu')

	<div class="submenu" data-submenu="range">
		<div class="outer-fix">
			<div class="menu-group">
				<div class="col">
					<menu>
						<li><a href="{!! route('range') !!}#washes" class="range-anchor">washes</a></li>
						<li><a href="/our-range/daily-intimate-wash">daily intimate wash</a></li>
						{{-- <li><a href="/our-range/ultimate-care-pure-wash">ultimate care pure wash</a></li> --}}
						<li><a href="/our-range/ultimate-care-active-wash">ultimate care<br/>active wash</a></li>
						<li><a href="/our-range/ultimate-care-soothing-wash">ultimate care soothing wash</a></li>
						<li><a href="/our-range/body-wash">0% wash</a></li>
					</menu>
				</div>
				<div class="col">
					<menu>
						<li><a href="{!! route('range') !!}#wipes" class="range-anchor">wipes</a></li>
						<li><a href="/our-range/daily-wipes">Daily wipes</a></li>
						<li><a href="/our-range/0percent-sensitive-wipes">0% Sensitive wipes</a></li>
					</menu>
				</div>
				<div class="col">
					<menu>
						<li><a href="{!! route('range') !!}#sprays" class="range-anchor">sprays</a></li>
						<li><a href="/our-range/ultimate-care-active-fresh-deodorant">ultimate care active fresh deodorant</a></li>
						<li><a href="/our-range/freshness-deodorant">freshness deodorant</a></li>
					</menu>
				</div>
				<div class="col">
					<menu>
						<li><a href="{!! route('range') !!}#solutions" class="range-anchor">Femfresh Solutions&trade;</a></li>
						<li><a href="/our-range/external-comfort-gel">external comfort&nbsp;gel</a></li>
						<li><a href="/our-range/odour-eliminating-gel">odour eliminating&nbsp;gel</a></li>
					</menu>
				</div>
				<div class="col">
					<menu>
						<li><a href="/our-range/re-balance-powder">powder</a></li>
						<li><a href="/our-range/re-balance-powder">re-balance<br/>powder</a></li>
					</menu>
				</div>
			</div>
		</div>
	</div>

	<div class="submenu" data-submenu="knowledge-hub">
		<div class="outer-fix">
			<div class="menu-group">
				<div class="col">
					<menu>
						<li><a href="/knowledge-hub#articles">the big questions</a></li>
						<li><a href="/knowledge-hub/about-ph-balance">about ph-balance</a></li>
						<li><a href="/knowledge-hub/interactive-quiz">quiz – vulva or vagina?</a></li>
					</menu>
				</div>
			</div>
		</div>
	</div>
</div>
