@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="she-talks-header">
		<div class="inner-fix head">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAGCAIAAACXToPoAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpENjI2MkYxNDMzM0YxMUU4QkNBQ0IxMkY2N0Q5MEI4MCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpENjI2MkYxNTMzM0YxMUU4QkNBQ0IxMkY2N0Q5MEI4MCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkQ2MjYyRjEyMzMzRjExRThCQ0FDQjEyRjY3RDkwQjgwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkQ2MjYyRjEzMzMzRjExRThCQ0FDQjEyRjY3RDkwQjgwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+XOHcVQAAABVJREFUeNpi/P//PwMRgHGYqAMIMACvCxH1wQhEqAAAAABJRU5ErkJggg==" class="shim-desktop" alt="Shim"/>

			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCN0Q3RkFEMTMzNTkxMUU4ODRFN0E3MDNEQzNBNDFEQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCN0Q3RkFEMjMzNTkxMUU4ODRFN0E3MDNEQzNBNDFEQyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkI3RDdGQUNGMzM1OTExRTg4NEU3QTcwM0RDM0E0MURDIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkI3RDdGQUQwMzM1OTExRTg4NEU3QTcwM0RDM0E0MURDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+fYH7rgAAAAZQTFRF////AAAAVcLTfgAAAAxJREFUeNpiYAAIMAAAAgABT21Z4QAAAABJRU5ErkJggg==" class="shim-mobile" alt="Shim"/>

			<img src="/images/videos/header-image-1.jpg?{{{$version}}}" class="header layer-1" alt="header image"/>
			<img src="/images/videos/header-image-2.png?{{{$version}}}" class="header layer-2" alt="header image"/>
			<img src="/images/videos/header-block.png?{{{$version}}}" class="header layer-3" alt="blue block"/>
			<img src="/images/videos/header-she-talks.svg?{{{$version}}}" class="header-logo" alt="She Talks: made possible by femfresh"/>

			<p>A new series that uncovers facts, fiction and some refreshingly real experiences.</p>
			<a href="#videos" class="btn btn-anchor">WATCH THE VIDEOS</a>
		</div>
	</div>

	<div class="container" data-ref="she-talks-intro">
		<div class="inner-fix">
			<div class="nush-intro">
				<div class="nush-image">
					<img src="/images/videos/nush.jpg?{{{$version}}}" class="" alt="Nush"/>
					<div class="block">
						<h2>Hey there,<br/>I&rsquo;m Nush,</h2>
						<p>and I want us to talk <br/>about &lsquo;down there&rsquo;</p>
					</div>
				</div>
				<div class="nush-text">
					<p>Yes, I want us to get intimate with our intimate areas – the vulva, the vagina, pubes, everything. As a vagina owner myself, I feel like I don’t really know enough about it to give it the TLC it deserves.</p>
					<p>So, join me in my new series as I go on a journey to get intimate with it. I’ll be inviting along a few guests to talk everything from sex, periods, anatomy, sweat and grooming – nothing is off the table.</p>
					<p><span>Well, over half the population has one, so let’s love it!</span></p>
				</div>	
			</div>
		</div>
	</div>

	<div id="videosVue" class="container" data-ref="video-player" :data-playing-id="selectedVideo" :data-status="status">
		<div class="outer-fix">
			<div class="inner-fix">
				<img id="videos" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAMAAAAM9FwAAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo0MTdEQzAwRDMyOTYxMUU4QUVCNTlDMUFCMjhDOTE3RiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo0MTdEQzAwRTMyOTYxMUU4QUVCNTlDMUFCMjhDOTE3RiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjQxN0RDMDBCMzI5NjExRThBRUI1OUMxQUIyOEM5MTdGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjQxN0RDMDBDMzI5NjExRThBRUI1OUMxQUIyOEM5MTdGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+hOkKRwAAAAZQTFRF////AAAAVcLTfgAAAA5JREFUeNpiYBikACDAAACZAAGHvThgAAAAAElFTkSuQmCC" class="shim" alt="Shim"/>
				<div id="ytplayer"></div>
				<div class="overlay">
					<img src="/images/videos/video-1-anatomy-large.jpg?{{{$version}}}" class="bg" data-id="1" alt="anatomy"/>
					<img src="/images/videos/video-2-fitness-large.jpg?{{{$version}}}" class="bg" data-id="2" alt="fitness"/>
					<img src="/images/videos/video-3-grooming-large.jpg?{{{$version}}}" class="bg" data-id="3" alt="grooming"/>
					<img src="/images/videos/video-4-periods-large.jpg?{{{$version}}}" class="bg" data-id="4" alt="periods"/>
					<img src="/images/videos/video-5-sex-large.jpg?{{{$version}}}" class="bg" data-id="5" alt="sex"/>
					<button @click.prevent="playVideo">PLAY</button>
					<div class="title">
						<div class="label"><span class="light">she</span>talks<span class="light">:</span>@{{videos[selectedVideo].name}}<span class="bar1"></span><span class="bar2"></span></div>
					</div>
				</div>
			</div>
			<div class="inner-fix">
				<div class="videoThumbs">
					<a href="#videos" data-id="1" class="vThumb btn-anchor" @click="selectVideo"><img src="/images/videos/video-1-anatomy-thumb.jpg?{{{$version}}}" alt="Watch video"/></a>
					<a href="#videos" data-id="2" class="vThumb btn-anchor" @click="selectVideo"><img src="/images/videos/video-2-fitness-thumb.jpg?{{{$version}}}" alt="Watch video"/></a>
					<a href="#videos" data-id="3" class="vThumb btn-anchor" @click="selectVideo"><img src="/images/videos/video-3-grooming-thumb.jpg?{{{$version}}}" alt="Watch video"/></a>
					<a href="#videos" data-id="4" class="vThumb btn-anchor" @click="selectVideo"><img src="/images/videos/video-4-periods-thumb.jpg?{{{$version}}}" alt="Watch video"/></a>
					<a href="#videos" data-id="5" class="vThumb btn-anchor" @click="selectVideo"><img src="/images/videos/video-5-sex-thumb.jpg?{{{$version}}}" alt="Watch video"/></a>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => ''])

	@include('main.components._instagram', ['shifted' => 'shifted'])
@endsection

@section('components')

@endsection
