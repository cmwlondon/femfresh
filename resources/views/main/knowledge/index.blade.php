@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="knowledge">
		<div class="outer-fix">
			<div class="inner-fix tar">
				<picture>
				  <source media="(min-width: 961px)" srcset="/images/knowledge/heading.jpg?{{{$version}}}">
				  <img src="/images/knowledge/heading-tall.jpg?{{{$version}}}" class="head-img" alt="Get intimate with your intimate area">
				</picture>
				<h1><span class="h2 p1">Get intimate with</span> <span class="p2">your</span> <br/><span class="p3">intimate</span> <br/><span class="p4">area</span></h1>
				<div class="desc"><p>everything you need to know about the vulva and the vagina.</p></div>
			</div>
		</div>
	</div>

	<div id="expert-advice" class="container bgw" data-ref="knowledge-sections">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="columns">
					{{--
						<div class="col" id="dr-sara-knowledge-hub-item">
						<img src="/images/knowledge/dr-sara.jpg?{{{$version}}}" alt="Dr Sara"/>
						<div class="box" data-box-id="1">
							<p>You shared your questions. Expert gynaecologist, Doctor Sara, has the answers.</p>
							<a href="{!! route('dr-sara') !!}" class="btn">Get&nbsp;the&nbsp;low&#8209;down</a>
						</div>
					</div>
					--}}
					<div class="col" id="ph-balance-knowledge-hub-item">
						<img src="/images/knowledge/ph-scale.jpg?{{{$version}}}" alt="pH Scale"/>
						<div class="box" data-box-id="2">
							<p>What is pH-balance?</p>
							<a href="{!! route('about-ph-balance') !!}" class="btn">Read&nbsp;about&nbsp;pH</a>
						</div>
					</div>
					<div class="col" id="quiz-knowledge-hub-item">
						<img src="/images/knowledge/vulva-or-vagina.jpg?{{{$version}}}" alt="Vulva or Vagina?"/>
						<div class="box" data-box-id="3">
							<p>Do you know your vulva from your vagina? Take our interactive quiz to find out!</p>
							<a href="{!! route('quiz') !!}" class="btn">Take&nbsp;the&nbsp;quiz</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => ''])

	@include('main.components._she-talks', ['shifted' => 'shifted'])
	
@endsection

@section('components')
	
@endsection