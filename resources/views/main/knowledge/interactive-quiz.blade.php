@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container" data-ref="interactive-quiz">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div id="gameVue">
					<h1 :data-state="state"><span class="h2 p1">Let&#39;s get</span> <span class="p2">intimate</span></h1>
					<div id="game" :class="{ready: isReady}" :data-state="state">
						<div id="bgs">
							<div class="bg" data-id="0"></div>
							<div class="bg" data-id="1"></div>
							<div class="bg" data-id="2"></div>
							<div class="bg" data-id="3"></div>
							<div class="bg" data-id="4"></div>
							<div class="bg" data-id="5"></div>
							<div class="bg" data-id="6"></div>
							<div class="bg" data-id="7"></div>
							<div class="bg" data-id="8"></div>
							<div class="bg" data-id="9"></div>
							<div class="bg" data-id="10"></div>
							<div class="bg" data-id="11"></div>
							<div class="bg" data-id="12"></div>
						</div>
						<div class="game-state" data-state="0">
							<p>Our intimate area is made up of many marvellous parts, but can you name them? Many of us ladies aren’t as familiar with our anatomy as we would like to be, especially when it comes to our private parts. There’s no time like the present for a quick multiple choice test to see how much you know. Put your knowledge to the test in our interactive quiz.</p>
							<h2>Do you know <br/>your vulva from your vagina?</h2>
							<div class="button" @click.prevent="proceed"><span>Take the quiz</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="1">
							<h2>Many people often mistake this external area for the vagina</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="1">
								<li data-id="1" @click.prevent="selectAnswer"><span>Uterus</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Vulva</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Clitoris</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="2">
							<h2>@{{ computedResult(1) }}</h2>
							<h3>It’s the vulva!</h3>
							<p>Your vulva comprises all the external organs of your intimate area – including the labia majora, labia minora, clitoris, and the external openings of the urethra and vagina. It’s often confused with the vagina – which is actually inside your body.</p>
							<img src="/images/quiz/quiz-answer1.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="3">
							<h2>This spongy tissue is located at the top of your vulva</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="2">
								<li data-id="1" @click.prevent="selectAnswer"><span>Clitoris</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Urethra</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Cervix</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="4">
							<h2>@{{ computedResult(2) }}</h2>
							<h3>It’s the clitoris!</h3>
							<p>Your clitoris has thousands of nerve endings – more than any other part of your body – and its primary purpose is to make you feel good.</p>
							<img src="/images/quiz/quiz-answer2.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="5">
							<h2>These lips are the vulva’s first line of defence</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="3">
								<li data-id="1" @click.prevent="selectAnswer"><span>Labia Minora</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Labia Majora</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Clitoral Hood</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="6">
							<h2>@{{ computedResult(3) }}</h2>
							<h3>It’s the labia majora!</h3>
							<p>Latin for ‘major lips,’ the labia majora is the vulva’s outer lips and the first line of defence against the outside world – protecting the softer tissues underneath.</p>
							<img src="/images/quiz/quiz-answer3.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="7">
							<h2>A fold of skin that often varies in size, shape and thickness</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="4">
								<li data-id="1" @click.prevent="selectAnswer"><span>Clitoral Hood</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Urethra</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Labia Minora</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="8">
							<h2>@{{ computedResult(4) }}</h2>
							<h3>It’s the clitoral hood!</h3>
							<p>The clitoral hood is a fold of skin that  protects the sensitive clitoris but – aside from protecting the clitoral glands – it stimulates pleasure.</p>
							<img src="/images/quiz/quiz-answer4.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="9">
							<h2>Little factories that release eggs every month</h2>
							<p>So what are they called?</p>
							<ul class="answer-group" data-question="5">
								<li data-id="1" @click.prevent="selectAnswer"><span>Fallopian tubes</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Ovaries</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Mons Pubis</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="10">
							<h2>@{{ computedResult(5) }}</h2>
							<h3>It’s the ovaries!</h3>
							<p>These little organs produce and store eggs that are either fertilised by sperm or flushed out during menstruation. They also produce hormones, including oestrogen, progesterone, and testosterone that control things like your period and pregnancy.</p>
							<img src="/images/quiz/quiz-answer5.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="11">
							<h2>A tiny opening that you pee out&nbsp;of</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="6">
								<li data-id="1" @click.prevent="selectAnswer"><span>Cervix</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Vagina</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Urethra</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="12">
							<h2>@{{ computedResult(6) }}</h2>
							<h3>It’s the urethra!</h3>
							<p>This small opening – where urine comes out – is right below the clitoris. It's hard to see and you can't really feel anything there. Many young women think the clitoris is actually the urethra but don't be fooled.</p>
							<img src="/images/quiz/quiz-answer6.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="13">
							<h2 class="smaller">This cylinder of tissue is the permeable barrier between the uterus and vagina</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="7">
								<li data-id="1" @click.prevent="selectAnswer"><span>Cervix</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Ovaries</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Vulva</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="14">
							<h2>@{{ computedResult(7) }}</h2>
							<h3>It’s the cervix!</h3>
							<p>The cervix divides your vagina and uterus and its purpose is to let menstrual blood out and sperm in. It's also the part that dilates before a woman gives birth.</p>
							<img src="/images/quiz/quiz-answer7.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="15">
							<h2 class="smaller">This canal stretches during childbirth before returning to its starting size.</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="8">
								<li data-id="1" @click.prevent="selectAnswer"><span>Vagina</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Uterus</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Clitoris</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="16">
							<h2>@{{ computedResult(8) }}</h2>
							<h3>It’s the vagina!</h3>
							<p>Located right below your urethral opening, the vagina is a tube that connects your vulva with your cervix and uterus. It's where menstrual blood leaves your body, and where babies pass through during childbirth. </p>
							<img src="/images/quiz/quiz-answer8.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="17">
							<h2>These lips provide a second layer of protection</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="9">
								<li data-id="1" @click.prevent="selectAnswer"><span>Clitoris</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Clitoral Hood</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Labia minora</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="18">
							<h2>@{{ computedResult(9) }}</h2>
							<h3>It’s the labia minora!</h3>
							<p>Labia minora are the inner lips that protect the vulva from dryness, irritation and infections. They often come in a variety of shapes, sizes &amp; skin shades and are as unique as our fingerprints.</p>
							<img src="/images/quiz/quiz-answer9.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="19">
							<h2 class="smaller">A highway where fresh eggs travel after leaving the ovaries.</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="10">
								<li data-id="1" @click.prevent="selectAnswer"><span>Fallopian Tubes</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Birth canal</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Inguinal canal</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="20">
							<h2>@{{ computedResult(10) }}</h2>
							<h3>It’s the fallopian tubes!</h3>
							<p>After an egg is released from the ovary, it makes its way down one of two fallopian tubes towards the uterus where it can be fertilised by sperm.</p>
							<img src="/images/quiz/quiz-answer10.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state question" data-state="21">
							<h2>Where a foetus grows during pregnancy</h2>
							<p>So what’s it called?</p>
							<ul class="answer-group" data-question="11">
								<li data-id="1" @click.prevent="selectAnswer"><span>Birth canal</span></li>
								<li data-id="2" @click.prevent="selectAnswer"><span>Uterus</span></li>
								<li data-id="3" @click.prevent="selectAnswer"><span>Ovaries</span></li>
							</ul>
						</div>
						<div class="game-state answer" data-state="22">
							<h2>@{{ computedResult(11) }}</h2>
							<h3>It’s the uterus!</h3>
							<p>Also known as the womb, the uterus is a safe home where a future baby (should you choose to have one) will live for nine months. It sheds its lining every 28 days(ish) – when there's no pregnancy – otherwise known as getting your period.</p>
							<img src="/images/quiz/quiz-answer11.png?{{{$version}}}" class="diagram" alt=""/>
							<div class="button" @click.prevent="proceed"><span>Next</span>
								@include('main.knowledge.partials._quiz-arrow')
							</div>
						</div>

						<div class="game-state" data-state="23">
							<div class="col">
								<h2>So how <br/>did you <br/>get on?</h2>
							</div>
							<div class="col">
								<p class="result">And more importantly how did you do measure up against everyone else?</p>
								<p class="score">You scored <span>@{{ score }}</span></p>
								<p class="average">Most people score <span>{{ $avgScore }}</span></p>
								<div class="score-text" :data-score="score">
									<p class="result-0-4">A bit of revision wouldn’t go amiss. Check out the results above to get up to speed and give the quiz another shot. </p>
									<p class="result-5-8">Not bad but there’s always room for improvement. Recap on the answers above so that next time you can nail it.</p>
									<p class="result-9-12">Way to go! You clearly know your minora from your majora. Help us spread the word and help us get more intimate with our intimate parts.</p>
								</div>
							</div>
							<div class="outcome-buttons">
								<div class="ob" @click.prevent="doRetry">Try Again @include('main.knowledge.partials._quiz-arrow')</div>
								<div class="ob" @click.prevent="doDiagrams">See Diagrams @include('main.knowledge.partials._quiz-arrow')</div>
								<div class="ob" @click.prevent="doChallenge">Challenge Friends @include('main.knowledge.partials._quiz-arrow')</div>
							</div>

							<div class="diagrams" :class="{visible: isDiagrams}">
								<img src="/images/quiz/quiz-review1.png?{{{$version}}}" class="diagram" alt=""/>
								<img src="/images/quiz/quiz-review2.png?{{{$version}}}" class="diagram" alt=""/>
								<img src="/images/quiz/circle-close.svg?{{{$version}}}" class="bClose" alt="Close" @click.prevent="closeDiagrams"/>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>

	@include('main.components._dr-sara-ph-balance', ['shifted' => 'shifted'])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])

@endsection

@section('components')

@endsection
