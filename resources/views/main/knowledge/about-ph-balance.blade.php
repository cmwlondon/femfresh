@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="about-ph-balance">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<h1><span class="h2 p1">about</span> <span class="p2">pH-balance</span></h1>
				
				<div class="upper">
					<div class="desc">
						<p class="hidden-sm hidden-md"><strong>Scroll to see the pH of various day to day items.</strong></p>

						<p class="mob-first mt2"><strong>Skin science</strong></p>

						<p>Our skin is an amazing barrier against the world. It fights off any infections and protects us from the sun's harmful rays. We have to keep it healthy and look after it, so it can stay on top of its game. The easiest way to keep your skin in tip top condition is to clean and moisturise.</p>

						<p>Skin is different on all parts of your body because each part has a different job. For example, the skin is very thick on our feet, there are more glands on our underarms and our fingertips have more nerve endings. The genital area is for having sex and going to the toilet, so this area is full of glands which make oil, sebum and sweat. It’s got lots of elastic tissue and a good immune system too. It’s important to look after your intimate skin so everything can keep working as it’s supposed to.</p>
					</div>
					
					<p class="hidden vis-sm vis-md mb1"><strong>Scroll to see the pH of various day to day items.</strong></p>
					@include('main.knowledge.partials._ph-balance-embed')
				</div>

				<div class="lower">
					<div class="col-full">
						<p><strong>pH-perfection</strong></p> 

						<p>Believe it or not, but all of our skin is naturally acidic including the genital skin. pH indicates how acidic our skin is and it’s the thin layer of oils on the very top layer of the skin that make sure the surface is at the correct pH. If your pH rises, it can damage cells, cause infection and the skin can become red and dry. </p>

						<p>Some soaps and liquid soaps are strong detergents which have an opposite pH to our skin. They are alkaline, so they react with all our natural oils. Every time we clean ourselves with a soap that’s not pH-balanced we are getting rid of the natural moisturising factors, so eventually our skin starts to play up. This becomes even more important to consider when cleaning intimate skin because the pH level is even lower down there. The skin on your body has a pH level of 5.5, whereas intimate skin has a pH of less than 4.5, making it even more prone to irritation from shower gels and soaps that are not pH-balanced.</p>

						<p><strong>Keeping skin happy</strong></p>

						<p>Looking after skin's natural pH is the key to skin harmony. Cleansers don’t know the difference between the oils containing dirt and the good oils, so gets rid of all of them which causes dryness and irritation. The best cleansing products are soap free, pH-balanced to the right level and moisturising.</p>

						<p>femfresh washes are made from a cleanser that is soap-free and balanced to the unique pH level of intimate skin. Each wash can also be used all over your body, not just on your intimate area, as the pH is safe for all skin. The gentle formulas also contain ingredients that moisturise and soothe skin to always leave it cleansed and cared for.</p>
					</div>

					{{--
					<div class="col-right">
						<div class="inner">
							<p>Dr Clare Patterson is our on-hand dermatologist. An experienced skin surgeon, she develops individual treatment plans designed to bring skin back into balance and is our go-to expert when it comes to giving skin the right care, every day.</p>
							<img src="/images/knowledge/dr-claire-patterson.jpg?{{{$version}}}" alt="Dr Clare Patterson"/>
						</div>
					</div> 
					--}}

				</div>
			</div>
			
		</div>
	</div>
	
	@include('main.components._articles', ['thisArticle' => ''])
	
	@include('main.components._instagram', ['shifted' => 'shifted'])
@endsection

@section('components')
	
@endsection