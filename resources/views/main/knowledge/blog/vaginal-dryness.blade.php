@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/vadr-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">The Basics on Vaginal Dryness</h1>
					</div>

<p>Vaginal dryness is a condition most women will experience at some point in their lives. Oestrogen is a key hormone involved in the production of vaginal discharge and lubrication. It&rsquo;s normal to have some white/clear vaginal discharge, this is a healthy sign that your vagina is cleaning itself and naturally self&#8209;lubricating.</p>

<p>Oestrogen levels drop during the menopause, causing atrophic vaginitis, a condition in which the vaginal mucosa becomes thinner, dryer and more fragile. Amongst others, vaginal dryness can be a big issue for peri and post-menopausal women. But it&rsquo;s not just menopausal women experiencing it, oestrogen levels change during breast feeding, when taking some oral contraceptive pills, antidepressant meds, or chemotherapy. You can also get vaginal dryness if not aroused during sex, if using fragranced soaps, body washes or douches in and around the vagina, and from medical conditions such as diabetes or Sjogren&rsquo;s syndrome.</p>

<p><strong>TOP TIP! To reduce vaginal dryness ensure you are aroused and using a water based lubricant before sex and avoid using harsh fragranced body washes and shower gels which can be irritating to the vagina and vulva.</strong> Be kind to the intimate skin by cleansing with warm water or a pH balanced and unfragranced feminine wash such as <strong>Femfresh 0% Intimate Wash</strong> which gently cleanses and moisturises for those with more sensitive skin. It&rsquo;s vegan-friendly, enriched with Lotus Flower and Bamboo, and works in harmony with your intimate area.  Remember to avoid cleansing or moisturising inside the actual vagina itself as this can strip away the natural vaginal discharge and lubrication and worsen the&nbsp;dryness.</p>

<p>If you&rsquo;ve been trying different things and your symptoms are not improving after a few weeks, are interfering with your daily life or sex life, or if you have any other symptoms then you should speak to your own&nbsp;GP.</p>

					<ol class="refs">
						<li><a href="https://www.nhs.uk/conditions/vaginal-dryness/" target="_blank">https://www.nhs.uk/conditions/vaginal-dryness/</a></li>
						<li><a href="https://patient.info/doctor/Atrophic-Vaginitis" target="_blank">https://patient.info/doctor/Atrophic-Vaginitis</a></li>
						<li><a href="https://www.rcog.org.uk/en/patients/patient-leaflets/treatment-symptoms-menopause/" target="_blank">https://www.rcog.org.uk/en/patients/patient-leaflets/treatment-symptoms-menopause/</a></li>
						<li><a href="https://www.nice.org.uk/guidance/ng23/ifp/chapter/managing-your-symptoms" target="_blank">https://www.nice.org.uk/guidance/ng23/ifp/chapter/managing-your-symptoms</a></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
