@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/peca-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">Period care 101</h1>
					</div>

<p>A period is the part of the menstrual cycle where the uterus lining breaks down and a woman bleeds for a few days. For most women this happens every 28 days, but it can vary between 21 days to 40 days and can be influenced by many factors such as contraception or conditions such as PCOS. Whilst every woman menstrual cycle will be different, it&rsquo;s something we&rsquo;ve all got in&nbsp;common.</p>

<p>Unfortunately, there&rsquo;s still a bit of a taboo when it comes to periods and many women feel dirty when they menstruate. There&rsquo;s an important distinction to make here, yes periods can be a bit messy, but they are completely natural and you are not dirty during your&nbsp;period.</p>

<p>During your period, you don&rsquo;t need to drastically change your cleansing regime. Some women feel they need to clean the menstrual blood away using a douche, which cleans the inside of the vagina and rinses menstrual blood out. The problem here is that firstly it implies menstrual blood is dirty and needs to be rinsed out (which it doesn&rsquo;t) and secondly, overcleaning and douching can disrupt the normal microbiome of the vagina and lead to problems. Menstrual blood is slightly more acidic, but your body knows how to keep the balance in check and naturally keep your microbiome&nbsp;happy.</p>

<p><strong>TOP TIP:  You may wish to cleanse your vulva, but it&rsquo;s still only recommended to do this once per day using warm water or a pH balanced wash such as one from femfresh.</strong> It can be tempting to reach for heavily perfumed products or wipes during your period to keep yourself feeling fresh, but this is unnecessary and can often lead to irritation. Cleansing with water is completely fine, or if you&rsquo;d prefer, use a pH balanced product such as <strong>Femfresh Soothing Wash</strong> which could be perfect whilst on your period as it contains a moisturising probiotic complex formula and is enriched with aloe vera to help keep the microbiome happy and you feeling fresh. I really want to empower you to embrace your period and not feel you need to clean it away, but if you do like to freshen up, Femfresh freshening and soothing wipes are also great to keep in your handbag when on the&nbsp;go!</p>

<p>Always remember, your period is natural and there is nothing dirty about it. If you&rsquo;ve noticed something different which you&rsquo;re worried about, it&rsquo;s always worth speaking to your own doctor&nbsp;about.</p>

					{{--
						<ol class="refs">
						<li><a href="https://www.nhs.uk/conditions/vaginal-dryness/" target="_blank">https://www.nhs.uk/conditions/vaginal-dryness/</a></li>
					</ol>
					--}}
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
