@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/image1.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">The Vagina Dialogue: <span>Keeping your Lady Garden fresh</span></h1>
					</div>
					<p>It's time to bust the taboo on female cleansing! If you speak to your friends about vaginal hygiene routines, you'll probably end up more confused about what the right thing to do is, as every woman seems to have their own routine which they swear by!</p>
					<p>Starting with anatomy, the vagina is the inner canal inside your body leading to your cervix and uterus. The vulva is all the surrounding skin encompassing your whole 'intimate area' including labia and clitoris.</p>
					<p>The vagina itself doesn't actually require any cleaning because it has self-cleansing mechanisms by producing a clear vaginal discharge. The surrounding intimate area, the vulva however may require some gentle cleansing if you wish, but its delicate skin so deserves a kinder cleansing routine than the rest of your body.</p>
					<ol class="refs">
						<li><a href="https://www.nhs.uk/live-well/sexual-health/keeping-your-vagina-clean-and-healthy/" target="_blank">https://www.nhs.uk/live-well/sexual-health/keeping-your-vagina-clean-and-healthy/</a></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
