@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/ui-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">Urinary Incontinence? <span>More normal than you think</span></h1>
					</div>

<p>Many of you reading this may be more familiar with the symptoms of urinary incontinence than you realise… that feeling of worry when you go out – will there be a toilet nearby? When you’ve got to go, you’ve got to go! Or even when you’re having a good old belly laugh with your friends and a little bit of wee comes out&hellip;</p>

<p>Urinary incontinence is when you pass urine without meaning to. It affects loads of women, but the medical stats can’t say exactly how many because far too often, many women feel too embarrassed to admit it or go to their doctor about it, but Dr Frankie is here to bust the taboos!</p>

<p>There are 2 types of urinary incontinence; stress incontinence and urge incontinence.</p>
<ul>
<li><strong>Stress incontinence happens when the muscles of the pelvic floor weaken such as in pregnancy, menopause or in obesity, leading to leaking of urine when you exert yourself such as when you laugh, cough or sneeze.</strong></li>
<li><strong>Urge incontinence is very different, this happens when the muscle of the bladder wall contracts when it’s not supposed to, making it difficult for you to hold urine in so you may feel that when you’ve got to go, you’ve got to go now, often resulting in leaking. This often happens as a result of neurological conditions; there are also even cases of ‘mixed incontinence’ which has features of both.</strong></li>
</ul>
<p>Whatever the cause, urinary incontinence can be really distressing and interfere with your daily life. Many women report it interferes with their social lives; they fear going out in case there isn’t a bathroom close by, but also within their sex life.</p>

<p>There are several ‘self-help’ things you can try at home such as pelvic floor exercises, avoiding caffeine, losing weight and avoiding constipation by eating more fruit & veg, which can add extra pressure on the bladder wall. I would always recommend speaking to your own doctor about any symptoms you are having as they may be able to offer you some specialist treatments.</p>

<p>The statistics show women are embarrassed to talk about this, but chances are your friends are affected by it too. Let’s break down the taboos about urinary incontinence and get you the treatment needed to live your normal life without being worried about leaking urine!</p>

					<ol class="refs">
						<li><a href="https://www.bupa.co.uk/health-information/womens-health/urinary-incontinence-in-women" target="_blank">https://www.bupa.co.uk/health-information/womens-health/urinary-incontinence-in-women</a></li>
						<li><a href="https://www.nhs.uk/conditions/urinary-incontinence/" target="_blank">https://www.nhs.uk/conditions/urinary-incontinence/</a></li>
						<li><a href="https://patient.info/doctor/urinary-incontinence-pro" target="_blank">https://patient.info/doctor/urinary-incontinence-pro</a></li>
					</ol>

				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
