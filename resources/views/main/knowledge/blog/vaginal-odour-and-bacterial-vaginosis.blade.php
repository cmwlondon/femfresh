@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<article>
			<div class="bg-fix bg25"></div>
			<div class="outer-fix">
				<div class="inner-fix tal">
					<div class="panel">
						<div class="head-swap imgleft">
							<img src="/images/blog/bacterial-vaginosis-image.jpg?{{{$version}}}" alt="" class="head"/>
							<header>
								<h1 class="h2 rfs">Vaginal Odour &amp; Bacterial Vaginosis</h1>
							</header>
						</div>

	<p>This is a taboo topic we don’t like talking about, but <strong>some vaginal odour is entirely normal</strong>. The vagina and vulva have a unique microbiome with bacteria called lactobacilli keeping the pH slightly more acidic than the rest of the body. This protects the vagina and prevents growth of other&nbsp;organisms.</p>

	<p><strong>The internal vagina is self-cleaning and produces a thin vaginal discharge daily to clean itself.</strong> This, coupled with the acidic pH and daily perspiration, can mean your intimate area may have a slight odour and that is completely normal and everyone has one. You should not be cleaning the inside of the vagina as this can lead to pH imbalances, but can clean the vulva (the outside bit only) with warm water or a pH balanced wash such as one from&nbsp;emfresh.</p>

	<p><strong>It’s important to get to know your own normal.</strong> Sometimes you might notice a change in your vaginal odour. This is likely due to a disruption of the pH balance, which can lead to an overgrowth of other commensal bacteria leading to infections such as Bacterial Vaginosis (BV) or thrush. BV itself is not a sexually transmitted infection, but more an overgrowth of the natural bacteria that lives on your skin. It’s extremely common and most women will have had BV at some point. You might also notice a more ‘fishy’ odour and a thin, grey vaginal&nbsp;discharge.</p>

	<p>It’s important <strong>if you notice a change in odour or discharge, you seek medical attention</strong> as BV is easily treated with antibiotics and topical gels. It’s especially important to get treated if you are&nbsp;pregnant.</p>

	<p><strong>Sometimes the pH of the skin can also be disrupted by menstrual blood, sex, change in diet, change in washing detergent or&nbsp;sweat.</strong></p>

	<p>Here at femfresh, we absolutely don’t want to perpetuate female insecurities about hygiene or odour. Some odour is normal and we want you to embrace that, but we understand that when the natural pH balance gets disrupted, perhaps due to BV, the change in odour can be an insecurity for some women. Whilst you are being investigated and treated for any possible infections such as BV, Femfresh Solutions Odour Eliminating Gel may be helpful in balancing the healthy vaginal pH to help eliminate odours and relieve feminine&nbsp;discomfort.</p>

	<p>If you’ve been checked for infection and everything is healthy down below then embrace your odour and don’t try to wash it&nbsp;away!</p>

	<p>We are advocates for women having a choice of how they wish to approach feminine&nbsp;hygiene.</p>

	<p>Dr Frankie</p>


	{{--
	<p class="cb"><strong>Tips for dealing with Vulvodynia:</strong></p>

	<ul>
		<li></li>
	</ul>
	--}}
						{{--
						<ol class="refs">
							<li><a href="https://www.nhs.uk/conditions/vaginal-dryness/" target="_blank">https://www.nhs.uk/conditions/vaginal-dryness/</a></li>
						</ol>
						--}}
					</div>
				</div>
			</div>
		</article>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
