@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<article>
			<div class="bg-fix bg25"></div>
			<div class="outer-fix">
				<div class="inner-fix tal">
					<div class="panel">
						<div class="head-swap imgleft">
							<img src="/images/blog/common-feminine-hygiene-myths-image.jpg?{{{$version}}}" alt="" class="head"/>
							<header>
								<h1 class="h2 rfs">5 common feminine hygiene myths</h1>
							</header>
						</div>



<p>When it comes to feminine hygiene, you can bet there are some myths floating around! It’s important to clear up what’s what, not only to bust some of the taboos but also to ensure you are looking after your intimate area properly and safely.</p>

<p class="myth2"><span>Myth 1: You have to cleanse your intimate skin with a wash</span><br>The vaginal and vulval skin is composed of a careful balance of friendly bacteria, years and viruses that all live-in harmony as the microbiome.</p>

<p>It’s absolutely fine to cleanse the surrounding skin around the vaginal canal (the vulva) and many women choose to, but it’s important you don’t overclean the intimate area as this can disrupt the microbiome and the natural pH of the skin and lead to symptoms of irritation and sometimes even infection.</p>

<p>Here at Femfresh, we are advocates of personal choice- your body, your choice! If you prefer to cleanse with just warm water – go for it! But many women prefer to cleanse this area with a dedicated wash. That’s fine too – but make sure you avoid perfumed washes, bubble baths and shower gels as these can disrupt that delicate microbiome. Opting for something pH balanced such as the Femfresh Soothing Wash, which is kinder to the intimate skin (being 100% soap free) and won’t disrupt the integrity of the microbiome as it is pH balanced. We would also only recommend washing once per day maximum.</p>

<p class="myth2"><span>Myth 2: You need to clean the inside of the vaginal canal</span><br>This one’s a no no! The inside of the vaginal canal itself is self-cleaning and produces a vaginal discharge to clean from the inside out, so you absolutely do not need to clean the inside bit. This is known as the term ‘douching’ which is a sure way to disrupt the microbiome and can lead to irritation or even infections such as thrush and bacterial vaginosis. Leave nature to do its course and only cleanse the outer skin (the vulva) when cleansing.</p>

<p class="myth2"><span>Myth 3: Sexually Transmitted infection (STIs) are related to personal hygiene</span><br>False! Unfortunately, I still hear of men and women cleansing their genitalia with harsh cleaning products after having sexual intercourse to reduce the risk of catching an STI. This is not helpful and actually might cause more problems! STIs are nothing to do with personal hygiene and anyone who is sexually active is at risk of contracting one. The best way to minimise your risk is to use barrier contraception such as condoms, get regular tests and don’t have unprotected sex until both you and your partner both have negative tests.</p>

<p class="myth2"><span>Myth 4: Vaginal discharge means I have a yeast infection</span><br>Yeast infections (also known as ‘thrush’) are common, and most people will develop one at some point in their lives. It’s true that thrush can cause a thick, white ‘cottage cheese’ like discharge, dry skin and itching, but there are also many other causes of vaginal discharge. Remember, some vaginal discharge is completely normal and nothing to be embarrassed about. The amount and consistency of your discharge can vary with your hormonal cycle too. But if you notice a change in your own normal, then it could be something else. Yes one of the common causes is thrush, but it could also be caused by bacterial vaginosis (think thin, grey discharge with a fishy odour), an STI* (trichomonas can sometimes cause a green discharge), a retained product (such as a tampon) or inflammation of the cervix (may contain blood). If you notice any change in your discharge please speak to your own doctor.</p>

<p class="myth2"><span>Myth 5: Removing pubic hair is more hygienic</span><br>This is another common myth! The removal of pubic hair, whether that be shaving, waxing, epilating or laser hair removal should be a personal choice. We support women doing whatever makes them feel comfortable and confident – but we want to make sure you are educated by the facts and not by the current trends and taboos!</p>

<p>A recent study published in JAMA Dermatology revealed 59% of women shave their pubic hair for hygiene purposes, despite medical facts. There’s little risk from removing pubic hair, but it was actually designed in evolutionary terms to prevent friction and infection of the genitalia. In fact, removal via shaving or waxing can come with its own risks such as ingrown hairs, infected hair follicles and hyperpigmentation/scarring etc. So if you do decide to do it out of personal (and informed) choice, make sure to regularly replace your razors, using a soothing shaving cream and keep the area free from irritation.</p>

<p>Hope this was useful.</p>
<p>Dr Frankie</p>
<p>*Remember the most common STIs, chlamydia and gonorrhea often show no symptoms at all! </p>
						{{--
						<ol class="refs">
							<li><a href="link URL" target="_blank">link URL</a></li>
						</ol>
						--}}
					</div>
				</div>
			</div>
		</article>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
