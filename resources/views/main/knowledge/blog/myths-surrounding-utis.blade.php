@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap imgleft">
						<img src="/images/blog/myths-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">Myths surrounding UTIs</h1>
					</div>

<p>Many of you reading this might be all too familiar with that uncomfortable feeling of a UTI brewing which always seems to come on in the middle of the night, leaving us scrolling the internet for advice whilst waiting for the GP to open in the morning&hellip; but how much of the UTI advice out there is fact and how much is&nbsp;fiction?</p>

<p>A Urinary Tract Infection (UTI) is an infection causing inflammation of the bladder (cystitis) and is usually caused by a bacteria called E.coli which is actually a contaminant from our&nbsp;bowel.</p>

<p><strong>Myth 1: Only women experience UTIs.</strong> It's a myth that only women experience UTIs – men can get them too although less frequently. It's more common for women to experience UTIs than men because the urethra (the tube we urinate from) is shorter than in men, so bacteria can track up into the bladder more&nbsp;easily.</p>

<p>Women will suffer differently, some women may get some mild discomfort or abdominal pain when urinating, other women may experience excruciating pain, needing to urinate what feels like every 5 minutes, but being faced with just a few drops of burning&nbsp;urine.</p>

<p><strong>Myth 2: Drinking cranberry juice cures a UTI.</strong> Another common myth surrounding UTIs is that drinking cranberry juice cures them because cranberry juice contains proanthocyanidins which is thought to stop the bacteria sticking to the bladder wall. Whilst drinking fluids can be a good way to flush out the bacteria from the bladder, cranberry juice has unfortunately not been shown to prevent or speed up recovery from a UTI. Some UTIs may recover on their own, but many women will need antibiotics from their GP, so it's recommended you speak to your doctor if you have symptoms. Especially if you experience a high fever or back pain as this could be a sign of the infection spreading further up the urinary&nbsp;tract.</p>

<p><strong>Myth 3: a UTI is caused by sex.</strong> Sex can sometimes exacerbate bladder inflammation due to friction and many women like to wee after sex to flush any bacteria away from the urethral opening, but sex itself does not cause UTIs and even women who are not sexually active may suffer from them. To clarify, a UTI (urinary tract infection) is not the same as an STI (sexually transmitted&nbsp;infection).</p>

<p>So, what to do if you experience UTI symptoms? Drink plenty of water, take some simple painkillers such as paracetamol or ibuprofen for the pain (if required), call your doctor for advice (you might need antibiotics) and look after yourself – plenty of rest, hot water bottle and give yourself some&nbsp;TLC.</p>

<p>You might also find your vulva and urethra is more sensitive due to the inflammation so taking care when cleansing, avoiding harsh soaps and washing with warm water or a pH balanced wash such as the Femfresh Soothing wash will be kinder to your&nbsp;skin.</p>

<p>Dr Frankie</p>

					{{--
						<ol class="refs">
						<li><a href="https://www.nhs.uk/conditions/vaginal-dryness/" target="_blank">https://www.nhs.uk/conditions/vaginal-dryness/</a></li>
					</ol>
					--}}
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
