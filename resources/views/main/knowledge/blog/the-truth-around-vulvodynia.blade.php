@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap imgleft">
						<img src="/images/blog/vulvodynia1-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">The truth around vulvodynia</h1>
					</div>
<p>Everything may look normal down below, but as many as 1 in 10 women may be suffering from Vulvodynia, a persistent unexplained pain around the vulva and in the vagina, which may feel sore like a burning, stinging, or throbbing and may be exacerbated by touch such as washing, using sanitary products or&nbsp;sex.</p>
<p>The cause of vulvodynia is largely unknown, but it is thought to be due to pain receptors around the intimate area being hypersensitive to touch. This can be caused by surgery, childbirth, trapped nerves, but most often no cause is&nbsp;found.</p>
<p>It&rsquo;s important we have these conversations as it can affect many aspects of a woman&rsquo;s life such as mood and&nbsp;relationships.</p>
<p class="cb"><strong>Tips for dealing with Vulvodynia:</strong></p>
<ul>
	<li><strong>Avoid irritants</strong> which could exacerbate the sensitivity, such as fragranced body washes, soaps or sanitary products. Opt for using water, emollients or pH balanced feminine products such as the <strong><em>Femfresh Soothing Wash</em></strong>, which is p-H balanced for the intimate skin and dermatologically and gynaecologically tested as well as being enriched with hydrating cranberry and cornflower&nbsp;extracts.</li>
	<li><strong>Wear loose cotton underwear and avoid tight clothing</strong> which could irritate the&nbsp;skin</li>
	<li><strong>Avoid wearing underwear at&nbsp;night</strong></li>
	<li><strong>Apply soothing cool packs</strong> to the area to soothe&nbsp;discomfort</li>
	<li>
		<p><strong>Focus on reducing general life stress</strong> as we know stress and mental wellbeing can affect women&rsquo;s experience of&nbsp;vulvodynia.</p>
		<p class="indent"><strong>Tips for reducing stress</strong> include mindfulness, prioritising 8 hours good quality sleep, regular exercise and consulting your&nbsp;doctor</p>
	</li>
	<li><strong>Don&rsquo;t avoid sex completely</strong>, but instead communicate your concerns to your partner to make it comfortable for you! Some women may also benefit from applying a topical local anaesthetic gel to the vulval area about 20 minutes before having&nbsp;sex.</li>
	<li><strong>Pelvic floor exercises</strong> can help relax the muscles around the vagina and may reduce tension and pain in the&nbsp;area</li>
</ul>
<p>There are no definitive treatments for vulvodynia, and it&rsquo;s important to ensure your symptoms are not due to infection, which can be easily treated. In some cases, women will be prescribed a type of anti-depressant medication are also good for treating ‘neuropathic pain&rsquo; which is pain caused by hypersensitivity of nerve&nbsp;endings.</p>
<p>Living with a long-term painful condition like vulvodynia can be emotionally challenging and I would encourage all women experiencing this to speak to their GP about how they may be feeling and join support networks such as Vulval Pain&nbsp;Society.</p>
<p>Dr Frankie</p>
					{{--
						<ol class="refs">
						<li><a href="https://www.nhs.uk/conditions/vaginal-dryness/" target="_blank">https://www.nhs.uk/conditions/vaginal-dryness/</a></li>
					</ol>
					--}}
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
