@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/microbiome-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">What is Skin Microbiome?</h1>
					</div>

<p>The word ‘microbiome’ has become increasingly trendy as we learn more about gut health. But did you know, the skin is also home to a diverse microbiome of bacteria, viruses, yeasts and other microbes which all exist in harmony with each other or did you know there are actually more bacteria living on the skin than grains of sand on a beach? Yes, that’s over 100 trillion microbes that live on your skin!</p>

<p>This large and diverse microbiome consists of the perfect balance of ‘good’ and ‘bad’ bacteria to maintain an optimal pH, which serves a physical barrier to prevent the invasion of harmful bacteria or pathogens through the skin (The pH is a measure of how acidic or alkaline something is). Under normal conditions and when in balance, these microbes do not cause harm and actually help regulate some natural body processes such as functioning of the immune system and preventing infections.</p>

<p>Each of our bodies has its own unique microbiome, cultivated from birth and built from our genes and environmental exposures such as diet and medications. It’s important not to forget that the microbiome spans across all our skin, including the female intimate area, the vagina and vulval skin.</p>

<h2>Balancing the Microbiome of intimate skin</h2>

<p>What do women need to know? The microbiome of the female intimate area is actually slightly different to the rest of the skin, it is occupied by a larger population of lactobacillus bacteria which maintain the pH at a slightly more acidic level of 3.5-4.5. This acts as a protective barrier. Lots of daily exposures can knock off this harmonious balance of the microbes in the microbiome and alter the pH of the intimate area. For example, if you take antibiotics, it can kill some of the lactobacillus ‘good’ bacteria and allow other microbes such as Candida species to grow and thrive, leading to yeast infections, commonly known as ‘thrush’.</p>

<p>Lots of daily exposures can disrupt this perfect balance such as menstruation, sex, diet, antibiotics, clothing and cleansing. However, one of the most common culprits for disrupting the balance of bacteria is totally preventable! Excessive washing, particularly with soaps or fragranced products can make the pH of the vagina less acidic, allowing more anaerobic bacteria to grow. This imbalance of vaginal bacteria can lead to an infection called BV (Bacterial Vaginosis) which is a common complaint in many women.</p>

<h2>TOP TIP! - Treat the skin of your vagina and vulva with as much love and care as you would your face.</h2>

{{-- /our-range/ultimate-care-soothing-wash --}}
<p>Either rinse with warm water, or if you prefer to use an intimate cleanser, opt for a pH neutral one such as the <strong>Femfresh Soothing Wash</strong> which is clinically tested by both gynaecologists and dermatologists for daily use. It is enriched with a probiotic complex to help support the feminine microbiome and keep everything healthy, without knocking this precious microbiome off balance!</strong>

<p>*If you develop any symptoms such as new vaginal discharge, including change in smell or colour, change in the intimate skin, lumps, bumps or sores, pain during sex, bleeding after sex or intermenstrual bleeding then it is advised to consult your own doctor for personalised advice and treatment if required.</p>

					<ol class="refs">
						<li><a href="https://www.nature.com/articles/nrmicro.2017.157" target="_blank">https://www.nature.com/articles/nrmicro.2017.157</a></li>
						<li><a href="https://www.dermnetnz.org/topics/the-gut-microbiome-in-skin-disease/" target="_blank">https://www.dermnetnz.org/topics/the-gut-microbiome-in-skin-disease/</a></li>
						<li><a href="https://patient.info/sexual-health/vaginal-discharge-female-discharge/bacterial-vaginosis" target="_blank">https://patient.info/sexual-health/vaginal-discharge-female-discharge/bacterial-vaginosis</a></li>
					</ol>

				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
