@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/ph-balance.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">pH Balance on Intimate Skin</h1>
					</div>
					<p>COPY</p>
					{{--
					<ol class="refs">
						<li><a href="https://www.rcog.org.uk/en/patients/patient-leaflets/skin-conditions-of-the-vulva/" target="_blank">https://www.rcog.org.uk/en/patients/patient-leaflets/skin-conditions-of-the-vulva/</a></li>
					</ol>
					--}}
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
