@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/menopause-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">Menopause and your intimate area: <span class="lhfix">what to expect during Menopause</span></h1>
					</div>

					<p>The menopause is the time in a woman&rsquo;s life when she stops having periods and is no longer able to get pregnant naturally. The menopause is a natural part of ageing and levels of the female hormone, oestrogen, decline resulting in periods becoming irregular and less frequent until they stop altogether. This usually happens between the age of 45-55 and varies between women. In some women they may stop more&nbsp;suddenly.</p>
					<p>Not all women going through menopause develop symptoms, but many women have a pretty tough time experiencing symptoms such as hot flushes and night sweats which can massively disturb sleep, reduced sex drive and mood&nbsp;swings.</p>
					<p>But did you know, the hormonal changes of menopause can also affect the microbiome and pH of the vaginal and vulval skin? The drop in oestrogen during menopause can cause changes in the vaginal mucosa, called ‘atrophic vaginitis&rsquo; whereby the intimate skin becomes thinner, dryer and more fragile. Changes in the balance of good and bad bacteria alter the pH balance of the skin, which can result in dryness and&nbsp;irritation.</p>
					<p>During this time when everything may feel a little more sensitive down there, treating the intimate skin with some TLC may improve your symptoms of vaginal irritation or dryness. Fragranced soaps and body washes can be harsh and irritating, so wash with either just warm water, or if you prefer to use a cleanser, choose feminine-specific products such as Femfresh 0% Intimate Wash, which is soap and fragrance free, and suitable for daily use if the skin down there is feeling a little sensitive.  Keeping the vagina lubricated with a water-based lubricant gel can also reduce&nbsp;discomfort.</p>
					<p>It&rsquo;s advisable to speak to your GP if you&rsquo;re affected by any of the symptoms of menopause as there are lots of treatments available, including Hormone Replacement Therapy (HRT), and topical oestrogen creams, which may relieve some of these&nbsp;symptoms.</p>

					<ol class="refs">
						<li><a href="https://www.nhs.uk/conditions/menopause/" target="_blank">https://www.nhs.uk/conditions/menopause/</a></li>
						<li><a href="https://patient.info/doctor/Atrophic-Vaginitis" target="_blank">https://patient.info/doctor/Atrophic-Vaginitis</a></li>
						<li><a href="https://www.dermnetnz.org/topics/atrophic-vulvovaginitis/" target="_blank">https://www.dermnetnz.org/topics/atrophic-vulvovaginitis/</a></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
