@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<article>
			<div class="bg-fix bg25"></div>
			<div class="outer-fix">
				<div class="inner-fix tal">
					<div class="panel">
						<div class="head-swap imgleft">
							<img src="/images/blog/dont-fear-the-smear-image.jpg?{{{$version}}}" alt="" class="head"/>
							<header>
								<h1 class="h2 rfs">Don’t fear the smear!</h1>
							</header>
						</div>



<p>The smear test can be a cause of anxiety for many women and I think some of that is due to not understanding what the results mean.

<p>If you are a woman between the ages of 25 – 64 you should have been invited to attend your smear test as part of the NHS cervical cancer screening programme. The smear test checks the health of your cervix. Most women age 25 – 49 have this test every 3 years, if aged 50 – 64 then it will be 5 years. It’s not a particularly pleasant experience, but it’s usually over in minutes and attending your appointments regularly is the most effective way to protect yourself from developing cervical cancer.</p>

<p class="myth2"><span>Myth 1: The smear test is a test for cancer</span><br>
During your smear test, the practitioner will take a small sample of cells from your cervix and check your sample for the presence of a virus called Human Papilloma Virus (HPV). HPV is really common and most people who are sexually active will catch it at some point. There are over 100 types of HPV and most of them cause no problems. However, certain high-risk strains of HPV can cause some cancers – the most well-known being cervical cancer, so it’s important to pick up if you have it.</p> 

<p>It’s a myth that the smear test is a check for cervical cancer, it is designed to pick-up high-risk cases or early changes to cervical cells and detect them before any cancer develops.</p> 

<p class="myth2"><span>Myth 2: HPV is an STI</span><br>
During the smear test, a small sample of cells will be taken from your cervix. These are then tested for high-risk cases of HPV. When you get your smear test results in the post, it will state whether your results are ‘HPV positive’ or ‘HPV negative’.</p>

<p>An HPV positive result is often a big anxiety for many women and some people are worried they have an STI. It’s important to understand that HPV is a very common virus and most people who have been sexually active will catch HPV at some point in their lives. Whilst it’s important to practice safe sex to avoid catching a sexually transmitted infection, most people who are sexually active will get the HPV virus at some point, so whilst it is associated with sexual activity, it isn’t considered an STI, per say.</p> 

<p class="myth2"><span>Myth 3: If I get a positive HPV result, I will develop cervical cancer</span><br>
In the majority of cases – the virus will cause no symptoms and most people naturally clear it from their body within 2 years without it causing any problems, but some people can get infected with a high-risk type of HPV and will not be able to clear it. It can cause the cells in the cervix to become abnormal and in some cases, can even cause cervical cancer. Whilst HPV strains 16 and 18 are responsible for over 70% cases of cervical cancer, most people who get these strains will not develop cervical cancer.</p> 

<p>If these high-risk types of HPV are found, the sample is then checked for any changes in the cells of your cervix. These can then be treated before they get a chance to turn into cervical cancer. You should receive these results within 2 weeks of having your smear test and it’s important you understand the results to avoid any unnecessary anxiety!</p>

<p class="fact"><strong>What do the results mean?</strong><br>
If your sample is found to be positive for HPV, but the cells are normal, you will be invited to attend your next smear test earlier – at 1 year rather than 3 years, to check that the HPV has gone and there are no abnormal cells.</p>

<p>If your sample is found to be positive for HPV, and abnormal cells are found on your sample, you will be referred to hospital for further investigations called a Colposcopy and it’s likely you will have these cells removed from your cervix, to prevent them developing into cervical cancer later in life. About 1 in 20 smear tests samples will contain abnormal cells, and most of these won’t turn into cancer. The cells are however often removed to reduce the chances.</p> 

<p>If your sample is negative for HPV, you will return back to routine recall and be invited for your next smear test in 3 years’ time (or 5 years if you are above age 50 years).</p> 

<p>In some cases, you may receive a letter saying your results are ‘inadequate’. Again, this isn’t a reason to panic. All this means is there aren’t enough cervical cells present for examination under the microscope and you’ll need to attend for a repeat smear after 3 months.</p>

<p class="myth2"><span>Myth 4: I had the HPV vaccination so I don’t need to attend my smear test</span><br>
Girls and boys aged 12 to 13 years are offered the HPV vaccine as part of the NHS vaccination programme. The vaccine is called Gardasil and protects against the 4 most common types of HPV; types 6 and 11 which are responsible for most cases of genital warts, and 16 and 18 which cause around 70% of cases of cervical cancer. Even if you’ve had both doses of the vaccine, it’s still vitally important you attend your smear tests regularly.</p>

<p>Stay safe and get tested when the time comes.</p>

<p>Dr Frankie</p>
						{{--
						<ol class="refs">
							<li><a href="link URL" target="_blank">link URL</a></li>
						</ol>
						--}}
					</div>
				</div>
			</div>
		</article>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
