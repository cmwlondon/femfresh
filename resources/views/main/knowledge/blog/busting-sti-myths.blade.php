@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<article>
			<div class="bg-fix bg25"></div>
			<div class="outer-fix">
				<div class="inner-fix tal">
					<div class="panel">
						<div class="head-swap imgleft">
							<img src="/images/blog/busting-sti-myths-thumbnail.jpg?{{{$version}}}" alt="" class="head"/>
							<header>
								<h1 class="h2 rfs">Busting Myths on&nbsp;STI's</h1>
							</header>
						</div>

<p>Today, we are busting myths concerning the most common STI’s (Sexually Transmitted Infections).
<p class="myth"><span>MYTH</span> - You will know if you have an STI</p>
<p class="fact"><strong>FACT</strong> - Surprising to many, some of the most common STIs often don’t have any symptoms and even doctors won’t know if you have one. In the UK, the most common STI is chlamydia, but 70% of women and 50% will have no symptoms and won’t know they have it until they get tested. For gonorrhoea, only 50% of women show symptoms. Men are more likely to show symptoms such as discharge, redness or pain on urination.</p>
<p>It’s important you get regular tests and always use barrier contraception (e.g condoms) until both you and your partner have had negative STI tests.</p>
<p>It’s really important to get regular tests even if you don’t have any symptoms because untreated STIs can lead to pelvic inflammatory disease, increase risk of ectopic pregnancies and may even affect fertility later down the line.</p>
<p class="myth"><span>MYTH</span> - Only young people get them</p>
<p class="fact"><strong>FACT</strong> - Anyone who is sexually active can catch an STI. The rates have been increasing over the past few years in the over 65s! Remember – contraception isn’t only about preventing pregnancy!</p>
<p class="myth"><span>MYTH</span> - Only people who ‘sleep around’ get STIs</p>
<p class="fact"><strong>FACT</strong> - This is a myth, and a damaging one because it contributes to the stigma surrounding STIs. Anyone who has been sexually active can catch an STI. Whether that’s 1 time or 100 times. Don’t forget STIs can be passed on through all kinds of sex – oral and anal sex too.</p>
<p class="myth"><span>MYTH</span> - Once you’ve treated them, they can never come back</p>
<p class="fact"><strong>FACT</strong> - It depends on the STI.  Most STIs can be cured if you catch and treat them early. For example, chlamydia and gonorrhoea - once treated the infection won’t appear again unless you catch it again from someone who is positive. If you get tested positive for an STI, both you and your partner should get treated for it, to avoid reinfection</p>
<p>Other STIs like genital herpes or HIV are with you for life because these are viruses that live in your body. Whilst you can take anti-viral medication to keep them at bay, they may come back again, particular during a period where you might be a bit run down. Taking the therapy is still very important as it slows down the progression of the disease and works to also reduce the chance of transmitting the disease to any future partners.</p>
<p class="myth"><span>MYTH</span> - You can catch STIs from a toilet seat</p>
<p class="fact"><strong>FACT</strong> - There is no evidence that you can catch any of the sexually transmitted infections from a surface – they are spready via sexual contact and/or bodily fluids.</p>
<p class="myth"><span>MYTH</span> - If you have good personal hygiene, you reduce the risk of getting an STI</p>
<p class="fact"><strong>FACT</strong> - Sexually transmitted infections have nothing to do with personal hygiene and only develop due to contact!</p>
<p>To find more information on STIs and how to address them, visit the NHS website for advice – click <a href="https://www.nhs.uk/conditions/chlamydia/symptoms/" target="_blank" title="View the NHS Chlamydia symptoms page - opens in a new window">here</a>.</p>
<p>Dr Frankie</p>
						<ol class="refs">
							<li><a href="https://www.nhs.uk/conditions/chlamydia/symptoms/" target="_blank">https://www.nhs.uk/conditions/chlamydia/symptoms/</a></li>
						</ol>
					</div>
				</div>
			</div>
		</article>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
