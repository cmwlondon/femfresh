@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/icsex-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">Intimate care &amp;&nbsp;sex</h1>
					</div>

<p>Today&rsquo;s topic is around intimate care after sex. Movies and pornography can be pretty misleading when it comes post-sex intimate care and hygiene, so I thought it was time to bust some taboos around this.

<p>First things first, the vagina is self-cleaning. It naturally produces a thin, white vaginal discharge to clean itself and keep the microbiome balanced. (if you&rsquo;re new here please check out my previous blog&rsquo;s on the ever so important vulva and vaginal microbiome <a href="/knowledge-hub/period-care" title="Period care 101" style="font-weight:700;">HERE</a>. I hear about some women frantically rushing to clean themselves after sex to reduce their risk of infection (I&rsquo;ve even been told by a patient they wash with Dettol after sex to reduce their risk of STIs!). This is a MYTH&hellip; and a potentially dangerous one!</p>

<p>Firstly, cleansing after sex does not reduce your risk of contracting a sexually transmitted infection (STI). The only way to prevent one is to use barrier contraception (such as condoms), and only when both you and your partner have a recent negative STI test should you stop using barrier contraception.</p>

<p>The other important point to remember is that using harsh fragranced soaps, or even antibacterial washes on your vulva or vagina can lead to a disruption of the bacteria in the microbiome, imbalance the pH and lead to symptoms such as irritation, or infections such as Bacterial Vaginosis (BV) or thrush. You do not need anything fancy to cleanse down below – keep it simple. Keep your perfumed body washes for the waist up and wash your vulva with either warm water or if you&rsquo;d like to use a wash, choose a pH balanced wash such as the Femfresh daily wash which is kind to the microbiome, 100% soap free as well as being pH balanced.</p>

<p><strong>TOP TIP:  If you do wish to clean up after sex then you can also use a feminine hygiene wipe</strong> such as the <strong>femfresh Biodegradable and Flushable Daily Wipes</strong> which are enriched with aloe and calendula extracts as well as pH balanced meaning these wipes are gentle to the skin, which might be irritated after sex&hellip; but you do not need to be excessively using these wipes all throughout your regular days as overcleaning can disrupt the microbiome!</p>

<p>Other tips to reduce your risk of infections after sex are</p>
<ul>
	<li>Wee after sex to reduce your risk of bacteria tracking up the urethra and causing a urinary tract infection</li>
	<li>Don&rsquo;t share sex toys</li>
	<li>Clean your sex toys</li>
	<li>Use barrier contraception unless both you and your partner have both had a recent negative STI test</li>
	<li>Never try and clean the inside of the vaginal canal, only cleanse the vulva if you wish.</li>
</ul>
<p>Keep your sex safe&hellip; and your microbiome happy! </p>

<p>Dr Frankie</p>

					{{--
						<ol class="refs">
						<li><a href="https://www.nhs.uk/conditions/vaginal-dryness/" target="_blank">https://www.nhs.uk/conditions/vaginal-dryness/</a></li>
					</ol>
					--}}
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
