@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/image2.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">Cleansing your Vagina 101</h1>
					</div>
					<p>The intimate area has a barrier of natural bacteria on the skin called the microbiome, which helps keep the vagina's pH balance (how acidic the vagina is) at an even level, which helps keep the balance of bacteria healthy.</p>
					<p class="halfwwidth">Many women will choose to cleanse with just warm water, but if you wish to cleanse then save your luxurious fragranced products for the waist up and avoid fragranced soaps, which can be harsh and irritating. Opt for a pH balanced gentle wash such as Fem Fresh Daily Intimate Wash which won’t strip away that natural healthy bacteria on the skin. Also note that over-washing can cause skin irritation and discomfort. The Royal College of Obstetricians and Gynaecologists (RCOG) recommends only washing the vulva area once a day, avoiding sponges or flannels which could irritate the skin.</p>
					<ol class="refs">
						<li><a href="https://www.rcog.org.uk/en/patients/patient-leaflets/skin-conditions-of-the-vulva/" target="_blank">https://www.rcog.org.uk/en/patients/patient-leaflets/skin-conditions-of-the-vulva/</a></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
