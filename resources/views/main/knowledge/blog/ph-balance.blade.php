@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/ph-balance.jpg?{{{$version}}}" alt="" class="desktop head"/>
						<img src="/images/blog/ph-balance-mobile.jpg?{{{$version}}}" alt="" class="mobile head"/>
						<h1 class="h2">pH Balance on Intimate Skin</h1>
					</div>
					<p>Did you know the skin all over our body is home to millions of natural bacteria which makes up our skin microbiome? The bacteria all live in harmony to keep our skin barrier healthy and maintain an optimum pH level. The pH of something tells you how acidic or alkaline something is, on a scale of 0-14, with numbers less than 7 being acidic and greater than 7 being&nbsp;alkaline.</p>
					<h2>Why is the pH of your vagina important?</h2>
					<p>The same goes for the vagina and the surrounding intimate skin called the vulva, which is maintained largely by lactobacillus-dominant bacterial flora, at a slightly acidic pH of 3.5-4.5.</p>
					<p class="halfwwidth">Keeping this vaginal pH in its optimal slightly acidic state is important for women’s health as it keeps the healthy bacteria thriving and the harmful bacteria at bay. When the pH gets disrupted, it can result in inflammation and vaginal dryness which can be uncomfortable, or even bacterial or yeast infections which may require&nbsp;treatment.</p>
					<h2>How can I keep the vaginal pH in harmony?</h2>
					<p>There are lots of factors that can disrupt the vaginal pH, but most commonly it’s caused by using harsh fragranced soap products on the intimate area. Treat your vagina with the same care as you would treat the skin of your face, it deserves its own individual cleansing routine. Femfresh products are specially developed to be kind to the intimate skin and are clinically approved by both gynaecologists and&nbsp;dermatologists.</p>
					<p>Using products on the vulva and vagina that are not specially formulated can disrupt the harmony of the bacterial flora and result in irritation, discomfort, yeast and bacterial infections. You wouldn’t use your luxury fragranced shower gel on your face, so avoid using it on your vaginal skin. Opt for either just warm water or a gentle pH balanced wash such as Femfresh 0% Sensitive Intimate Wash when cleansing, which is soap and fragrance free and made of 94% natural&nbsp;ingredients.</p>
					<h2>What can disrupt the optimal pH?</h2>
					<p>It’s not just body washes and bubble baths that can disrupt your vaginal pH, but also other lifestyle factors such as menstruation, sex, stress, diet, antibiotics and medications, the laundry detergent you use and the material of your underwear. If you notice a change down below, have a think back to what may have changed in your daily routine and keep a diary to identify the culprit&nbsp;irritant!</p>
					<p>Of course, every woman is different and what works for your friend might not work for you. If you notice any persistent changes from your own normal, including a change in the skin, discharge or smell, then it’s always important to speak to your own&nbsp;doctor.</p>
					<ol class="refs">
						<li><a href="https://journals.lww.com/greenjournal/Fulltext/2011/04000/Vaginal_pH_Balanced_Gel_for_the_Control_of.24.aspx" target="_blank">https://journals.lww.com/greenjournal/Fulltext/2011/04000/Vaginal_pH_Balanced_Gel_for_the_Control_of.24.aspx</a></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
