@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/image3.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">Listening to your vagina</h1>
					</div>
					<p>Whilst we’ve covered that gently cleansing the vulval skin can be ok, vaginal douching is never recommended by medical professionals. Vaginal douching involves washing the inside of the vagina with water or cleansing products. Douching has been shown to disrupt the harmony of this natural balance of friendly bacteria, can cause irritation and discomfort, and even contribute to infections like thrush or BV. One study also showed women who did vaginal douching when pregnant, were more likely to give birth prematurely.</p>

					<p class="halfwwidth">Remember, some clear vaginal discharge is entirely normal! The amount and consistency may change with your hormones throughout your menstrual cycle with ovulation, pregnancy and menopause but you don’t need to be alarmed or change your cleansing routine. I encourage all women to become aware of what their own ‘normal’ is and seek advice if you notice change such as change in colour or smell of discharge, or you develop other symptoms as irritation, rashes, lumps or sores, bleeding and pain during urination or sexual intercourse, then you should contact your doctor.</p>

					<ol class="refs">
						<li><a href="https://www.nature.com/articles/s41591-019-0450-2" target="_blank">https://www.nature.com/articles/s41591-019-0450-2</a></li>
					</ol>
{{--
					<p class="refs-source">All data retrieved from live articles on 25th&nbsp;January&nbsp;2018.</p>
--}}
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
