@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="blogpost">
		<div class="bg-fix bg25"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/blog/vaod-image.jpg?{{{$version}}}" alt="" class="head"/>
						<h1 class="h2">The Basics on Vaginal Odour </h1>
					</div>

<p>Let&rsquo;s start by getting one thing straight – it is completely natural for your vagina to have some discharge and odour. The vagina actually produces a clear discharge as a method of cleaning itself. It is when there is a change from your normal that you need to take&nbsp;note.</p>

<p>If you&rsquo;ve read my previous blog posts on the intimate skin, you&rsquo;ll know the vagina and vulva is occupied by a carefully balanced microbiome ecosystem of good and bad bacteria, which maintain a slightly acidic environment. When the balance is upset, the area can become less acidic, which can manifest as a change in vaginal odour or discharge and may be a sign of&nbsp;infection.</p>

<p>The most common cause for a change in vaginal odour is a common infection called Bacterial Vaginosis (BV).  A common misconception is that BV is caused by poor hygiene. In fact, it is often excessive cleaning of the intimate area, often with harsh soaps or fragranced products, that disrupts the natural balance of bacteria making the area less-acidic and allowing certain bacteria to thrive. Other culprits of disturbing the natural acidity of the vagina and vulva is menstruation, use of sex toys or wearing nylon tights or non-cotton underwear for long periods. The result is often an unpleasant fishy odour and a greyish-white thin, watery discharge. Top tips to minimise BV is to wear breathable cotton underwear and only wash once per day, using either just warm water or an un-fragranced wash such as Femfresh 0% Intimate wash, which is pH balanced to keep everything in harmony and suitable for sensitive skin. Femfresh 0% Intimate Wash is also soap free and made up of 94% natural origin ingredients, making it the perfect intimate skincare&nbsp;product.</p>

<p>BV is not the only cause of a change in normal vaginal odour; STIs, menstruation, a forgotten tampon and sweating are all other common culprits. It&rsquo;s common to notice a change in smell during your period, as menstrual blood has a slightly less acidic pH. Remember this is completely normal so avoid being tempted to over-wash or use fragranced products and it&rsquo;s still recommended to only wash once per&nbsp;day.</p>

<p>Whilst I hope this article helps clear up some of the common causes of abnormal vaginal odours and gives you some do&rsquo;s and don&rsquo;ts, I want the takeaway message to be that some vaginal odour is completely normal, nothing to be embarrassed and only if it&rsquo;s different for you do you need to do something about&nbsp;it!</p>

<p>*Whilst I mention STIs can cause a change in odour or discharge, scarily, the most common STIs, chlamydia and gonorrhoea, often show no symptoms. If you do develop a change in symptoms then make sure to visit your GP or sexual health clinic, but it&rsquo;s advised to have regular&nbsp;check&#8209;ups.</p>

					<ol class="refs">
						<li><a href="https://www.nhs.uk/conditions/bacterial-vaginosis/" target="_blank">https://www.nhs.uk/conditions/bacterial-vaginosis/</a></li>
						<li><a href="https://patient.info/sexual-health/vaginal-discharge-female-discharge/bacterial-vaginosis" target="_blank">https://patient.info/sexual-health/vaginal-discharge-female-discharge/bacterial-vaginosis</a></li>
						<li><a href="https://patient.info/news-and-features/vaginal-odours-to-be-aware-of" target="_blank">https://patient.info/news-and-features/vaginal-odours-to-be-aware-of</a></li>
						<li><a href="https://www.rcog.org.uk/en/patients/patient-leaflets/skin-conditions-of-the-vulva/" target="_blank">https://www.rcog.org.uk/en/patients/patient-leaflets/skin-conditions-of-the-vulva/</a></li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._blogcrosslinks', [])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
