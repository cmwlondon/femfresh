@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="article">
		<div class="bg-fix bg20"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/articles/periods-dos-and-donts.jpg?{{{$version}}}" alt="Period - dos and donts" class="head"/>
						<h1 class="h2">Period - dos and donts</h1>
					</div>
					<p><strong>We all know that having your period is no walk in the park.</strong></p>
					<p>The cramps, bloating, headaches, cravings and fatigue affects almost all women, for 5-10 days each month <sup>(1)</sup>. Throw in the heaps of online advice claiming what you should and shouldn&rsquo;t be doing &ndash; and it gets hard to know what to listen to.</p>
					<p>There are plenty of period myths out there too, some more extreme than others, but it can sometimes be difficult to sort fiction from truth. We&rsquo;ve tackled three top period myths that can leave us struggling to judge what we should or shouldn&rsquo;t be doing when on our period.</p>
					<p>Myth 1 <strong>&ldquo;Exercise is bad for you when on your period.&rdquo;<sup>(2)</sup></strong></p>
					<p>NOT TRUE: Not only can you exercise during your period, but there&rsquo;s also evidence to say that you should! Light exercise can help relieve cramps as it increases the blood flow to the uterus which is &lsquo;beneficial in pain relief&rsquo;. Yoga and swimming are great for this as they stretch your abdominal muscles. The endorphins released during workouts also help you to relax, reducing stress and anxiety. <sup>(3,4)</sup> So, there you have it!</p>
					<p>Myth 2 <strong>&ldquo;You can&rsquo;t get pregnant while on your period.&rdquo;<sup>(2)</sup></strong></p> 
					<p>NOT TRUE: Getting pregnant on your period is less common but this doesn&rsquo;t mean it&rsquo;s impossible.</p>
					<p>Women are most likely to conceive just before and during ovulation &ndash; usually 14 days before your period starts <sup>(5)</sup>. But, if you have a shorter than average cycle you may ovulate just after your period. So why do you need to know this? Sperm can survive in your uterus for up to five days and so if you have sex during your period, sperm could still be present and ready to fertilise around the time you ovulate. <sup>(6,7)</sup></p>
					<p>The bottom line? Your period isn&rsquo;t a 100% effective natural contraceptive so always take extra precautions if you&rsquo;re not planning for a baby.</p>
					<p>Myth 3 <strong>&ldquo;You lose a lot of blood during your period&rdquo;</strong></p>
					<p>NOT TRUE: Average blood loss during a single period is around 5-12 teaspoons <sup>(8)</sup>. So no need to panic. You may find that your period can leave you feeling in need of a refresh throughout the day, especially when you have an unexpected leak. <a href="{!! route('25-wipes') !!}">femfresh soothing and freshening wipes</a> are ideal to use to freshen up your intimate area between showers and when changing your intimate sanitary protection.</p>
					<ol class="refs">
						<li><a href="https://www.webmd.com/women/pms/features/pms-signs-symptoms" target="_blank">https://www.webmd.com/women/pms/features/pms-signs-symptoms</a></li>
						<li><a href="https://www.lovelibra.co.nz/periods/its-not-true/" target="_blank">https://www.lovelibra.co.nz/periods/its-not-true/</a></li>
						<li><a href="http://www.sheknows.com/health-and-wellness/articles/949189/reasons-you-should-exercise-during-your-period" target="_blank">http://www.sheknows.com/health-and-wellness/articles/949189/reasons-you-should-exercise-during-your-period</a></li>
						<li><a href="https://www.livestrong.com/article/479293-how-does-exercising-affect-your-period/" target="_blank">https://www.livestrong.com/article/479293-how-does-exercising-affect-your-period/</a></li>
						<li><a href="https://www.babycentre.co.uk/ovulation" target="_blank">https://www.babycentre.co.uk/ovulation</a></li>
						<li><a href="http://uk.clearblue.com/pregnant/myths-about-getting-pregnant" target="_blank">http://uk.clearblue.com/pregnant/myths-about-getting-pregnant</a></li>
						<li><a href="https://www.babycenter.com/404_can-you-get-pregnant-during-your-period_1460117.bc" target="_blank">https://www.babycenter.com/404_can-you-get-pregnant-during-your-period_1460117.bc</a></li>
						<li><a href="https://www.nhs.uk/conditions/periods/" target="_blank">https://www.nhs.uk/conditions/periods/</a></li>
					</ol>
					<p class="refs-source">All data retrieved from live articles on 25th&nbsp;January&nbsp;2018.</p>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => $articleSlug])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
