@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="article">
		<div class="bg-fix bg20"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/articles/irritation-down-there.jpg?{{{$version}}}" alt="Irritation down there?" class="head"/>
						<h1 class="h2">Irritation down there?</h1>
					</div>
					<p><strong>Vaginal itching and vaginal irritation can leave you feeling off your A-game, but it&rsquo;s not always a sign of a serious problem.</strong></p>
					<p>A first port of call is to take a look at the chemical substances that your vulva (your intimate skin) may be coming into contact with. Biological washing powders, scented toilet papers and even condoms are known to irritate delicate vulval skin. Your everyday soaps and shower gels could also be a trigger for intimate skin irritation, because they&rsquo;re not specially formulated for the <a href="{!! route('about-ph-balance') !!}">unique pH</a> of this very sensitive skin. To combat this, switch to a hypoallergenic washing powder to clean your undies and try a <a href="{!! route('daily-intimate-wash') !!}">pH-balanced intimate wash</a> to care for and soothe your most sensitive vulval skin. One thing to remember is that intimate washes are designed for your external intimate skin (your vulva) and shouldn&rsquo;t be used as a douche to clean the inside of your vagina (your internal organ). We <a href="https://www.nhs.uk/Livewell/vagina-health/Pages/keep-vagina-clean.aspx" target="_blank">don&rsquo;t recommend douching</a>, it&rsquo;s best to let the vagina naturally clean itself. Not sure about the difference between the Vulva and Vagina &ndash; <a href="{!! route('quiz') !!}">take our quiz</a> to test yourself on female anatomy.</p>
					<p>Another trigger can be a change in hormones, particularly that comes with the <a href="https://www.nhs.uk/conditions/menopause/" target="_blank">menopause</a>, <a href="{!! route('article', ['slug' => 'periods-dos-and-donts']) !!}">periods</a> and <a href="https://www.nhs.uk/conditions/pregnancy-and-baby" target="_blank">pregnancy</a> which can disrupt your intimate skin&rsquo;s natural pH. This in turn can lead to vaginal dryness and irritation. Although not a cause for concern, this can be uncomfortable so you may want to ease vaginal dryness with a specially designed vaginal moisturiser.</p>
					<p>If your vaginal irritation is combined with other symptoms such as cottage cheese like discharge or soreness/ stinging during sex or when you pee, then you could be suffering from an infection such as <a href="https://www.nhs.uk/conditions/thrush-in-men-and-women/" target="_blank">thrush</a>, which effects 3 out 4 women in their life <sup>(1,2)</sup>. If your symptoms also include odour then you could be suffering from <a href="https://www.nhs.uk/conditions/bacterial-vaginosis/" target="_blank">bacterial vaginosis</a> which will require treatment. If you&rsquo;re at all concerned or discover you&rsquo;re suffering from any of these symptoms it&rsquo;s best to chat about them with your GP and get them treated.</p>

					<ol class="refs">
						<li><a href="https://www.webmd.boots.com/women/guide/vaginal-itching-burning-irritation" target="_blank">https://www.webmd.boots.com/women/guide/vaginal-itching-burning-irritation</a></li>
						<li><a href="https://www.nhs.uk/conditions/thrush-in-men-and-women/" target="_blank">https://www.nhs.uk/conditions/thrush-in-men-and-women/</a></li>
					</ol>
					<p class="refs-source">All data retrieved from live articles on 25th&nbsp;January&nbsp;2018.</p>

				</div>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => $articleSlug])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
