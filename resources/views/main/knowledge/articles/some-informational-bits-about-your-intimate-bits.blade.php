@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="article">
		<div class="bg-fix bg22"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/articles/informational-bits.jpg?{{{$version}}}" alt="Feminine Care: Some informational bits about your intimate bits." class="head"/>
						<h1 class="h2">Feminine Care:</h1>
					</div>
					<h2 class="h3 mt1">Some informational bits about your intimate bits.</h2>
					<p><strong>It may not be top of your list, but your feminine hygiene routine is worth some thought.</strong></p>
					<p><a href="{!! route('article', ['slug' => 'periods-dos-and-donts']) !!}">Periods</a>, peeing, sexual intercourse, <a href="{!! route('article', ['slug' => 'intimate-sweating-no-sweat']) !!}">sweat</a>, (even what we eat and drink), can all upset the natural balance of our intimate skin which can lead to unwelcome odours, itching, discomfort and even <a href="https://www.nhs.uk/conditions/vaginal-discharge/" target="_blank">abnormal discharge</a> and infections <sup>(1)</sup>. Sure, our intimate area has a natural self-cleaning system but with today&rsquo;s lifestyles changing, (tight fitting clothes &amp; more vigorous indoor exercise), this natural system could use some back-up to help keep the natural balance of our intimate skin feeling fit and healthy. <sup>(2)</sup></p>
					<p class="col5"><strong>Here are 4 tips to help you keep your vulva and intimate area feeling fresh, healthy and comfortable.</strong></p>
					<p class="col5"><strong>Wipe Right</strong></p>
					<p class="mt2">After a visit to the bathroom, make a habit of wiping your intimate skin front to back. This will remove any excess urine, sweat build up and (if menstruating) period discharge. Avoid scented cloths to avoid upsetting this more sensitive skin!</p>
					<p class="col5 div-line"><strong>Cleanse Right</strong></p>
					<p class="mt2">When you want to use more than water to cleanse your intimate skin, choose a specially designed <a href="{!! route('daily-intimate-wash') !!}">intimate wash</a>. Using scented body soaps and shower gels could cause dryness and irritation, so try to use an intimate wash that&rsquo;s specially designed for the unique pH-balance of intimate skin <sup>(4.5)</sup>. Our expert gynaecologist <!-- Dr Sara Matthews,--> tells us, &ldquo;When talking about intimate washes, it is important to recognise that we aren&rsquo;t talking about douching at all, i.e. putting water into the vagina itself. femfresh is not a douche and I never recommend cleaning inside the vagina, but keeping the vulval skin healthy helps the inside too.&rdquo;</p>
					<p class="col5 div-line"><strong>Eat Right</strong></p>
					<p class="mt2">Did you know that the foods we eat can help to keep our intimate area healthy? Greek yogurt contains probiotics which can help to fight off bad bacteria. Salmon can be good for menstrual pain. Green tea is known for its antioxidant benefits which means it can reduce the risk of bladder infection. Sweet potatoes, carrots and any orange fruit or veg can even be libido boosting!</p>
					<p class="col5 div-line"><strong>Drink Right</strong></p>
					<p class="mt2">Water is a magic ingredient. Drink lots of it. Aside from keeping you hydrated, it will flush out toxins. If your pee is dark yellow with an odour you should be guzzling more H20.<sup>(3)</sup></p>

					<ol class="refs">
						<li><a href="http://www.refinery29.uk/vaginal-infections-facts" target="_blank">http://www.refinery29.uk/vaginal-infections-facts</a></li>
						<li><a href="https://www.nhs.uk/Livewell/vagina-health/Pages/keep-vagina-clean.aspx" target="_blank">https://www.nhs.uk/Livewell/vagina-health/Pages/keep-vagina-clean.aspx</a></li>
						<li><a href="http://www.dailymail.co.uk/femail/article-5056347/Foods-eat-healthier-vagina-better-sex-life.html" target="_blank">http://www.dailymail.co.uk/femail/article-5056347/Foods-eat-healthier-vagina-better-sex-life.html</a></li>
					</ol>
					<p class="refs-source">All data retrieved from live articles on 25th&nbsp;January&nbsp;2018.</p>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => $articleSlug])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
