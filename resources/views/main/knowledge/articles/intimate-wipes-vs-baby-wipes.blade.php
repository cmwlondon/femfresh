@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="article">
		<div class="bg-fix bg23"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/articles/intimate-wipes-vs-baby-wipes.jpg?{{{$version}}}" alt="Intimate wipes vs baby wipes" class="head"/>
						<h1 class="h2">Intimate wipes vs baby wipes</h1>
					</div>
					<p><strong>Wiping your intimate skin regularly is recommended especially after using the bathroom, when you&rsquo;ve finished a <a href="{!! route('article', ['slug' => 'intimate-sweating-no-sweat']) !!}">workout</a>, post sex or when you&rsquo;re on your <a href="{!! route('article', ['slug' => 'periods-dos-and-donts']) !!}">period</a>.</strong></p>
					<p>Aside from making you feel clean and fresh, a frequent <a href="{!! route('25-wipes') !!}">wipe</a> will help promote a healthy, bacteria-free intimate area.</p>
					<p>But here comes the million-dollar question, &ldquo;What should I use to wipe when I need more than toilet paper? Baby wipes?&rdquo;</p>
					<p>Our answer is, &ldquo;preferably not&rdquo;.</p>
					<p>When you need something with a bit more moisture than toilet paper, a natural default can be baby wipes but these often contain strong fragrances that could irritate your intimate skin and cause the exact effects that you were trying to avoid in the first place. Because of these reasons, we recommend leaving baby wipes for babies&rsquo; bottoms and instead using a specialised feminine wipe such as, femfresh <a href="{!! route('25-wipes') !!}">Freshening &amp; Soothing wipes</a>. Unlike baby wipes, these specially formulated intimate wipes are designed to match the unique pH-balance of intimate adult skin – to keep the <a href="{!! route('about-ph-balance') !!}">skin&rsquo;s natural balance</a> in check.</p>
				</div>
				
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => $articleSlug])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
