@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="article">
		<div class="bg-fix bg21"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/articles/intimate-sweating.jpg?{{{$version}}}" alt="Intimate sweating? No sweat!" class="head"/>
						<h1 class="h2">Intimate sweating?<br/>No sweat!</h1>
					</div>
					<p><strong>Intimate sweating is a normal everyday occurrence for every woman on the planet.</strong></p>
					<p>It&rsquo;s certainly not something that should make us blush. We should however be aware of the effects this can have on our body as this is the ideal environment for bacteria to thrive. This can not only cause irritation and itchiness, but could lead to infections and odour. <sup>(1)</sup></p>
					<p>When you feel the effects of sweating, we recommend that you wipe your intimate area with a specially designed <a href="{!! route('25-wipes') !!}">feminine wipe</a> &ndash; as opposed to <a href="{!! route('article', ['slug' => 'intimate-wipes-vs-baby-wipes']) !!}">baby wipes</a> or scented toilet paper &ndash; to reduce the risk of irritation and infection.</p>
					<p>But this is just one solution. Is there anything else that can be done before or after exercise to keep our intimate skin cared for? Glad you asked.</p>
					<ul>
						<li>Lace and silk underwear is a bit &lsquo;ooh-la-la&rsquo; but it won&rsquo;t help when it comes to sweating. Best avoid these when exercising and on hot days. Rather, choose a breathable material such as cotton. It&rsquo;ll keep you cool and reduce sweating. You can also try underwear that are specially designed to deal with sweat &ndash; <a href="http://www.sweatybetty.com" target="_blank">Sweaty Betty</a> have a great range without VPL for when you&rsquo;re working it.</li>
						<li>After a few laps in the pool or a particularly vigorous HIIT session, make sure to change into clean, dry clothing straight away &ndash; hanging around in sweaty/damp clothes can allow bacteria to develop. If you can&#39;t change straight away, use a femfresh intimate wipe to freshen up and care for skin.</li>
						<li>Using powder on hot days or when exercising is a great way to soak up sweat. But make sure to use a powder that&rsquo;s specially designed for your intimate skin. Something like femfresh <a href="{!! route('re-balance-powder') !!}">Re-Balance intimate powder</a> would be perfect.</li>
					</ul>

					<ol class="refs">
						<li><a href="http://metro.co.uk/2017/05/10/lets-talk-about-the-sweaty-vagina-triangle-6617535/" target="_blank">http://metro.co.uk/2017/05/10/lets-talk-about-the-sweaty-vagina-triangle-6617535/</a></li>
					</ol>
					<p class="refs-source">All data retrieved from live articles on 25th&nbsp;January&nbsp;2018.</p>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => $articleSlug])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
