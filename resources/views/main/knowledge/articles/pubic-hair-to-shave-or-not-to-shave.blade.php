@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="article">
		<div class="bg-fix bg20"></div>
		<div class="outer-fix">
			<div class="inner-fix tal">
				<div class="panel">
					<div class="head-swap">
						<img src="/images/articles/pubic-hair.jpg?{{{$version}}}" alt="Pubic hair - to shave or not to shave?" class="head"/>
						<h1 class="h2">Pubic hair - to shave or not to shave?</h1>
					</div>
					<p><strong>For centuries, trends in the grooming of pubic hair have gone round in circles.</strong></p>
					<p>From the Egyptians removing it all with pumice stones and flint (ouch!) to the &lsquo;70s bush&rsquo; (low maintenance) and then how could we forget the infamous Brazilian wax in the late 90&rsquo;s (a la Gwyneth et al) <sup>(1)</sup>, women now have so many options for when it comes to de-fuzzing their intimate skin.</p>
					<p>We say, do it your way &ndash; trim it, shave it or go au-naturel &ndash; just do what makes you feel comfortable. It&rsquo;s your skin after all. Just remember, if you do shave or trim, your intimate care regime becomes even more important to avoid nasties such as ingrown hairs and the dreaded razor bumps. {{-- There are products out there to care for intimate skin on these occasions such as femfresh <a href="{!! route('2-in-1-shower-shave-cream') !!}">2in1 shower and shave cream</a>.  It&rsquo;s <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> for use on intimate skin and specifically designed to give you a super smooth shave, whilst giving a burst of freshness. To reduce the risk of redness and razor bumps after shaving, grab some <a href="{!! route('post-shave-balm') !!}">femfresh post-shave balm</a>. It helps minimise in-grown hairs and soothes skin. --}}</p>

					<ol class="refs">
						<li><a href="http://www.rebelcircus.com/blog/love-pubic-hair-trends-throughout-history/" target="_blank">http://www.rebelcircus.com/blog/love-pubic-hair-trends-throughout-history/</a></li>
					</ol>
					<p class="refs-source">All data retrieved from live articles on 25th&nbsp;January&nbsp;2018.</p>

				</div>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => $articleSlug])

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')

@endsection
