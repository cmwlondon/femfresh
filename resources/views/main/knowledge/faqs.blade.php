@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="faqs-head">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<img src="/images/faqs/question-mark.png?{{{$version}}}" class="head-img" alt="Frequently asked questions">
				<h1><span class="h2 p1">frequently asked</span> <span class="p2">questions</span></h1>
				<div class="desc"><p>When? Where? What? How? Below we&#39;ve answered your most popular questions about the femfresh range and caring for your intimate area.</p></div>
			</div>

			<div id="searchGroup" class="inner-fix tal">
				<div id="search-faqs">
					<input type="text" placeholder="SEARCH FAQS" class="search-input" v-model="terms" @keyup.enter="doSearch"/>
					<input type="button" value="" class="m-glass" @click.prevent="doSearch"/>
					<a href="#" id="bReset" class="reset hidden" @click.prevent="resetSearch">Reset</a>
				</div>
				<div id="search-categories">
					<p>Which category can we help with?</p>
					<ul class="cats">
						<li data-group="skin-care"><a href="#intimate" class="btn-anchor">Intimate Skin</a></li>
						<li data-group="washes"><a href="#washes" class="btn-anchor">Washes</a></li>
						<li data-group="wipes"><a href="#wipes" class="btn-anchor">Wipes</a></li>
						<li data-group="shaving"><a href="#shaving" class="btn-anchor">Shaving</a></li>
						<li data-group="deodorant"><a href="#deodorants" class="btn-anchor">Deodorants</a></li>
					</ul>
					<ul class="expanders">
						<li><a href="#faqs" class="btn-anchor" id="faqs-open-all">Open all</a></li>
						<li><a href="#faqs" class="btn-anchor" id="faqs-close-all">Close all</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container" data-ref="faqs">
		<div class="outer-fix" id="faqs">
			<p id="noResults" class="hidden">Sorry no results found</p>

			<div id="faqsWrapper" class="inner-fix">
				<h2 id="intimate" class="heading" data-group="skin-care">INTIMATE SKIN CARE</h2>

				<ul class="faq-list" data-group="skin-care">
					<transition name="filtered">
					<li class="faq" data-id="1" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">What is intimate skincare?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>When it comes to our skin, we all use different products for different parts of our bodies. Just like shampoo could irritate your face and body lotion may leave your hair feeling less than perfect, using the wrong products on your intimate vulval skin &ndash; that protects your vagina &ndash; may result in itching or irritation. That&#39;s where intimate skincare can help.</p>
							<p>The <a href="{!! route('range') !!}">femfresh range</a> is specifically designed to love your bits to bits. It&#39;s a specifically designed collection with formulas designed to protect your vagina&#39;s natural environment, avoid irritation and leave you feeling pHAB!</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="2" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">Is femfresh tested by gynaecologists?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>Yes. All <a href="{!! route('range') !!}">femfresh products</a> are dermatologically and gynaecologically tested by experts. They&#39;re also hypoallergenic, soap free and alcohol free.</p>
						</div>
					</li>
					</transition>
				</ul>

				<h2 id="washes" class="heading" data-group="washes">FEMFRESH INTIMATE WASHES</h2>

				<ul class="faq-list" data-group="washes">
					<transition name="filtered">
					<li class="faq" data-id="1" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">Is femfresh suitable for cleaning inside my vagina?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>femfresh intimate washes should never be used to cleanse the inside of your vagina, which is also known as <a href="https://www.nhs.uk/Livewell/vagina-health/Pages/keep-vagina-clean.aspx" target="_blank">douching</a>.</p>
							<p>Douching is a method of spraying water or specific douche formulas upward inside the vagina to wash and flush out secretions. There is much discussion around vaginal douching amongst health experts and we recommend that you discuss your specific health needs with your doctor.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="2" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">Can I use femfresh on other parts of my body?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p><a href="{!! route('range') !!}#washes">femfresh intimate washes</a> are pH-balanced and specifically designed for use on your intimate skin but they can also be used safely on other external parts of the body.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="3" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">How come there are different washes in the femfresh range? What are they for?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>All our <a href="{!! route('range') !!}#washes">intimate washes</a> are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
							<p>femfresh intimate washes come in four different formulas so you can choose the perfect care to suit your intimate skin. The intimate washes range includes <a href="{!! route('daily-intimate-wash') !!}">Daily Intimate Wash</a> with a hint of soothing aloe vera to keep your intimate skin happy all-day long.</p>
							<p>The Ultimate Care range consists of  <a href="{!! route('ultimate-care-soothing-wash') !!}">Soothing Wash</a> and <a href="{!! route('ultimate-care-active-wash') !!}">Active Wash</a>. The Pure Wash with lactic acid and caring aloe vera is fragrance free and specially formulated for those with sensitive skin. Our Soothing Wash has a moisturising formula with a probiotic complex, and is enriched with aloe vera and cranberry extracts best known for their healing and hydrating properties. The Active Wash provides all day, feel go</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="4" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">How often can I use femfresh intimate washes?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>Each femfresh wash is suitable for everyday use on every part of your body.</p>
						</div>
					</li>
					</transition>
				</ul>

				<h2 id="wipes" class="heading" data-group="wipes">WIPES</h2>

				<ul class="faq-list" data-group="wipes">
					<transition name="filtered">
					<li class="faq" data-id="1" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">What are the benefits of using femfresh intimate wipes, rather than toilet tissue or baby wipes?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>Using <a href="{!! route('25-wipes') !!}">femfresh intimate wipes</a> can help to maintain a healthy <a href="{!! route('about-ph-balance') !!}">pH-balance</a>, whilst giving you that longer lasting feeling of freshness. The gentle formula, with calendula &amp; soothing aloe extracts, is perfect for a quick refresh when you don&#39;t have access to water.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="2" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">When would I use femfresh intimate wipes?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>Whether you&#39;ve been working out at the gym, dancing at a festival or simply just want a refresh during the day, the gentle pH-balanced formula with calendula &amp; soothing aloe extracts is perfect for busy lifestyles and on the go freshness. They&#39;re also ideal for when you&#39;re on your <a href="{!! route('article', ['slug' => 'periods-dos-and-donts']) !!}">period</a> as they fit <a href="{!! route('25-wipes') !!}">discreetly in your bag</a> and can quickly and effectively remove blood from on and around your vulval skin.</p>
						</div>
					</li>
					</transition>
				</ul>

				<h2 id="shaving" class="heading" data-group="shaving">SHAVING RANGE</h2>

				<ul class="faq-list" data-group="shaving">
					<transition name="filtered">
					<li class="faq" data-id="1" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">What is the best way to shave my bikini line?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>There are lots of different ways to take care of your bikini line and now, femfresh has developed a new <a href="{!! route('2-in-1-shower-shave-cream') !!}">2in1 Shower and Shave Cream</a> which has been specifically formulated to care for your delicate intimate skin when shaving. The moisturising formula reduces redness, irritation and razor bumps that shaving your bikini area often results in. Enriched with moisturising almond oil and hydrating argan oil, it&#39;s so gentle, it can also be used as an intimate wash.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="2" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">How can I care for my bikini line post-shaving?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p><a href="{!! route('post-shave-balm') !!}">femfresh post-shave balm</a> is part of the new product range designed to care for your intimate skin during your shaving routine. This skin loving, <a href="{!! route('about-ph-balance') !!}">pH-balanced cream</a> has been specifically formulated to calm and soothe delicate skin post hair removal, and will leave your intimate skin feeling beautiful, soft and smoother for longer. Enriched with blackcurrant oil to soothe, aloe vera to calm and sweet almond oil to moisturise, the formula can be used daily to minimise ingrown hairs, reduce razor bumps and redness.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="3" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">What makes the femfresh shave range different to other shave products?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>As with all the products in the femfresh range, the <a href="{!! route('range') !!}#shaving">shaving collection</a> is pH-balanced and specially formulated to care for and protect your delicate intimate skin. Each product is hypoallergenic, and gynaecologist and dermatologist approved &ndash; so gentle they can be used every day.</p>
						</div>
					</li>
					</transition> 
				</ul>

				<h2 id="deodorants" class="heading" data-group="deodorant">DEODORANT</h2>

				<ul class="faq-list" data-group="deodorant">
					<transition name="filtered">
					<li class="faq" data-id="1" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">What is &lsquo;MULTIActif freshening complex&rsquo;, found in femfresh Ultimate Care Active Freshness Deodorant?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>The MULTIActif complex includes ingredients such as Calendula extract and silver ions which are specially designed to give you both immediate and long lasting fresh for up to 12 hours.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="2" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">Why would I use an intimate deodorant?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>Intimate deodorant is specially formulated for use on your delicate intimate skin. A quick spritz around your intimate area or directly on to underwear will give you that long-lasting feeling of freshness. All thanks to our unique MULTIActif freshening complex.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="3" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">When is the best time to use femfresh Freshness Deodorant, and is it for everyday use?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>Our intimate deodorant range has a gentle formula which can be used every day at any time of the day.</p>
						</div>
					</li>
					</transition>

					<transition name="filtered">
					<li class="faq" data-id="4" :show="show" :filtered="!filtered" v-if="!filtered">
						<h4 @click="toggle">Are intimate deodorants safe to use?</h4>
						<img src="/images/faqs/plus.svg?{{{$version}}}" class="plus"/>
						<img src="/images/faqs/minus.svg?{{{$version}}}" class="minus"/>
						<div class="ans" :style="{'max-height':styleHeight}">
							<p>Our intimate deodorant range is safe to use every day around your external intimate area or directly on to underwear, but should not be used inside the vagina, as this can upset your natural intimate pH-balance.</p>
						</div>
					</li>
					</transition>
				</ul>
			</div>
		</div>

	</div>
@endsection

@section('components')

@endsection
