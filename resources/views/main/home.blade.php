@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	{{-- banner carousel --}}
	<div class="container first bg1666" data-ref="home-1">
		<div class="test-slick">

			<div class="outer-fix home_default">
				<div class="inner-fix">
					<div class="zoom-photo">
						<div class="init-slow"><img src="/images/home/header.jpg?{{{$version}}}" alt="it's just skin"/></div>
					</div>
					<div class="el1"><div class="init">it&rsquo;s just skin</div></div>
					<div id="el_head_text">
						<div class="el2">
							<p>Your intimate skin is skin too. <br/>It&rsquo;s just different to the rest of your body.</p>
							<p>All femfresh products are specially designed to care for your delicate vulval skin and keep you feeling fresher for longer.</p>
						</div>
					</div>
					<div class="btn-group" id="el_head_button">
						<div class="xinit"><a href="{!! route('about') !!}" class="btn">What is femfresh?</a></div>
					</div>
					<div class="el3"><div class="init"><img src="/images/home/love.svg?{{{$version}}}" class="el_love_it" alt="LOVE"/></div></div>
					<div class="el4"><div class="init"><img src="/images/home/it.svg?{{{$version}}}" class="el_love_it" alt="IT"/></div></div>
					
				</div>
			</div>

			<div class="outer-fix home_ffs">
				<div class="inner-fix">
					<div class="zoom-photo">
						<div class="init-slow"><img src="/images/home/fs/banner.jpg?{{{$version}}}" alt="welcome our new health expert!" class="d" /><img src="/images/home/fs/banner.jpg?{{{$version}}}" class="m" alt="welcome our new health expert!"/></div>
					</div>
					
					<div id="el_head_text">
						<div class="el2">
							<p>Take control of your feminine health and&nbsp;balance.</p>
							<p>Our specialized range for the specific needs of your intimate&nbsp;skin.</p>
						</div>
					</div>
					<div class="btn-group" id="el_head_button">
						<div class="init"><a href="{!! route('range') !!}#solutions" class="btn">find out more</a></div>
					</div>
					<div class="el3"><div class="init"><img src="/images/home/fs/el3.png?{{{$version}}}" class="el_love_it" alt="LOVE"/></div></div>
					<div class="el4"><div class="init"><img src="/images/home/fs/el4.png?{{{$version}}}" class="el_love_it" alt="IT"/></div></div>
				</div>
			</div>

			<div class="outer-fix home_drfrankie">
				<div class="inner-fix">
					<div class="zoom-photo">
						<div class="init-slow"><img src="/images/home/dr-frankie.jpg?{{{$version}}}" alt="welcome our new health expert!" class="d" /><img src="/images/home/dr-frankie_m.jpg?{{{$version}}}" class="m" alt="welcome our new health expert!"/></div>
					</div>
					<div class="el1"><div class="init">welcome our new femfresh</div></div>
					<div id="el_head_text">
						<div class="el2">
							<p>Dr Frankie’s her name, and myth-busting’s her game. Here to get women talking about all things gynaecological and intimate care-related, and of course answer your questions. She’s our brand-new, taboo breaking Femfresh health expert. <br><br>Welcome, Dr. Francesca Jackson-Spence!</p>
						</div>
					</div>
					<div class="btn-group" id="el_head_button">
						<div class="init"><a href="{!! route('drfrankie') !!}" class="btn">find out more</a></div>
					</div>
					<div class="el3"><div class="init"><img src="/images/home/health.png?{{{$version}}}" class="el_love_it" alt="LOVE"/></div></div>
					<div class="el4"><div class="init"><img src="/images/home/expert.png?{{{$version}}}" class="el_love_it" alt="IT"/></div></div>
				</div>
			</div>


		</div>
	<div id="trigger_head"></div>
	</div>

	{{-- range triplet --}}
	<div class="container bgw" data-ref="home-2">
		<div class="outer-fix">
			<div class="block-range" data-id="1">
				<div class="frame">
					<div class="img">
						<img src="/images/home/range-cleanse.jpg?{{{$version}}}" class="left" alt="cleanse"/>
					</div>
					<div class="text left bottom">
						<h2 class="h2" id="range_shift_title_1">cleanse</h2>
						<p id="range_shift_text_1">Add some daily intimate skincare to your shower routine with femfresh daily&nbsp;wash</p>
						<a href="{!! route('daily-intimate-wash') !!}" class="btn" id="range_shift_btn_1">Find out more</a>
					</div>
				</div>
			</div>
			<div class="block-range" data-id="2">
				<div class="frame">
					<div class="img">
						<img src="/images/home/range-move.jpg?{{{$version}}}" class="" alt="move"/>
					</div>
					<div class="text left top20">
						<h2 class="h2" id="range_shift_title_2">move</h2>
						<p id="range_shift_text_2">On-the-go? Leave your intimate skin feeling pHab wherever, whenever with femfresh 25 soothing and cleansing wipes</p>
						<a href="{!! route('25-wipes') !!}" class="btn range_shift_btn" id="range_shift_btn_2">Find out more</a>
					</div>
				</div>
			</div>
			<div class="block-range" data-id="3">
				<div class="frame">
					<div class="img">
						<img src="/images/home/range-spritz.jpg?{{{$version}}}" class="" alt="spritz"/>
					</div>
					<div class="text right top">
						<h2 class="h2" id="range_shift_title_3">spritz</h2>
						<p id="range_shift_text_3">For freshness that lasts all day long, spritz with femfresh intimate deodorant</p>
						<a href="{!! route('freshness-deodorant') !!}" class="btn range_shift_btn" id="range_shift_btn_3">Find out more</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	{{-- Femfresh Solutions Videos --}}
	<section class="container bgw" data-ref="home-femfresh-solutions">
		<header>
			<h2>Femfresh Solutions&trade;</h2>
			<p>Range of feminine treatments to complement your daily hygiene and address the specific needs of your intimate&nbsp;area.</p>
		</header>

		<div class="row">
			
			<article class="fs_item" data-ref="fs-external-comfort">
				<div class="videoBox">
					<figure><img alt="Femfresh Solutions&trade; External Comfort Gel" src="/images/home/fs/external-comfort-poster.jpg"></figure>
					 <span class="popupVideo" title="Play Femfresh Solutions&trade; External Comfort Gel video" data-source="/video/femfresh-solutions/comfort.mp4" data-title="Femfresh Solutions&trade; External Comfort Gel" data-poster="/images/home/fs/external-comfort-poster2.jpg" data-vh="2700" data-vw="2160">PLAY VIDEO</span>
				</div>
				<a href="{!! route('external-comfort-gel') !!}" class="btn">FIND OUT MORE</a>
			</article>
			<article data-ref="fs-odour-eliminating">
				<div class="videoBox">
					<figure><img alt="Femfresh Solutions&trade; Odour Eliminating Gel" src="/images/home/fs/odour-eliminating-poster.jpg"></figure>
					 <span class="popupVideo" title="Play Femfresh Solutions&trade; Odour Eliminating Gel video" data-source="/video/femfresh-solutions/odour.mp4" data-title="Femfresh Solutions&trade; Odour Eliminating Gel" data-poster="/images/home/fs/odour-eliminating-poster2.jpg" data-vh="2700" data-vw="2160">PLAY VIDEO</span>
				</div>
				<a href="{!! route('odour-eliminating-gel') !!}" class="btn">FIND OUT MORE</a>
			</article>
			
		</div>
	</section>

	@include('main.components._articles', ['thisArticle' => ''])

	{{-- @include('main.components._dr-sara-ph-balance', ['shifted' => '', 'version' => 'home']) --}}

	{{-- @include('main.components._she-talks-single', ['shifted' => '', 'episode' => 1]) --}}
	@include('main.components._she-talks', ['shifted' => ''])

	{{-- @include('main.components._vulva-or-vagina', ['shifted' => '']) --}}

	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')
	
@endsection