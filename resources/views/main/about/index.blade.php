@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="about">
		<div class="outer-fix">
			<div class="inner-fix tar">
				<img src="/images/about/heading-scene.jpg?{{{$version}}}" class="head-img" alt="About femfresh"/>
				<h1><span class="h2">about</span> femfresh</h1>
				<div class="orange-box"><p>If you&#39;re like us, you&#39;ve probably got hundreds of lotions and potions locked away in your bathroom cupboards. And maybe even spilling out of them…</p></div>
				<div class="checks">Hand creams? Check.<br/>Body lotions? <br class="hidden vis-sm vis-md"/>Which one?<br/>Lip Balms? Of course.<br/>Intimate wash? Huh?</div>
			</div>

			<div class="inner-fix">
				<div class="columns flippable">
					<div class="col flip-1">
						<div class="flip-pad">
							<p>Often this very special skin gets overlooked and when it comes to caring for our vulva, we usually grab a bottle of <a href="{!! route('daily-intimate-wash') !!}">shower gel</a> or a bar of soap. This works, doesn&#39;t it? Well, sort of… but not really.</p>
							<p class="mt1">These products do a great job at cleansing your body – which has a pH level of 5.5 – but our vulva? That needs special attention. Because using something that&#39;s not specially designed for the unique pH-level of our intimate area (pH-4.5) can strip the skin of its natural defences; causing irritation, odour and other unpleasantness.</p>
							<div class="promote">
								<p>That&#39;s right, your intimate skin needs love too.</p>
								<a href="{!! route('about-ph-balance') !!}" class="btn">Read about pH</a>
							</div>
						</div>
					</div>
					<div class="col flip-2">
						<div class="flip-pad">
							<img src="/images/about/body-shot.jpg?{{{$version}}}" class="" alt="Body shot"/>
						</div>
					</div>
				</div>
			</div>

			<div class="inner-fix columns">
				<div class="columns">
					<div class="col">
						<img src="/images/about/products.jpg?{{{$version}}}" class="" alt="femfresh products"/>
					</div>

					<div class="col">
						<p>That&#39;s why we put our heads together and did something about it. That something? The first-of-its-kind <a href="{!! route('daily-intimate-wash') !!}">intimate wash</a>: femfresh.</p>
						<p class="mt1">All our products are specially designed to care for the unique pH of your intimate skin (and are gynaecologically &amp; dermatologically tested!) because some of us prefer to feel a little extra love when it comes to our most intimate of skin.</p>
						<div class="promote">
							<p>Join the millions of women who care for down there with femfresh.</p>
							<a href="{!! route('range') !!}" class="btn">THE PRODUCTS</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{{-- @include('main.components._she-talks-single', ['shifted' => 'shifted', 'episode' => 1]) --}}
	@include('main.components._she-talks', ['shifted' => 'shifted'])

	@include('main.components._dr-sara-ph-balance', ['shifted' => 'shifted'])

	@include('main.components._vulva-or-vagina', ['shifted' => 'shifted'])

	@include('main.components._instagram', ['shifted' => 'shifted'])
@endsection

@section('components')

@endsection
