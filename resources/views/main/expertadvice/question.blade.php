	<div id="question-anchor" class="container" data-ref="ask-a-question">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<h2 class="h2">ask Dr Frankie a question</h2>
				@if (session('response'))
					<p class="h3 mt2 mb2">{{ session('response.message') }}</p>
				@else
				<p>By providing us with your details, you are giving your consent that your personal information may be processed for the purposes necessary to conduct our services, and provide you with the appropriate advice. For more information about your rights and how we use your personal data please read our <a href="/privacy-policy">Privacy Policy</a></p>
				
				<div class="form">

					{!! Form::open(array('url' => [route('drfrankie').'#question-anchor'], 'method' => 'POST', 'id' => 'questionForm') )!!}
					<div class="form-row {{ $errors->has('name') ? 'has-error' : '' }}">
						<!-- Form::label('name', 'Name:') -->
						{!! Form::text('name',null,['placeholder' => 'Full name', 'id' => 'name', 'class' => 'input', 'maxlength' => 64]) !!}
						{!! $errors->first('name', '<small class="error">:message</small>') !!}
					</div>

					<div class="form-row  {{ $errors->has('email') ? 'has-error' : '' }}">
						{!! Form::text('email',null,['placeholder' => 'email address', 'id' => 'email', 'class' => 'input']) !!}
						{!! $errors->first('email', '<small class="error">:message</small>') !!}
					</div>

					<div class="form-row  {{ $errors->has('question') ? 'has-error' : '' }}">
						{!! Form::textarea('question',null,['placeholder' => 'Type your question here', 'id' => 'question', 'rows' => '4', 'class' => 'input', 'maxlength' => 1000]) !!}
						{!! $errors->first('question', '<small class="error">:message</small>') !!}
					</div>
					<!-- terms -->
					<!-- google recapctha v2 -->
					<div class="form-row">
						{!! Form::submit('Submit', array('class' => 'send', 'id' => 'bSubmit')) !!}
					</div>

					{!! Form::close() !!}
				</div>
				@endif


			</div>
		</div>
	</div>
	<script src='https://www.google.com/recaptcha/api.js'></script>	
