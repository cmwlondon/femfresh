@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	<div class="container first" data-ref="expert">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<img src="/images/dr-frankie/dr-frankie.jpg?{{{$version}}}" class="head-img" alt="Meet our go-to gynaecologist Dr Sara">
				<h1>meet <span class="abpos">Doctor Frankie!</span></h1>
				<h2>Femfresh Health Expert</h2>
				<div class="desc"><p>Full-time doctor and female health advocate, <span>Dr. Francesca Jackson-Spence</span> is our go-to health expert. When she’s not working on the NHS front-line, she can be found promoting life-saving cancer screening, busting social media’s medical myths, and getting women talking about their intimate&nbsp;health.</p></div>
			</div>
			<div class="inner-fix">
				<div class="btns">
					<a href="#expert-advice" class="btn btn-anchor">EXPERT ADVICE</a>
					{{-- <a href="#q-and-a" class="btn btn-anchor">Q &amp; As</a> --}}
					<a href="#question-anchor" class="btn btn-anchor">ASK A QUESTION</a>
				</div>
			</div>
		</div>
	</div>

	@include('main.components._dr_jackies_expert_advice', [])

	
	{{-- @include('main.components.q-and-a', []) --}}

	{{--
	<div class="ask-more">
		<p>What would you like to <br class="hidden vis-sm vis-md"/>know more about?</p>
		<div class="form-row">
			<input type="text" id="question" value="" placeholder="SUBMIT A QUESTION FOR DR SARA">
			<input type="button" class="bSubmit" id="question-submit" value="SUBMIT">
		</div>
		<div class="result-row"></div>
	</div>
	--}}

	<div id="question-anchor" class="container" data-ref="ask-a-question">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<h2 class="h2">ask Dr Frankie a question</h2>
				@if (session('response'))
					<p class="h3 mt2 mb2">{{ session('response.message') }}</p>
				@else
				<p>By providing us with your details, you are giving your consent that your personal information may be processed for the purposes necessary to conduct our services, and provide you with the appropriate advice. For more information about your rights and how we use your personal data please read our <a href="/privacy-policy">Privacy Policy</a></p>
				
				<div class="form">

					{!! Form::open(array('url' => [route('drfrankie').'#question-anchor'], 'method' => 'POST', 'id' => 'questionForm') )!!}
					<div class="form-row {{ $errors->has('name') ? 'has-error' : '' }}">
						{{-- {!! Form::label('name', 'Name:') !!} --}}
						{!! Form::text('name',null,['placeholder' => 'Full name', 'id' => 'name', 'class' => 'input', 'maxlength' => 64]) !!}
						{!! $errors->first('name', '<small class="error">:message</small>') !!}
					</div>

					<div class="form-row  {{ $errors->has('email') ? 'has-error' : '' }}">
						{{-- {!! Form::label('email', 'Email Address: *') !!} --}}
						{!! Form::text('email',null,['placeholder' => 'email address', 'id' => 'email', 'class' => 'input']) !!}
						{!! $errors->first('email', '<small class="error">:message</small>') !!}
					</div>

					<div class="form-row  {{ $errors->has('question') ? 'has-error' : '' }}">
						{{-- {!! Form::label('question', 'Question: *') !!} --}}
						{!! Form::textarea('question',null,['placeholder' => 'Type your question here', 'id' => 'question', 'rows' => '4', 'class' => 'input', 'maxlength' => 1000]) !!}
						{!! $errors->first('question', '<small class="error">:message</small>') !!}
					</div>

					{{--
						<div class="form-row checkbox {{ $errors->has('terms') ? 'has-error' : '' }}">
						<input type="checkbox" id="terms" name="terms" value="accepted" @if ( old('terms', '') === 'accepted' ) checked="checked" @endif>
						<label for="terms">I have read and agreed to femfresh's website <a href="/terms-and-conditions" target="_blank">Terms &amp; Conditions</a></label>
						{!! $errors->first('terms', '<small class="error">:message</small>') !!}
					</div>
					--}}

					<!-- google recpatcha v2 dev,staging,live -->
					<div class="form-row googleRecaptcha  {{ $errors->has('g-recaptcha-response') ? 'has-error' : '' }}">
					@if(env('GOOGLE_RECAPTCHA_KEY'))
					     <div class="g-recaptcha"
					          data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
					     </div>
					@endif
					{!! $errors->first('g-recaptcha-response', '<small class="error">:message</small>') !!}
					</div>

					<div class="form-row">
						{!! Form::submit('Submit', array('class' => 'send', 'id' => 'bSubmit')) !!}
					</div>

					{!! Form::close() !!}
				</div>
				@endif


			</div>
		</div>
	</div>

	<div id="learn-more" class="container" data-ref="learn-more">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<h2 class="h2">want to learn more</h2>
				<p>Follow Dr Frankie on Instagram <a href="https://www.instagram.com/drfrankiejs/?hl=en" target="_blank">@drfrankiejs</a> to see which health myths she's been busting&nbsp;lately.</p>
			</div>
		</div>
	</div>

	@include('main.components._articles', ['thisArticle' => ''])
	@include('main.components._instagram', ['shifted' => ''])

@endsection

@section('components')
<script src='https://www.google.com/recaptcha/api.js'></script>	
@endsection