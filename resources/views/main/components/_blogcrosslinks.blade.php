<div class="container" data-ref="blogcrosslinks">
	<div class="inner-fix tal">
		<h2 class="h2">more expert advice</h2>

		<div class="links">

			<ul>
				{{-- <li><a href="/knowledge-hub/vagina-dialogue" @if ($blogSlug === 'vagina-dialogue') class="current" @endif>The Vagina Dialogue: Keeping your Lady Garden fresh</a></li> --}}
				@foreach ( $blogitems as $blogkey => $blog)
				@if ($blogSlug !== $blogkey && $blog['type'] !== 'video' )<li><a href="/knowledge-hub/{{ $blogkey }}">{!! $blog['title'] !!}</a></li>@endif
				@endforeach
			</ul>
		</div>
	</div>
</div>