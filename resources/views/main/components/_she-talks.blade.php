<div class="container bg24" data-ref="she-talks">
	<div class="inner-fix tal">
		<img src="/images/videos/she-talks-component-heading.png" alt="she talks: made possible by femfresh" class="heading"/>
		
		<div class="inner-fix">
			<div class="videoThumbs">
				<a href="{!! route('videos') !!}?play=1#videos" data-id="1" class="vThumb"><img src="/images/videos/video-1-anatomy-thumb.jpg?{{{$version}}}" alt="Watch video"/><img src="/images/videos/play-arrow.svg?{{{$version}}}" class="arrow" alt="Play"/></a>
				<a href="{!! route('videos') !!}?play=2#videos" data-id="2" class="vThumb"><img src="/images/videos/video-2-fitness-thumb.jpg?{{{$version}}}" alt="Watch video"/><img src="/images/videos/play-arrow.svg?{{{$version}}}" class="arrow" alt="Play"/></a>
				<a href="{!! route('videos') !!}?play=3#videos" data-id="3" class="vThumb"><img src="/images/videos/video-3-grooming-thumb.jpg?{{{$version}}}" alt="Watch video"/><img src="/images/videos/play-arrow.svg?{{{$version}}}" class="arrow" alt="Play"/></a>
				<a href="{!! route('videos') !!}?play=4#videos" data-id="4" class="vThumb"><img src="/images/videos/video-4-periods-thumb.jpg?{{{$version}}}" alt="Watch video"/><img src="/images/videos/play-arrow.svg?{{{$version}}}" class="arrow" alt="Play"/></a>
			</div>
			<p class="text">Watch our she talks videos or read what it&rsquo;s all about <a href="{!! route('videos') !!}">here</a></p>
		</div>
	</div>
</div>