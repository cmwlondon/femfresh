<div class="container bg8" data-ref="ymal">
	<div class="inner-fix">
		<h2 class="h2">you may also like</h2>
		<div class="ymal-group">
			@foreach ($products as $product)
				@if ($product == 'daily-wash')
					<div class="col">
						<div class="inner bg-daily-wash">
							<a href="{!! route('daily-intimate-wash') !!}"><img src="/images/range/product-daily-wash.jpg?{{{$version}}}" alt="Daily Wash" title="Daily Wash"/></a>
						</div>
					</div>
				@endif
				@if ($product == 'soothing-wash')
					<div class="col">
						<div class="inner bg-soothing-wash">
							<a href="{!! route('ultimate-care-soothing-wash') !!}"><img src="/images/range/product-soothing-wash.jpg?{{{$version}}}" alt="Soothing Wash" title="Soothing Wash"/></a>
						</div>
					</div>
				@endif
				@if ($product == 'active-wash')
					<div class="col">
						<div class="inner bg-active-wash">
							<a href="{!! route('ultimate-care-active-wash') !!}"><img src="/images/range/product-active-wash.jpg?{{{$version}}}" alt="Active Wash" title="Active Wash"/></a>
						</div>
					</div>
				@endif

				{{--
				@if ($product == 'pure-wash')
					<div class="col">
						<div class="inner bg-pure-wash">
							<a href="{!! route('ultimate-care-pure-wash') !!}"><img src="/images/range/product-pure-wash.jpg?{{{$version}}}" alt="Pure Wash" title="Pure Wash"/></a>
						</div>
					</div>
				@endif
				--}}

				@if ($product == '25-wipes')
					<div class="col wide">
						<div class="inner bg-wipes">
							<a href="{!! route('25-wipes') !!}#25"><img src="/images/range/product-25-wipes.jpg?{{{$version}}}" alt="25 Wipes" title="25 Wipes"/></a>
						</div>
					</div>
				@endif
				@if ($product == '10-wipes')
					<div class="col wide">
						<div class="inner bg-wipes">
							<a href="{!! route('25-wipes') !!}#10"><img src="/images/range/product-10-wipes.jpg?{{{$version}}}" alt="10 Wipes" title="10 Wipes"/></a>
						</div>
					</div>
				@endif

				@if ($product == 'ultimate-care-active-fresh-deodorant')
					<div class="col">
						<div class="inner bg-ultimate-care-active-fresh-deodorant">
							<a href="{!! route('ultimate-care-active-fresh-deodorant') !!}"><img src="/images/range/product-ultimate-care-active-fresh-deodorant.jpg?{{{$version}}}" alt="Ultimate Care Active Fresh Deodorant" title="Ultimate Care Active Fresh Deodorant"/></a>
						</div>
					</div>
				@endif
				@if ($product == 'freshness-deodorant')
					<div class="col">
						<div class="inner bg-freshness-deodorant">
							<a href="{!! route('freshness-deodorant') !!}"><img src="/images/range/product-freshness-deodorant.jpg?{{{$version}}}" alt="Freshness Deodorant" title="Freshness Deodorant"/></a>
						</div>
					</div>
				@endif

				{{--
				@if ($product == '2-in-1-shower-shave-cream')
					<div class="col">
						<div class="inner bg-shower-shave-cream">
							<a href="{!! route('2-in-1-shower-shave-cream') !!}"><img src="/images/range/product-2-in-1-shower-shave-cream.jpg?{{{$version}}}" alt="2 in 1 Shower &amp; Shave Cream" title="2 in 1 Shower &amp; Shave Cream"/></a>
						</div>
					</div>
				@endif
				@if ($product == 'post-shave-balm')
					<div class="col">
						<div class="inner bg-post-shave-balm">
							<a href="{!! route('post-shave-balm') !!}"><img src="/images/range/product-post-shave-balm.jpg?{{{$version}}}" alt="Post Shave Balm" title="Post Shave Balm"/></a>
						</div>
					</div>
				@endif
				--}}
				@if ($product == 're-balance-powder')
					<div class="col">
						<div class="inner bg-re-balance-powder">
							<a href="{!! route('re-balance-powder') !!}"><img src="/images/range/product-re-balance-powder.jpg?{{{$version}}}" alt="Re-Balance Powder" title="Re-Balance Powder"/></a>
						</div>
					</div>
				@endif


			@endforeach
		</div>
	</div>
</div>
