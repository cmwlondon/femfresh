	<div class="container" data-ref="top-tips">
		<div class="inner-fix tal">
			<h2 id="top-tips" class="h2">Top tips</h2>
			<p class="desc">If you&#39;ve suffered from irritation before you&#39;ll know just how annoying it can be. So, what can you do to avoid it?</p>

			<div class="slick-wrapper" data-show="1">
				<div class="slick-tips">
					<div><img src="/images/knowledge/top-tips/tip-1.png?{{{$version}}}" alt="Wash regularly using a pH-balanced shower gel on your vulva."/></div>
					<div><img src="/images/knowledge/top-tips/tip-2.png?{{{$version}}}" alt="Wipe from front to back after going to the toilet, and consider using femfresh wipes when you have your period. They will remove the blood more easily and more gently than traditional toilet paper and ensure your pH-balance is not disrupted."/></div>
					<div><img src="/images/knowledge/top-tips/tip-3.png?{{{$version}}}" alt="If you have a tendency to cystitis go to the toilet, drink some water and have a quick wash immediately after sex."/></div>
					<div><img src="/images/knowledge/top-tips/tip-4.png?{{{$version}}}" alt="Consider switching biological washing powder to a non-biological formula, for your lingerie at least."/></div>
					<div><img src="/images/knowledge/top-tips/tip-5.png?{{{$version}}}" alt="Try to avoid tight clothing all the time! We all love our skinny jeans but make sure you mix it up a little."/></div>
					<div><img src="/images/knowledge/top-tips/tip-6.png?{{{$version}}}" alt="Don&rsquo;t hang around for hours in sweaty lycra, or on holiday, in a wet bikini. If you know you will be by the pool for a while and don&rsquo;t have a change, or you have the school run to do and then a dash to the supermarket before you can get home, use a femfresh intimate wipe."/></div>
					<div><img src="/images/knowledge/top-tips/tip-7.png?{{{$version}}}" alt="Eating a healthy, balanced diet and drinking alcohol and caffeine in moderation, all help to maintain good general health and will help reduce irritation. A probiotic supplement can also be really helpful as a preventative measure for those prone to thrush or bacterial vaginosis."/></div>
					<div><img src="/images/knowledge/top-tips/tip-8.png?{{{$version}}}" alt="Drink plenty of fluids throughout the day, such as water or herbal teas."/></div>
					<div><img src="/images/knowledge/top-tips/tip-9.png?{{{$version}}}" alt="Pee regularly! Waiting until you&rsquo;re bursting can give any bacteria more of a chance to multiply."/></div>
					<div><img src="/images/knowledge/top-tips/tip-10.png?{{{$version}}}" alt="Use a water based hypoallergenic lubricant if you need some extra help during sex."/></div>
				</div>

				<div class="h2 tip-title">tip 1</div>
				<div class="arrow left"><img src="/images/knowledge/tip-arrow.svg?{{{$version}}}" alt="Previous"/></div>
				<div class="arrow right"><img src="/images/knowledge/tip-arrow.svg?{{{$version}}}" alt="Next"/></div>
			</div>
		</div>
	</div>

