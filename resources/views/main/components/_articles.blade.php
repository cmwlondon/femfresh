<div id="articles" class="container {!! (($thisArticle == '') ? 'bg18' : 'bgw notop') !!}" data-ref="articles">
	<div class="outer-fix">
		<div class="inner-fix">

			<h2 class="h2">the big questions on feminine&nbsp;hygiene</h2>

			{{--
			@if ($thisArticle == '')
				<h2 class="h2">Latest articles</h2>
			@else
				<h2 class="h2">More articles</h2>
			@endif
			--}}

			<div class="columns">
				@foreach ($articles as $slug => $article)
					@if (!isset($articleSlug) || ($articleSlug != $slug))
						<div class="col">
						
							<a href="{!! route('article', ['slug' => $slug]) !!}"><img src="/images/articles/{!! $article['image'] !!}?{{{$version}}}" alt="{!! $article['title'] !!}"/></a>
							<div class="box">
								<p>{!! $article['title'] !!}</p>
								<div class="rmore"><a href="{!! route('article', ['slug' => $slug]) !!}" class="btn">Read&nbsp;More</a></div>
							</div>

						</div>
					@endif
				@endforeach

				@if (isset($articleSlug))
					<div class="col">
						<a href="{!! route('knowledge') !!}#articles" class="vMore"><img src="/images/articles/view-more.png?{{{$version}}}" alt="View More Articles"/><span>View More Articles</span></a>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>