<div class="container {{{$bg}}} {{{$first}}}" data-ref="range">
	<div class="inner-fix">
		<h1 class="h2">our range</h1>
		@if ($textshown)
		<div class="text">
			<p>femfresh intimate washes, wipes and sprays are all <a href="{!! route('about-ph-balance') !!}"><strong>pH-balanced</strong></a> for vulval skin, to give this delicate and extra sensitive area the TLC it deserves. <strong>femfresh products are enhanced with soothing and hydrating ingredients</strong> to care for skin and leave us ladies feeling loved all day long.</p>
		</div>
		@endif
		<div class="range-group">

			<div class="col">
				<div class="inner">
					<img src="/images/range/our-range-solutions.jpg?{{{$version}}}" alt="Femfresh Solutions&trade;"/>
					<h3>Femfresh Solutions&trade;</h3>
					<a href="{!! route('range') !!}#solutions" class="btn range-anchor">View Products</a>
				</div>
			</div>

			<div class="col">
				<div class="inner">
					<img src="/images/range/washes.jpg?{{{$version}}}" alt="Washes"/>
					<h3>washes</h3>
					<a href="{!! route('range') !!}#washes" class="btn range-anchor">View Products</a>
				</div>
			</div>
			<div class="col">
				<div class="inner">
					<img src="/images/range/wipes.jpg?{{{$version}}}" alt="Wipes"/>
					<h3>wipes</h3>
					<a href="{!! route('range') !!}#wipes" class="btn range-anchor">View Products</a>
				</div>
			</div>
			<div class="col">
				<div class="inner">
					<img src="/images/range/sprays.jpg?{{{$version}}}" alt="Sprays"/>
					<h3>sprays</h3>
					<a href="{!! route('range') !!}#sprays" class="btn range-anchor">View Products</a>
				</div>
			</div>
		</div>
	</div>
</div>