<div class="container" data-ref="dr-sara-ph-balance">
	<div class="flex-fix">
		{{--
		<div class="col50" data-col="1" id="trigger_sara_top">
			<div class="block block-sara">
				<img src="/images/home/dr-sara.jpg?{{{$version}}}" alt="Dr Sara">
				<div class="title {{{$shifted}}}" id="sara_title"><h2 class="h2">Dr<br/>Sara</h2></div>
				@if ($version == 'home')
				<h3 class="h5">In the words of Dr Sara our Gynaecologist&hellip;</h3>
				<p>&ldquo;Using a specialist product is a personal choice, however if you do choose to use more than just water to clean your intimate skin, it’s important to use the right product. femfresh is a specially designed, <a href="{!! route('range') !!}#washes">pH-balanced range</a> which can be used safely on a daily basis. The addition of ingredients such as lactic acid and probiotics across the range also help to maintain your natural <a href="{!! route('about-ph-balance') !!}">pH-balance</a>.&rdquo;</p>
				@else
				<p>&ldquo;I personally would recommend using a <a href="{!! route('daily-intimate-wash') !!}">pH-balanced wash</a> to ensure you are cleaning your vulval skin thoroughly without disrupting your <a href="{!! route('about-ph-balance') !!}">natural pH-balance</a>. Using water alone will not harm the intimate balance however many women also want the added benefits of cleansing, soothing and moisturising the skin.&rdquo;</p>
				@endif
				<div id="sara_button" class="slide_button right {{{$shifted}}}"><a href="{!! route('dr-sara') !!}" class="btn">Read More</a></div>
			</div>
		</div>
		--}}
		<div class="col50" data-col="2" id="trigger_ph_top">
			<div class="block block-ph">
				<h2 class="h2" id="ph_title {{{$shifted}}}">What is pH&#8209;balance?</h2>
				<p>pH-balance is a measure of how acid or alkali something is. The skin on your body has a pH-level of 5.5. However, the pH-level of intimate skin is closer to 4.5.</p> 
				<p class="mb1">If the pH of intimate vulval skin changes (making it less, or more, acidic) it can cause irritation, or – in some cases – cause bacteria and can multiply, resulting in infections such as bacterial vaginosis or thrush.</p>
				<div id="ph_button" class="slide_button right {{{$shifted}}}"><a href="{!! route('about-ph-balance') !!}" class="btn">Read More</a></div>
			</div> 
		</div>
	</div>
</div>