<?php
	$videos = [
		'1' => ['num' => 'one', 'name' => 'anatomy'],
		'2' => ['num' => 'two', 'name' => 'fitness'],
		'3' => ['num' => 'three', 'name' => 'grooming'],
		'4' => ['num' => 'four', 'name' => 'periods']
	];
	if ($episode == null) {
		$episode = "1";
	}
?>

<div class="container bg8" data-ref="she-talks-single">
	<div class="inner-fix tar">
		<div class="video-wrap">
			<h2 class="h2">she talks:</h2>
			<a href="{!! route('videos') !!}?play={{{ $episode }}}#videos"><img src="/images/videos/video-{{{ $episode }}}-{{{ $videos[$episode]['name'] }}}-large.jpg?{{{$version}}}" alt="She Talks - Episode {{{ $videos[$episode]['num'] }}}"/><img src="/images/videos/play-arrow.svg" class="arrow" alt="Play"/></a>

			<p><span><strong>Episode {{{ $videos[$episode]['num'] }}}:</strong> {{{ $videos[$episode]['name'] }}}</span> <a href="{!! route('videos') !!}" class="btn">WATCH SHE TALKS</a></p>
		</div>
		
	</div>
</div>