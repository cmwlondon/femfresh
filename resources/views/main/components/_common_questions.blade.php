	<div id="expert-advice" class="container" data-ref="expert-advice">
		<div class="outer-fix">
			<div class="inner-fix tal">
				<h2 class="h2">Expert advice</h2>
				<ul class="advice" data-reveal="0">
					<li data-id="1">
						<h4>So, talk us through the differences between the internal and external intimate organs?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>The vulva is the external sexual organ of a woman&rsquo;s body and its main purpose is to protect the inner sexual organs, including the vagina and the urethra (opening for urination). The vulva is the external skin which consists of the mons pubis (the top area towards the lower abdomen), the labia majora or outer labia/lips that run down both sides, and the labia minora or inner labia/ lips. The mons and labia majora are covered in hair, whilst the labia minora are hair free and can be particularly sensitive. The labia minora form the transition between the inner vagina and the outer vulva.</p>
						</div>
					</li>
					<li data-id="2">
						<h4>Why should I be thinking about pH-balance when it comes to my intimate skin?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>pH is simply the measure of how acidic or alkaline something is. It is based on a numerical scale ranging from 0 to 14. Anything below 7 is acidic, anything above 7 is alkaline. Pure water has a pH of 7, which is neutral but you may not realise that your skin does not have a neutral pH. Your skin has a normal pH between 5.4 and 5.9, however the intimate skin has a normalised pH less than 4.5 for a women at reproductive age. This is regulated by good bacteria in the vagina called lactobacilli, which helps to prevent the growth of harmful organisms. If the <a href="{!! route('about-ph-balance') !!}">pH of the vagina</a> increases (making it less acidic) the quality or amount of lactobacilli can fall and other harmful bacteria can multiply which can lead to infections such as bacterial vaginosis or thrush.</p>
						</div>
					</li>
					<li data-id="3">
						<h4>What can upset my intimate pH-balance and how do I know if my pH levels have become unbalanced?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p><a href="{!! route('about-ph-balance') !!}">pH levels</a> change with age - before puberty and then again with the onset of the menopause. In between, there are a number of factors that can cause your pH to fluctuate which can make you more prone to infections. Below are some of the more common triggers that can cause a change in your natural balance.</p>
							<h5>Triggers:</h5>
							<p class="mt05">&bull; During your <a href="{!! route('article', ['slug' => 'periods-dos-and-donts']) !!}">monthly cycle</a>, hormonal changes alter the pH-balance. It&rsquo;s least acidic on the days just before and during your period leaving you more prone to infection, so changing your sanitary protection regularly and cleaning the vulval skin outside during your period is essential<br />
							&bull; Perfumed or high pH shower gels<br />
							&bull; Biological washing powders, and non-hypoallergenic washing powders that contain irritating chemicals<br />
							&bull; Heat and sweat caused from wearing tight underwear or jeans, and exercise<br />
							&bull; Friction during sex can cause irritation during or immediately after intercourse<br />
							&bull; Some men have very alkaline sperm which can also disrupt the female pH and make bacterial vaginosis more likely<br />
							&bull; Non-breathable underwear or sanitary protection products can also be irritants, especially if they are worn all day<br />
							&bull; Being on antibiotics usually lessens the good bacteria, weakening defences against vaginal infections, particularly thrush (yeast)<br />
							&bull; Some contraceptive pills can result in changes in the vaginal bacteria that predisposes to infection, and the drop in hormones in the pill free week leading up to a period can also be a trigger</p>
							<h5>Symptoms:</h5>
							<p class="mt05">&bull; The most common symptom of a change in your pH is an itching or burning sensation on the vulval skin</p>
							<h5>Other symptoms include:</h5>
							<p class="mt05">&bull; Increased discharge which is a different colour and/or smell to your usual discharge, which could mean that you have an infection (if this is the case, contact your GP)<br />
							&bull; Discomfort during sex<br />
							&bull; Occasionally, it can exhibit itself as discomfort when going to the toilet</p>
						</div>
					</li>
					<li data-id="4">
						<h4>What&rsquo;s the best way to maintain a balanced intimate pH level? Do I need to use an intimate wash to achieve this?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Using an <a href="{!! route('daily-intimate-wash') !!}">intimate wash</a> is a personal choice and for women who want to use anything more than water, it is important to choose a product that works in harmony with your body. <a href="{!! route('about') !!}">femfresh</a> is great because it is specially designed to maintain the natural pH-balance of the intimate skin.</p>
							<p>If you have a tendency to skin irritation, maintaining a healthy pH-balance is very important for you, so using <a href="{!! route('range') !!}#washes">femfresh intimate washes</a> is a good choice to help you achieve this. If you have an active lifestyle and often go back to work after the gym, the <a href="{!! route('25-wipes') !!}">femfresh intimate wipes</a> are brilliant as a temporary solution to <a href="{!! route('article',['slug' => 'intimate-sweating-no-sweat']) !!}">remove sweat</a> without disrupting the pH-balance.</p>
						</div>
					</li>
					<li data-id="5">
						<h4>Isn&rsquo;t water enough? And why can&rsquo;t I use my regular shower gel to cleanse my intimate skin? Isn&rsquo;t all skin the same?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>I personally would recommend using a <a href="{!! route('daily-intimate-wash') !!}">pH-balanced wash</a> to ensure you are cleaning your vulval skin without disrupting your natural <a href="{!! route('about-ph-balance') !!}">pH-balance</a>. Using water alone will not harm the intimate balance however many women also want the added benefits of cleansing, soothing and moisturising the skin. When talking about intimate washes, it is important to recognise that we aren&rsquo;t talking about douching at all, i.e. putting water into the vagina itself. femfresh is not a douche and I never recommend cleaning inside the vagina, but keeping the vulval skin healthy helps the inside too.</p>
						</div>
					</li>
					<li data-id="6">
						<h4>Sometimes I find my intimate skin gets quite itchy. What could be causing this?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Itchiness can be caused by inflammation, infection or as a result of an allergy so it&rsquo;s important to identify what could be causing this. There are over 60 different reasons why you could be experiencing itchiness in your intimate area with the below being some common triggers:</p>
							<p>&bull; Non pH-balanced shower gels<br />
								&bull; Washing powder<br />
								&bull; Panty liners<br />
								&bull; A reaction to your tampon or sanitary towel, or not changing them regularly enough<br />
								&bull; Creams / lotions<br />
								&bull; Laser or wax<br />
								&bull; Antibiotics<br />
								&bull; Contraceptive pill<br />
								&bull; Infections such as thrush<br />
								&bull; Being sexually active more than usual</p>
								<p>It is also important to look at your intimate skin to see whether you have a rash, any swelling, spots or ulcers. If you do you should consult your doctor.</p>
						</div>
					</li>
					<li data-id="7">
						<h4>Why are some women more prone to irritation and/or discomfort than others?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Some women find that there are different times when they are more prone to irritation than others, which is largely to do with a change in the <a href="{!! route('about-ph-balance') !!}">pH of your vagina</a>. After the menopause or just before your <a href="{!! route('article', ['slug' => 'periods-dos-and-donts']) !!}">period</a> for example, or if you take part in activities which can disrupt your natural balance, like regularly doing a <a href="{!! route('article', ['slug' => 'intimate-sweating-no-sweat']) !!}">sweaty workout</a>, then it&rsquo;s a good idea to counteract that with <a href="{!! route('range') !!}#washes">pH-balancing washes</a> or <a href="{!! route('25-wipes') !!}">wipes</a>. Some women have skin conditions such as eczema and psoriasis around the vagina. These women will also benefit from using <a href="{!! route('daily-intimate-wash') !!}">femfresh wash</a>.</p>
						</div>
					</li>
					<li data-id="8">
						<h4>Why is irritation more common in pregnant and menopausal women?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>The simple answer is changes in your hormones. Throughout pregnancy the surge of hormones can change the <a href="{!! route('about-ph-balance') !!}">pH levels</a>, which then makes pregnant women more prone to infection. This is extremely common. In menopausal women, the decrease in hormones changes the intimate skin so it becomes thinner and less moist, which alters the <a href="{!! route('about-ph-balance') !!}">pH-balance</a>, making the drier skin more sensitive and prone to infection.</p>
						</div>
					</li>
					<li data-id="9">
						<h4>What is vaginal douching and is it safe?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p><a href="https://www.nhs.uk/Livewell/vagina-health/Pages/keep-vagina-clean.aspx" target="_blank" rel="nofollow">Vaginal douching</a> involves using a syringe to insert a solution the consistency of water into the vagina itself (internal organ). femfresh is not a douche and I never recommend doing this as it is not safe. Should you have an infection, it will not solve the issue, in fact, it&rsquo;s more likely to cause irritation and result in a more serious infection as douching disrupts your natural intimate balance.</p>
						</div>
					</li>
					<li data-id="10">
						<h4>Is white vaginal discharge normal?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Yes, it is completely normal. Discharge changes throughout the month. Just after your <a href="{!! route('article', ['slug' => 'periods-dos-and-donts']) !!}">period</a> you will notice that there isn&rsquo;t a lot of discharge but as you are coming up to ovulation you will notice an odourless clear discharge (similar to egg whites). This is a sign that ovulation is about to happen. After ovulation you will notice white discharge with either no smell or a mild smell. Women who are on the pill have no cyclical changes but it is normal to have a small amount of white discharge.</p>
						</div>
					</li>
					<li data-id="11">
						<h4>Does a change in the odour of my vaginal discharge mean I have an infection and if so what could it be?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>A change in odour can relate to hormonal changes, bleeding, dehydration, and of course infection. If the discharge has a strong odour, or there is more than normal, I would recommend seeing your doctor, who will be able to investigate.</p>
						</div>
					</li>
					<li data-id="12">
						<h4>What is the cause of irritation after sex and what is the best way to reduce this?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>After sex, you may feel some immediate irritation which is normal and is usually caused by friction, particularly if you haven&rsquo;t had sex for a while. In some cases, irritation may even be caused by the sperm itself, which is alkaline or, if you use a condom, possibly by a sensitivity to latex or an allergic reaction to the spermicide coating the condom. Some women find that using a water based lubricant during sex can help reduce this irritation. Going to the toilet and having a <a href="{!! route('daily-intimate-wash') !!}">wash</a> or using a <a href="{!! route('25-wipes') !!}">pH-balanced intimate wipe</a> immediately after sex can also help to reduce irritation by restoring your natural balance. If irritation continues 7-10 days after sex, it may mean that you have an infection so I&rsquo;d recommend going to see your doctor who will be able to investigate the cause of irritation. Very rarely, a woman can be allergic to sperm itself, especially if she has other allergies. In this situation, swelling and discomfort happen after every sexual encounter.</p>
						</div>
					</li>
					<li data-id="13">
						<h4>What is cystitis and if I have it once, am I more likely to get it again?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Cystitis is a bladder infection which may be triggered after having sex, but it can occur at any age, and is more common when dehydration is also an issue. I suggest drinking plenty of water, going to the toilet and having a <a href="{!! route('daily-intimate-wash') !!}">quick wash</a> immediately after sex. <a href="{!! route('range') !!}#washes">femfresh intimate washes</a> or <a href="{!! route('25-wipes') !!}">wipes</a> are great as they can help to maintain your natural pH-balance. Mild cystitis is usually treatable with re-hydration and cranberry or drink sachets available over the counter to reduce the bladder inflammation, but it sometimes does need to be treated with antibiotics. Antibiotics are always required if there is blood visible in the urine, or if you develop a fever</p>
						</div>
					</li>
					<li data-id="14">
						<h4>What would you recommend as the best way to relieve symptoms of dryness/irritation?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Whatever the cause or severity of dryness/irritation there are things you can do to relieve the symptoms. You can see your GP to discuss prescriptive remedies, but there are some steps you can take yourself to help.<br />
							&bull; Hygiene &ndash; make sure you keep everything clean and dry<br />
							&bull; Use a <a href="{!! route('ultimate-care-soothing-wash') !!}">pH appropriate wash</a> in your intimate area<br />
							&bull; If necessary, apply a hypoallergenic emollient cream<br />
							&bull; Take in lots more water than usual to ensure you are suitably hydrated<br />
							&bull; Avoid any dehydrating drinks like alcohol and caffeine<br />
							&bull; Avoid biological washing powders - opt for the most sensitive non-biological option<br />
							&bull; Use a water-based lubricant during sex<br />
							&bull; Avoid wearing tight underwear and trousers<br />
							&bull; Avoid any intimate irritants, like perfumed shower gels, soaps or bath oils. Opt for femfresh <a href="{!! route('ultimate-care-soothing-wash') !!}">Ultimate Care Soothing Wash</a> which is hypoallergenic and at the right <a href="{!! route('about-ph-balance') !!}">pH-balance</a> for the intimate vulval skin</p>
						</div>
					</li>
					<li data-id="15">
						<h4>What is Bacterial Vaginosis and can femfresh help to eliminate BV?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Bacterial Vaginosis is a common infection caused by an imbalance of the bacteria that are present inside the vagina. Symptoms typically include a greenish discharge and a fishy odour. femfresh is not a treatment for BV and it won&rsquo;t mask the odour. If you think you might have Bacterial Vaginosis, you will need to see your doctor who will be able to diagnose the problem and prescribe specific treatment.</p>
						</div>
					</li>
					<li data-id="16">
						<h4>Can I use femfresh products if I have thrush?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Yes of course. <a href="{!! route('range') !!}">femfresh products</a> are great because they&rsquo;re much less likely to irritate the skin. When you have thrush there&rsquo;s a baseline inflammation of the skin which can cause discomfort and itching. Therefore using a soothing solution to <a href="{!! route('ultimate-care-soothing-wash') !!}">wash the intimate area</a> is ideal. Again, it won&rsquo;t treat the thrush but it may help to soothe and ease the itching.</p>
						</div>
					</li>
					<li data-id="17">
						<h4>Can I use femfresh products if I&rsquo;m trying to conceive?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Absolutely. You can also use them whilst pregnant.</p>
						</div>
					</li>
					<li data-id="18">
						<h4>Do I need to use a specialist product to wash during my period?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>You don&rsquo;t need to use a specialist product during your <a href="{!! route('article',['slug' => 'periods-dos-and-donts']) !!}">period</a>, but blood raises the pH of the vulvar skin, and provides a perfect environment for bacteria to thrive, so keeping the vulval area clean is sensible, and I recommend using a <a href="{!! route('daily-intimate-wash') !!}">pH-balanced formula</a> specifically designed to clean your vulvar skin gently but effectively. The <a href="{!! route('range') !!}">femfresh range</a> is suitable for this time of the month, with both <a href="{!! route('daily-intimate-wash') !!}">wash</a> and <a href="{!! route('25-wipes') !!}">wipes</a> giving you the confidence that you have cleaned properly and helping you to feel fresh. Both products are hypoallergenic and won&rsquo;t disrupt your <a href="{!! route('about-ph-balance') !!}">pH-balance</a>.</p>
						</div>
					</li>
					<li data-id="19">
						<h4>What are the benefits of using the femfresh range and is it important to use?</h4>
						<img src="/images/knowledge/minus-square.svg?{{{$version}}}" class="minus"/>
						<img src="/images/knowledge/plus-square.svg?{{{$version}}}" class="plus"/>
						<div class="reveal">
							<p>Using a specialist product is a personal choice, however if you do choose to use more than just water to <a href="{!! route('daily-intimate-wash') !!}">clean your intimate skin</a>, it&rsquo;s important to use the right product. <a href="{!! route('about') !!}">femfresh</a> is a specially designed range which can be used safely on a daily basis. The addition of ingredients such as lactic acid and probiotics across the range also help to maintain healthy intimate skin.</p>
						</div>
					</li>
				</ul>
