	<div class="container" data-ref="df-expert-advice" id="expert-advice">
		<div class="inner-fix tal">
			<h2 class="h2">expert advice</h2>
			<div class="frame">
				<div class="row blogslick">

					{{-- 387 x 274 --}}

				@if( count( $blogitems) > 0 )
				@foreach( $blogitems as $url => $blogitem)
					@if( $blogitem['type']  === 'article' )
					<div class="column">
						<div class="pic"><a href="/knowledge-hub/{{ $url }}" title="{!!  $blogitem['title'] !!}"><img alt="{!!  $blogitem['title'] !!}" src="{{ $blogitem['thumbnail'] }}"></a></div>
						<p>{!!  $blogitem['title'] !!}<p>
						<a href="/knowledge-hub/{{ $url }}" class="btn">MORE</a>
					</div>
					@else
		            <div class="column video">
		                <div class="pic">
		                    <img alt="{{ $blogitem['title'] }}" src="{{ $blogitem['thumbnail'] }}">
		                    <span class="popupVideo" title="Play video: {{ $blogitem['title'] }}" data-source="{{ $blogitem['mp4'] }}" data-title="{{ $blogitem['title'] }}" data-poster="{{ $blogitem['poster'] }}">PLAY VIDEO</span>
		                </div>
		                <p>{{ $blogitem['title'] }}<p>
		            </div>
					@endif
				@endforeach
				@endif

				</div>
			</div>
		</div>
	</div>

