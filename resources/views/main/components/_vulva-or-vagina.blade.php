<div class="container bg9" data-ref="vulva-or-vagina">
	<div class="inner-fix">
		<h2>Vulva or Vagina</h2>
		<div class="text">
			<p>Get intimate with your intimate area in our interactive quiz.</p>
			<div id="quiz_button" class="slide_button left {{{$shifted}}}"><a href="{!! route('quiz') !!}" class="btn">Take the quiz</a></div>
		</div>
		<div class="question-mark">
			<img src="/images/home/question-mark.png?{{{$version}}}" alt="Question Mark" class="qm"/>
		</div>
	</div>
</div>