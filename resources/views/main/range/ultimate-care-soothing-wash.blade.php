@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="soothing-wash">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/ultimate-care-soothing-wash.jpg?{{{$version}}}" alt="Ultimate Care Soothing Wash"/>
					<h1 class="h2">soothing</h1>
					<a href="{{ __('links.ultimate-care-soothing-wash') }}" target="_blank" rel="nofollow" data-track="ultimate-care-soothing-wash" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">soothing</h1>
					<div class="box">
						<p class="intro">Enjoy hour after hour of that ‘aaaah’ feeling with femfresh Ultimate Care Soothing Wash</p>
						<p>Aaaah&hellip; and relax. Get that pampered feeling with these <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> soothing suds. The specially designed formula contains probiotics to help stimulate skin's natural defences and the cranberry and cornflower extracts gently hydrate. Then the MULTIActif complex leaves you feeling fresh all day, every day.</p>
						<ul class="mt2">
							<li>Enriched with hydrating cranberry and cornflower extracts</li>
							<li>pH-balanced for intimate skin</li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Hypoallergenic and soap free</li>
							<li>MULTIActif complex</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.ultimate-care-soothing-wash') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="ultimate-care-soothing-wash" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Use femfresh Ultimate Care Soothing Wash daily to cleanse your intimate skin. Lather up and then rinse thoroughly with warm water.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Aqua, Sodium Laureth Sulfate, Sodium Chloride, Glycol Distearate, Cocamidopropyl Betaine, Triethyl Citrate, Parfum, Glycerin, Laureth-4, Polyquaternium-39, Dichlorobenzyl Alcohol, Alpha-glucan Oligosaccharide, Lactic Acid, Butylene Glycol, Lonicera Japonica (Honeysuckle) Flower Extract, Sodium Lactate, 2-Bromo-2-Nitropropane-1,3-Diol, Polymnia Sonchifolia Root Juice, Vaccinium Macrocarpon Fruit Extract, Centaurea Cyanus Flower Extract, Maltodextrin, Sodium Benzoate, Calendula Officinalis Flower Extract, Aloe Barbadensis Extract, Lactobacillus, Sodium Hydroxide, Sodium Carbonate</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>How often can I use femfresh intimate washes?</h5>
							<div class="reveal">
								<p>All our intimate washes are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
								<p>Each <a href="{!! route('range') !!}#washes">femfresh wash</a> is suitable for everyday use.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>Can I use femfresh on other parts of my body?</h5>
							<div class="reveal">
								<p>femfresh <a href="{!! route('range') !!}#washes">intimate washes</a> are <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> and specifically designed for use on your intimate skin but they can also be used safely on other external parts of the body.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>How come there are different washes in the femfresh range? What are they for?</h5>
							<div class="reveal">
								<p>All our intimate washes are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
								<p>femfresh intimate washes come in four different formulas so you can choose the perfect care to suit your intimate skin. The intimate washes range includes <a href="{!! route('daily-intimate-wash') !!}">Daily Intimate Wash</a> with a hint of soothing aloe vera to keep your intimate skin happy all-day long.</p>
								<p>The Ultimate Care range consists of <a href="{!! route('ultimate-care-soothing-wash') !!}">Soothing Wash</a> and <a href="{!! route('ultimate-care-active-wash') !!}">Active Wash</a>. The Pure Wash with lactic acid and caring aloe vera is fragrance free and specially formulated for those with sensitive skin. Our Soothing Wash has a moisturising formula with a probiotic complex, and is enriched with aloe vera and cranberry extracts best known for their healing and hydrating properties. The Active Wash provides all day, feel good confidence, with ginseng and antioxidants that work in harmony with your body to provide up to 24 hours of freshness.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-soothing-wash">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;femfresh Ultimate Care Soothing Wash is great for everyone but especially if you have a tendency to skin irritation. Maintaining a healthy <a href="{!! route('about-ph-balance') !!}">pH-balance</a> is very important for you, so using femfresh intimate washes is a good choice to help you achieve this.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['daily-wash','pure-wash','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
