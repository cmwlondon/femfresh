@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	
	<div class="container product" data-ref="wipes">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<div class="slick-products">
						<img src="/images/products/25-wipes.jpg" alt="25 Freshening and Soothing Wipes"/>
						<img src="/images/products/10-wipes.jpg" alt="10 Freshening and Soothing Wipes"/>
					</div>
					<div class="slick-arrow left">@include('main.range.partials._arrow')</div>
					<div class="slick-arrow right">@include('main.range.partials._arrow')</div>
					{{--<img src="/images/products/25-and-10-wipes.jpg?{{{$version}}}" alt="25 and 10 Freshening and Soothing Wipes"/>--}}
					<h1 class="h2">on-the-go</h1>
					<div class="button-switcher" data-show="25">
						<a href="{{ __('links.25-wipes') }}" target="_blank" rel="nofollow" data-track="25-wipes" data-type="buy-now" class="bTrack btn" data-btn="25">BUY NOW</a>
						<a href="{{ __('links.10-wipes') }}" target="_blank" rel="nofollow" data-track="10-wipes" data-type="buy-now" class="bTrack btn" data-btn="10">BUY NOW</a>
					</div>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">on-the-go</h1>
					<div class="box">
						<p class="intro">Keep things cool on-the-go with femfresh freshening &amp; soothing wipes</p>
						<p>Gym, meeting, dinner date? All in one day!? Thank goodness for these soothing wipes with calendula &amp; aloe extracts. They come in portable packs of 10 and 25, to freshen and soothe your intimate skin when you don&rsquo;t have access to H2O. That&rsquo;s H2WOW!</p>
						<!-- <p class="themed mt2"><strong>250ml</strong></p> -->
						<ul class="mt2">
							<li>A festival must-have</li>
							<li>Enriched with calendula &amp; soothing mallow</li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Hypoallergenic and soap free</li>
							<li>MULTIActif complex</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.25-wipes') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="25-wipes" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>femfresh wipes are ideal to freshen up on-the-go. To use, wipe intimate skin front to back as you would with toilet paper.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Aqua, Polysorbate 80, Aloe Barbadensis Leaf Juice, Chamomilla Recutita Flower Water, Malva Sylvestris Extract, Calendula Officinalis Extract, Panthenol, Tocopheryl Acetate, Lactic Acid, Glycerin, C12-15 Alkyl Benzoate, Triisostearin, Ceteth-2, Glyceryl Stearate, Diglycerin, Cetyl Palmitate, Cetearyl Alcohol, Lauryl Alcohol, Myristyl Alcohol, Ceteareth-25, C12-15 Pareth-3, C12-15 Pareth-3 Phosphate, Dimethicone, Benzoic Acid, Dehydro-Acetic Acid, Ethylhexylglycerin, PEG-40 Hydrogenated Castor Oil, Trideceth-9, Phenoxyethanol, Methylparaben, Ethylparaben, Propylparaben, Imidazolidinyl Urea, Sodium Benzoate, Potassium Sorbate, Parfum</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>What are the benefits of using femfresh intimate wipes, rather than toilet tissue or baby wipes?</h5>
							<div class="reveal">
								<p>Using femfresh intimate wipes can help to maintain a healthy pH-balance, whilst giving you that longer lasting feeling of freshness. The gentle formula, with calendula &amp; soothing aloe extracts, is perfect for a quick refresh when you don&rsquo;t have access to water.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>When would I use femfresh intimate wipes?</h5>
							<div class="reveal">
								<p>Whether you&rsquo;ve been working out at the gym, dancing at a festival or simply just want a refresh during the day, the gentle pH-balanced formula with calendula &amp; soothing aloe extracts is perfect for busy lifestyles and on the go freshness. They&rsquo;re also ideal for when you&rsquo;re on your period as they fit discreetly in your bag.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>Is femfresh tested by gynaecologists?</h5>
							<div class="reveal">
								<p>Yes. All femfresh products are dermatologically and gynaecologically tested by experts. They&rsquo;re also hypoallergenic, soap free and alcohol free.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-wipes">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;If you have an active lifestyle, the Freshening &amp; Soothing wipes are brilliant as a temporary solution to remove sweat without disrupting the pH-balance. The packaging fits perfectly in your bag for any time of day.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['daily-wash','freshness-deodorant','post-shave-balm']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')
	
@endsection