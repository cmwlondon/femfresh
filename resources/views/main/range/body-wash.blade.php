@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="body-wash">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/body-wash.jpg?{{{$version}}}" alt="Daily Intimate Wash"/>
					<h1 class="h2">0% wash</h1>
					<a href="{{ __('links.body-wash') }}" target="_blank" rel="nofollow" data-track="body-wash" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">0% wash</h1>
					<div class="box">
						<p class="intro">Give your intimate skin the gentle loving care it deserves, with femfresh 0% wash for sensitive skin</p>
						<p>There are plenty of things in life that&rsquo;ll irritate you&hellip; But, with our gentle 0% Wash, your intimate skin won’t be one of them. Enriched with purifying Lotus Flower Extract, its carefully pH-balanced formula works in harmony with your intimate skin. 0% soap, fragrance or colour. 100% gentle and effective cleansing for sensitive skin, every day.</p>
						<ul class="mt2">
							<li>pH-balanced for intimate skin</li>
							<li>Fragrance, colour and soap-free</li>
							<li>Vegan friendly</li>
							<li>94% natural original ingredients</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.body-wash') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="body-wash" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Use femfresh 0% Body Wash daily to cleanse your intimate skin. Lather up and then rinse thoroughly with warm water.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Aqua, Sodium Laureth Sulfate, Sodium Chloride, Cocamidopropyl Betaine, Triethyl Citrate, Glycerin, Polyquaternium-39, Dichlorobenzyl Alcohol, Lactic Acid, Propylene Glycol, Sodium Lactate, Hamamelis Virginiana Leaf Extract, Nelumbo Nucifera Flower Extract, 2-Bromo-2-Nitropropane-1, 3-Diol, Sodium Benzoate, Calendula Officinalis Flower Extract, Maltodextrin, Bambusa Vulgaris Leaf Extract, Aloe Barbadensis Extract, Sodium Hydroxide, Sodium Carbonate.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>How often can I use femfresh intimate washes?</h5>
							<div class="reveal">
								<p>Each <a href="{!! route('range') !!}#washes">femfresh wash</a> is suitable for everyday use.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>Can I use femfresh on other parts of my body?</h5>
							<div class="reveal">
								<p>femfresh intimate washes are pH-balanced and specifically designed for use on your intimate skin but they can also be used safely on other external parts of the body.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>How come there are different washes in the femfresh range? What are they for?</h5>
							<div class="reveal">
								<p>All our intimate washes are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
<p><a href="{!! route('range') !!}#washes">femfresh intimate washes</a> come in four different formulas so you can choose the perfect care to suit your intimate skin. The intimate washes range includes Daily Intimate Wash with a hint of soothing aloe vera to keep your intimate skin happy all-day long.</p>
<p>The Ultimate Care range consists of Pure Wash, Soothing Wash and Active Wash. The Pure Wash with lactic acid and caring aloe vera is fragrance free and specially formulated for those with sensitive skin. Our Soothing Wash has a moisturising formula with a probiotic complex, and is enriched with aloe vera and cranberry extracts best known for their healing and hydrating properties. The Active Wash provides all day, feel good confidence, with ginseng and antioxidants that work in harmony with your body to provide up to 24 hours of freshness.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-body-wash">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;I am absolutely delighted to see 0% be launched, this lovely environmentally friendly product contains no allergenic ingredients and is perfectly balanced to provide optimal skin care for the intimate area. It is PH balanced to ensure that the skin is cleansed and protected in the best way possible.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	@include('main.components._ymal', ['products' => ['re-balance-powder', 'freshness-deodorant', 'soothing-wash']])


	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
