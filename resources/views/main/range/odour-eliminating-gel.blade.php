@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="odour-eliminating-gel">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">

<img src="/images/products/odour-eliminating-gel.jpg?{{{$version}}}" alt="Ultimate Care Soothing Wash"/>

					<h1 class="h2">Femfresh Solutions &trade; odour eliminating gel</h1>
					<a href="{{ __('links.odour-eliminating-gel') }}" target="_blank" rel="nofollow" data-track="odour-eliminating-gel" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">Femfresh Solutions &trade; odour eliminating gel</h1>
					<div class="box">
						<p class="intro">Helps eliminate unwanted vaginal odours and&nbsp;discomfort</p>
						<p>Take control of your feminine health and balance - and enjoy long-lasting freshness and confidence. A single application can maintain vaginal pH in the normal range for 72 hours or&nbsp;more.</p>
						<ul class="mt2">
							<li>Eliminates unpleasant odours for up to 3 days</li><li>Helps maintain a healthy vaginal pH</li><li>1 applicator = up to 3 days of freshness</li><li>Hormone Free</li><li>Clinically tested</li><li>For internal use</li>

						</ul>
						{{-- <p class="review mt2"><a href="{{ __('links.odour-eliminating-gel') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="ultimate-care-soothing-wash" data-type="reviews">Read Reviews</a></p> --}}
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Balances pH to eliminate feminine odour and relieve minor discomfort. FEMFRESH SOLUTIONS&trade; Odour Eliminating Gel is inserted directly into the vagina. Each application lasts 3&nbsp;days.</p>
								<h5>WHEN TO USE</h5>
								<ul>
								<li>After your period</li><li>Before or after sex</li><li>After douching</li><li>For freshness anytime</li>
								</ul>
								
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Purified Water, Glycerin, Polycarbophil, Carbomer Homopolymer Type B, Benzyl Alcohol, Ethylhexylglycerin, Sodium Hydroxide, Tocopherol.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			{{--
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>QUESTION</h5>
							<div class="reveal">
								<p>ANSWER</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>QUESTION</h5>
							<div class="reveal">
								<p>ANSWER</p>
							</div>
						</li>


					</ul>
				</div>
			</div>
			--}}
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-soothing-wash">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;femfresh Ultimate Care Soothing Wash is great for everyone but especially if you have a tendency to skin irritation. Maintaining a healthy <a href="{!! route('about-ph-balance') !!}">pH-balance</a> is very important for you, so using femfresh intimate washes is a good choice to help you achieve this.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['daily-wash','pure-wash','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
