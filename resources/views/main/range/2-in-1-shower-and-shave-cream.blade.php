@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="shave-cream">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/2-in-1-shower-and-shave-cream.jpg?{{{$version}}}" alt="2 in 1 Shower and Shave Cream"/>
					<h1 class="h2">smooth</h1>
					<a href="{{ __('links.2-in-1-shower-shave-cream') }}" target="_blank" rel="nofollow" data-track="2-in-1-shower-shave-cream" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">smooth</h1>
					<div class="box">
						<p class="intro">Take your bikini line to new levels of smooth with femfresh 2in1 Shower &amp; Shave Cream</p>
						<p>There&rsquo;s smooth and there&rsquo;s smooth. Then, there&rsquo;s femfresh 2in1 Shower &amp; Shave Cream smooth. The moisturising formula reduces redness, irritation and razor bumps and is enriched with moisturising almond oil and hydrating argan oil.</p>
						<!-- <p class="themed mt2"><strong>250ml</strong></p> -->
						<ul class="mt2">
							<li>Enriched with argan oil &amp; almond oil</li>
							<li>pH-balanced for intimate skin</li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Can also be used as a daily intimate wash</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.2-in-1-shower-shave-cream') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="2-in-1-shower-shave-cream" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Massage cream into dampened skin &amp; shave in the direction of hair growth. Can also be used as an everyday intimate wash. Use daily alongside femfresh <a href="{!! route('post-shave-balm') !!}">post-shave balm</a> for a complete soothing &amp; smoothing experience.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Aqua, Sodium Laureth Sulfate, Sodium Chloride, Cocamidopropyl Betaine, Argania Spinosa (Argan) Oil, Prunus Amygdalus Dulcis (Sweet Almond) Oil, Plukenetia Volubilis Seed Oil, Polymnia Sonchifolia Root Juice, Glycerin, Lactic Acid, Alpha-Glucan Oligosaccharide, Maltodextrin, Lysine, Lactobacillus, Sodium Lactate, Carbomer, Triethyl Citrate, Sodium Hydroxide, Parfum, Polyquaternium-39, Sodium Carbonate, Sodium Benzoate, Dichlorobenzyl Alcohol, 2-Bromo-2-Nitropropane-1,3-Diol, CI 77891</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>What is the best way to shave my bikini line?</h5>
							<div class="reveal">
								<p>There are lots of different ways to take care of your bikini line and now, femfresh has developed a new 2in1 Shower and Shave Cream which has been specifically formulated to care for your delicate intimate skin when shaving. The moisturising formula reduces redness, irritation and razor bumps that shaving your bikini area often results in. Enriched with moisturising almond oil and hydrating argan oil, it’s so gentle, it can also be used as an intimate wash.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>How can I care for my bikini line post-shaving?</h5>
							<div class="reveal">
								<p>femfresh <a href="{!! route('post-shave-balm') !!}">Post-Shave Balm</a> is part of the new product range designed to care for your intimate skin during your shaving routine. This skin loving, <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> cream has been specifically formulated to calm and soothe delicate skin post hair removal, and will leave your intimate skin feeling beautiful, soft and smoother for longer. Enriched with blackcurrant oil to soothe, aloe vera to calm and sweet almond oil to moisturise, the formula can be used daily to minimise ingrown hairs, reduce razor bumps and redness.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>What makes the femfresh shave range different to other shave products?</h5>
							<div class="reveal">
								<p>As with all the products in the <a href="{!! route('range') !!}">femfresh range</a>, the shaving collection is pH-balanced and specially formulated to care for and protect your delicate intimate skin. Each product is hypoallergenic, and gynaecologist and dermatologist approved – so gentle they can be used every day.</p>
							</div>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-shave-cream">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;When shaving your intimate area it is important to use products that work in harmony with your body. Products such as the femfresh Shave &amp; Shower Cream allow you to get the hair-free results you want without compromising your natural pH-balance.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shave-cream-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['post-shave-balm','daily-wash','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
