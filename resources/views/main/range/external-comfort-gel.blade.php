@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="external-comfort-gel">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
<img src="/images/products/external-comfort-gel1.jpg?{{{$version}}}" alt="Ultimate Care Soothing Wash"/>
{{--
					<div class="productSlick">
						<div class="column">
							<div class="pic"><img src="/images/products/external-comfort-gel1.jpg?{{{$version}}}" alt="Ultimate Care Soothing Wash"/></div>
						</div>
						<div class="column">
							<div class="pic"><img src="/images/products/external-comfort-gel2.jpg?{{{$version}}}" alt="Ultimate Care Soothing Wash"/></div>
						</div>
					</div>
--}}
					<h1 class="h2">Femfresh Solutions&trade; external comfort gel</h1>
					<a href="{{ __('links.external-comfort-gel') }}" target="_blank" rel="nofollow" data-track="external-comfort-gel" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">Femfresh Solutions&trade; external comfort gel</h1>
					<div class="box">
						<p class="intro">Helps soothe and relieve external vaginal&nbsp;dryness.</p>
						<p>The moisturizing formula with Hydrapro Complex&trade; also includes Vitamin E and Pro-Vitamin B5; to help relieve irritation, discomfort & chafing from external dryness, so you feel like yourself&nbsp;again&hellip;</p>
						<ul class="mt2">
<li>Use daily to help soothe external vaginal dryness</li>
<li>Helps you enjoy intimacy again</li>
<li>Feel like yourself again</li>
<li>Fragrance free</li>
<li>For external use</li>
						</ul>
						{{-- <p class="review mt2"><a href="{{ __('links.external-comfort-gel') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="external-comfort-gel" data-type="reviews">Read Reviews</a></p> --}}
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Wash hands before and after application. Apply a fingertip amount of FEMFRESH SOLUTIONS™ External Comfort Gel to the external vaginal&nbsp;area.</p>
								<h5>WHEN TO USE</h5>
								<p>Use daily or as needed for continuous comfort of dry external vaginal skin. Use as much as&nbsp;desired.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Aqua, Glycerin, Paraffinum Liquidum, Polycarbophil, Hydrogenated Palm Glyceride, Panthenol, Benzyl Alcohol, Carbomer, Tocopheryl Acetate, Sodium Hydroxide, Ethylhexylglycerin, Sorbic Acid, Tocopherol, Potassium Carbonate, Acrylic Acid.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			{{--
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>QUESTION</h5>
							<div class="reveal">
								<p>ANSWER</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>QUESTION</h5>
							<div class="reveal">
								<p>ANSWER</p>
							</div>
						</li>

					</ul>
				</div>
			</div>
			--}}
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-soothing-wash">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;femfresh Ultimate Care Soothing Wash is great for everyone but especially if you have a tendency to skin irritation. Maintaining a healthy <a href="{!! route('about-ph-balance') !!}">pH-balance</a> is very important for you, so using femfresh intimate washes is a good choice to help you achieve this.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['daily-wash','pure-wash','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
