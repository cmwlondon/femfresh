@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="re-balance-powder">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/re-balance-powder.jpg?{{{$version}}}" alt="Re-Balance Powder"/>
					<h1 class="h2">absorb</h1>
					{{-- <a href="{{ __('links.re-balance-powder') }}" target="_blank" rel="nofollow" data-track="re-balance-powder" data-type="buy-now" class="bTrack btn">BUY NOW</a> --}}
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">absorb</h1>
					<div class="box">
						<p class="intro">Soak up the suds for added comfort with femfresh Re-Balance Powder</p>
						<p>Absorb moisture after cleansing intimate skin with this specially designed, perfectly <a href="{!! route('about-ph-balance') !!}">pH-balanced intimate powder</a>. The delicately scented and delicately formulated talc-free dust gives you extra level comfort and freshness.</p>
						<!-- <p class="themed mt2"><strong>250ml</strong></p> -->
						<ul class="mt2">
							<li>pH-balanced for intimate skin</li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Talc-free formula</li>
						</ul>
						{{-- <p class="review mt2"><a href="{{ __('links.re-balance-powder') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="re-balance-powder" data-type="reviews">Read Reviews</a></p> --}}
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Dust onto intimate skin post bath or shower to absorb moisture</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Zea Mays Starch, Tricalcium Phosphate, Parfum, Linalool, Limonene, Benzyl Salicylate, Hydroxycitronellal, Hydroxyisohexyl 3-Cyclohexene Carboxaldehyde, Citronellol, Alpha - Isomethyl Ionone, Benzyl Alcohol, Butylphenyl Methylpropional, Eugenol, Benzyl Benzoate, Hexyl Cinnamal, Geraniol, Isoeugenol, Citral</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>What is intimate skincare?</h5>
							<div class="reveal">
								<p>When it comes to our skin, we all use different products for different parts of our bodies. Just like shampoo could irritate your face and body lotion may leave your hair feeling less than perfect, using the wrong products on your intimate vulval skin – that protects your vagina – may result in itching or irritation. That&rsquo;s where intimate skincare can help.</p>
								<p>The <a href="{!! route('range') !!}">femfresh range</a> is specifically designed to love your bits to bits. It&rsquo;s a <a href="{!! route('about-ph-balance') !!}">pH-balanced collection</a> with formulas designed to protect your vagina&rsquo;s natural environment, avoid irritation and leave you feeling pHAB!</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>Is femfresh tested by gynaecologists?</h5>
							<div class="reveal">
								<p>Yes. All femfresh products are dermatologically and gynaecologically tested by experts. They&rsquo;re also hypoallergenic, soap free and alcohol free.</p>
							</div>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>

	{{--<div class="container dr-sara bgw" data-ref="dr-sara-re-balance-powder">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;Awaiting quote&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>--}}

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['daily-wash','freshness-deodorant','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
