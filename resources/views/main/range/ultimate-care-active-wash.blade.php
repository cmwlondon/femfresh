@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="active-fresh-wash">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/ultimate-care-active-fresh-wash.jpg?{{{$version}}}" alt="Ultimate Care Active Wash"/>
					<h1 class="h2">active</h1>
					<a href="{{ __('links.ultimate-care-active-fresh-wash') }}" target="_blank" rel="nofollow" data-track="ultimate-care-active-fresh-wash" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">active</h1>
					<div class="box">
						<p class="intro">Refresh your post-workout routine with femfresh Ultimate Care Active Wash</p>
						<p>Stay super-chilled for up to 24 hours with our energising formula. This <a href="{!! route('about-ph-balance') !!}">pH-balanced wash</a> is charged with silver-ions technology, ginseng extract and antioxidants to hydrate your intimate skin and give you that super fresh feeling all-day long. Work it, girl.</p>
						<!-- <p class="themed mt2"><strong>250ml</strong></p> -->
						<ul class="mt2">
							<li>Blend of silver ions, ginseng and antioxidants</li>
							<li>pH-balanced for intimate skin</li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Hypoallergenic and soap free</li>
							<li>MULTIActif complex</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.ultimate-care-active-fresh-wash') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="ultimate-care-active-fresh-wash" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Use femfresh Ultimate Care Active Wash daily to cleanse your intimate skin. Lather up and then rinse thoroughly with warm water.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<!-- <p>Aqua Sodium Laureth Sulfate Sodium Chloride Cocamidopropyl Betaine Glycol Distearate Triethyl Citrate Parfum Laureth-4 Polyquaternium-39 Sodium Hydroxide Dichlorobenzyl Alcohol Glycerin Lactic Acid Sodium Carbonate Butylene Glycol Propylene Glycol Citric Acid (and) Silver Citrate 2-Bromo-2-Nitropropane-1, 3-Diol Sodium Benzoate Calendula Officinalis Extract Lilium Candidum Flower Extract Plumeria Alba Extract Methylparaben Propylparaben Sodium Lactate</p> -->
								<p>Aqua, Sodium Laureth Sulfate, Sodium Chloride, Glycol Distearate, Cocamidopropyl Betaine, Citrus Aurantium (Bitter Orange) Fruit Extract, Panax Ginseng Extract, Calendula Officinalis Flower Extract,  Hamamelis Virginiana Leaf Extract, Aloe Barbadensis Extract, Citric Acid (and) Silver Citrate, Glycerin, Lactic Acid,  Maltodextrin, Sodium Lactate, Triethyl Citrate, Parfum, Laureth-4, Polyquaternium-39, Dichlorobenzyl Alcohol, 2-Bromo-2-Nitropropane-1,3-Diol, Sodium Benzoate, Sodium Hydroxide, Sodium Carbonate</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>How often can I use femfresh intimate washes?</h5>
							<div class="reveal">
								<p>Each <a href="{!! route('range') !!}#washes">femfresh wash</a> is suitable for everyday use.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>Can I use femfresh on other parts of my body?</h5>
							<div class="reveal">
								<p>femfresh <a href="{!! route('range') !!}#washes">intimate washes</a> are <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> and specifically designed for use on your intimate skin but they can also be used safely on other external parts of the body.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>How come there are different washes in the femfresh range? What are they for?</h5>
							<div class="reveal">
								<p>All our intimate washes are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
								<p>femfresh intimate washes come in four different formulas so you can choose the perfect care to suit your intimate skin. The intimate washes range includes <a href="{!! route('daily-intimate-wash') !!}">Daily Intimate Wash</a> with a hint of soothing aloe vera to keep your intimate skin happy all-day long.</p>
								<p>The Ultimate Care range consists of <a href="{!! route('ultimate-care-soothing-wash') !!}">Soothing Wash</a> and <a href="{!! route('ultimate-care-active-wash') !!}">Active Wash</a>. The Pure Wash with lactic acid and caring aloe vera is fragrance free and specially formulated for those with sensitive skin. Our Soothing Wash has a moisturising formula with a probiotic complex, and is enriched with aloe vera and cranberry extracts best known for their healing and hydrating properties. The Active Wash provides all day, feel good confidence, with ginseng and antioxidants that work in harmony with your body to provide up to 24 hours of freshness.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-active-fresh-wash">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;If you have a busy lifestyle and often go back to work after the gym, Ultimate Care Active Wash is the perfect partner for active women.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['daily-wash','ultimate-care-active-fresh-deodorant','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
