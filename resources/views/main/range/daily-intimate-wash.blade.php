@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="daily-intimate-wash">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/daily-intimate-wash.jpg?{{{$version}}}" alt="Daily Intimate Wash"/>
					<h1 class="h2">daily</h1>
					<a href="{{ __('links.daily-intimate-wash') }}" target="_blank" rel="nofollow" data-track="daily-intimate-wash" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">daily</h1>
					<div class="box">
						<p class="intro">Treat yourself to the nation&rsquo;s favourite, femfresh Daily Intimate Wash</p>
						<p>This bottle of daily intimate goodness is the perfect partner for a quick spruce in the shower. The specially designed <a href="{!! route('about-ph-balance') !!}">pH-balanced formula</a> is enriched with soothing aloe vera to keep intimate skin happy and feeling fresh all-day long.</p>
						<ul class="mt2">
							<li>With a hint of soothing aloe</li>
							<li>pH-balanced for intimate skin</li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Hypoallergenic and soap free</li>
							<li>Also available in cabin-friendly 50ml travel size</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.daily-intimate-wash') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="daily-intimate-wash" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Use femfresh intimate wash daily to cleanse your intimate skin. Lather up and then rinse thoroughly with warm water. For external use only.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Aqua, Sodium Laureth Sulfate, Sodium Chloride, Cocamidopropyl Betaine, Triethyl Citrate, Parfum, Polyquaternium-39, Dichlorobenzyl Alcohol,  Glycerin,  Lactic Acid,  Sodium Lactate,  2-Bromo-2-Nitropropane-1,3-Diol, Linalool, Limonene, Sodium Benzoate, Calendula Officinalis Flower Extract,  Maltodextrin, Aloe Barbadensis Extract, Sodium Hydroxide,  Sodium Carbonate,  CI 14720, CI 15510</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>How often can I use femfresh intimate washes?</h5>
							<div class="reveal">
								<p>All our intimate washes are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
								<p>Each <a href="{!! route('range') !!}#washes">femfresh wash</a> is suitable for everyday use.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>Can I use femfresh on other parts of my body?</h5>
							<div class="reveal">
								<p>All our <a href="{!! route('range') !!}#washes">intimate washes</a> are <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
								<p>femfresh intimate washes are pH-balanced and specifically designed for use on your intimate skin but they can also be used safely on other external parts of the body.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>How come there are different washes in the femfresh range? What are they for?</h5>
							<div class="reveal">
								<p>All our intimate washes are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
								<p>femfresh intimate washes come in four different formulas so you can choose the perfect care to suit your intimate skin. The intimate washes range includes <a href="{!! route('daily-intimate-wash') !!}">Daily Intimate Wash</a> with a hint of soothing aloe vera to keep your intimate skin happy all-day long.</p>
								<p>The Ultimate Care range consists of <a href="{!! route('ultimate-care-soothing-wash') !!}">Soothing Wash</a> and <a href="{!! route('ultimate-care-active-wash') !!}">Active Wash</a>. The Pure Wash with lactic acid and caring aloe vera is fragrance free and specially formulated for those with sensitive skin. Our Soothing Wash has a moisturising formula with a probiotic complex, and is enriched with aloe vera and cranberry extracts best known for their healing and hydrating properties. The Active Wash provides all day, feel good confidence, with ginseng and antioxidants that work in harmony with your body to provide up to 24 hours of freshness.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-daily-intimate-wash">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;Using an intimate wash is a personal choice and for women who want to use anything more than water, it is important to choose a product that works in harmony with your body. The femfresh Daily Intimate Wash is safe to use daily if needed so great for those women that want to use more than water. It is particularly good for women who have some skin irritation or infection, as it is specially designed to soothe the skin and reduce irritation.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['soothing-wash','freshness-deodorant','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
