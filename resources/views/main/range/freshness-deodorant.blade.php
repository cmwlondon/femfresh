@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="freshness-deodorant">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/freshness-deodorant.jpg?{{{$version}}}" alt="Freshness Deodorant"/>
					<h1 class="h2">spritz</h1>
					<a href="{{ __('links.freshness-deodorant') }}" target="_blank" rel="nofollow" data-track="freshness-deodorant" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">spritz</h1>
					<div class="box">
						<p class="intro">A delicate mist for delicate skin, femfresh freshness deodorant</p>
						<p>This intimate spray is delicately fragranced for your delicate intimate skin. Specially designed with calendula, moisturising silk extracts and MULTIActif complex for an extra layer of protection for that fresh feeling for longer.</p>
						<!-- <p class="themed mt2"><strong>250ml</strong></p> -->
						<ul class="mt2">
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>24-hour protection</li>
							<li>MULTIActif complex</li>
							<li>Also available in cabin-friendly 50ml travel size</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.freshness-deodorant') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="freshness-deodorant" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Spritz femfresh deodorants onto intimate skin or directly onto undwerwear for day-long intimate protection</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Butane, Isobutane, Propane, Disiloxane, Zea Mays Starch, Cyclopentasiloxane, Disteardimonium Hectorite, Propylene Carbonate, Chlorhexidine Dihydrochloride, Parfum, Serica Powder, Helianthus Annuus Seed Oil, Calendula Officinalis Flower Extract, Triethyl Citrate</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>When is the best time to use femfresh Freshness Deodorant, and is it for everyday&nbsp;use?</h5>
							<div class="reveal">
								<p>Our intimate deodorant range uses a gentle formula which can be used every day at any time of the day.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>Are intimate deodorants safe to use?</h5>
							<div class="reveal">
								<p>Our intimate deodorant range is safe to use every day around your external intimate area or directly onto spritz.</p>
							</div>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-freshness-deodorant">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;For those that like to pay a little daily attention to their intimate area. This isn&rsquo;t a perfume, it simply provides a refreshing &lsquo;zizz&rsquo; before you start the day, or for a fast freshen up later on. Not designed to hide or treat a vaginal odour that may be infection related - that&rsquo;s when you&rsquo;ll need to speak to your doctor&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['active-wash','ultimate-care-active-fresh-deodorant','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
