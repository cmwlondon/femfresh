@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="shave-balm">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/post-shave-balm.jpg?{{{$version}}}" alt="Post Shave Balm"/>
					<h1 class="h2">soothe</h1>
					<a href="{{ __('links.post-shave-balm') }}" target="_blank" rel="nofollow" data-track="post-shave-balm" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">soothe</h1>
					<div class="box">
						<p class="intro">Be-you-tiful with soothing femfresh post-shave balm.</p>
						<p>Calm and soothe your skin post hair removal with this skin-loving <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> delight. Enriched with blackcurrant oil, almond oil and aloe vera, the specially designed moisturising formula helps minimise ingrown hairs, reduce redness and keep intimate skin feeling smoother for longer.</p>
						<!-- <p class="themed mt2"><strong>250ml</strong></p> -->
						<ul class="mt2">
							<li>Enriched with blackcurrant oil, almond oil and aloe vera</li>
							<li>pH-balanced for intimate skin &amp; <strong>fragrance free</strong></li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Use after shaving, waxing or depilatory creams</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.post-shave-balm') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="post-shave-balm" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Use straight after hair removal. Put a small amount onto fingers and rub gently into skin. Use daily alongside femfresh 2in1 shower and shave cream for a complete soothing and smoothing experience.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<!-- <p>Aqua, Cetyl Alcohol, Stearyl Alcohol, Glyceryl Stearate SE, Isopropyl Palmitate, Ceteareth-20, Salicylic Acid, Aloe Barbadensis Extract, Prunus Amygdalus Dulcis (Sweet Almond) Oil, Ribes Nigrum (Black Currant Seed) Oil, Helianthus Annuus (Sunflower) Seed Oil, Rosmarinus Officinalis (Rosemary Extract), Cardiospermum Halicacabum Flower/Leaf/Vine Extract, Tocopherol, Helianthus Annuus Seed Oil Unsaponifiables, Allantoin, Maltodextrin, Octyldodecanol, Sodium Hydroxide, Phenoxyethanol</p> -->
								<p>Aqua, Cetyl Alcohol, Stearyl Alcohol, Glyceryl Stearate SE, Isopropyl Palmitate, Ceteareth-20, Salicylic Acid, Aloe Barbadensis Extract, Prunus Amygdalus Dulcis (Sweet Almond) Oil, Ribes Nigrum (Black Currant Seed) Oil, Helianthus Annuus  (Sunflower) Seed Oil, Rosmarinus Officinalis (Rosemary) Extract, Cardiospermum Halicacabum Flower/Leaf/Vine Extract, Tocopherol, Helianthus Annuus Seed Oil Unsaponifiables, Allantoin, Maltodextrin, Octyldodecanol, Sodium Hydroxide, Phenoxyethanol</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>What is the best way to shave my bikini line?</h5>
							<div class="reveal">
								<p>There are lots of different ways to take care of your bikini line and now, femfresh has developed a new <a href="{!! route('2-in-1-shower-shave-cream') !!}">2in1  Shower and Shave Cream</a> which has been specifically formulated to care for your delicate intimate skin when shaving. The moisturising formula reduces redness, irritation and razor bumps that shaving your bikini area often results in. Enriched with moisturising almond oil and hydrating argan oil, it&rsquo;s so gentle, it can also be used as an intimate wash.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>How can I care for my bikini line post-shaving?</h5>
							<div class="reveal">
								<p>femfresh Post-Shave Balm is part of the new product range designed to care for your intimate skin during your shaving routine. This skin loving, <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> cream has been specifically formulated to calm and soothe delicate skin post hair removal, and will leave your intimate skin feeling beautiful, soft and smoother for longer. Enriched with blackcurrant oil to soothe, aloe vera to calm and sweet almond oil to moisturise, the formula can be used daily to minimise ingrown hairs, reduce razor bumps and redness.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>What makes the femfresh shave range different to other shave products?</h5>
							<div class="reveal">
								<p>As with all the products in the femfresh range, the shaving collection is pH-balanced and specially formulated to care for and protect your delicate intimate skin. Each product is hypoallergenic, and gynaecologist and dermatologist approved – so gentle they can be used every day.</p>
							</div>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-shave-balm">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;The process of shaving around the intimate area often causes a degree of inflammation, and skin can feel irritated for a while afterwards. femfresh post shave balm is a great way to soothe the skin, making you feel comfortable immediately.&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['2-in-1-shower-shave-cream','daily-wash','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
