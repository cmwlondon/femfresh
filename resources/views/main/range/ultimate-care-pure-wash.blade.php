@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container product" data-ref="pure-wash">
		<div class="inner-fix">
			<div class="col odd product-image mb0">
				<div class="inner">
					<img src="/images/products/ultimate-care-pure-wash.jpg?{{{$version}}}" alt="Ultimate Care Pure Wash"/>
					<h1 class="h2">pure</h1>
					<a href="{{ __('links.ultimate-care-pure-wash') }}" target="_blank" rel="nofollow" data-track="ultimate-care-pure-wash" data-type="buy-now" class="bTrack btn">BUY NOW</a>
				</div>
			</div>
			<div class="col even product-description">
				<div class="inner">
					<h1 class="h2">pure</h1>
					<div class="box">
						<p class="intro">Treat your intimate skin to this wonder of a wash, femfresh Ultimate Care Pure Wash</p>
						<p>Filled with all things caring and kind to intimate skin, this <a href="{!! route('about-ph-balance') !!}">pH-balanced wash</a> has a specially designed formula that's infused with the delights of lotus flower extract. Add our MUTIActif purifying complex to that mix, and you&rsquo;ll be feeling fresh all day. Can we get a 'whoop'?</p>
						<!-- <p class="themed mt2"><strong>250ml</strong></p> -->
						<ul class="mt2">
							<li>Enriched with lotus flower extract</li>
							<li>pH-balanced for intimate skin</li>
							<li>Dermatologically &amp; gynaecologically tested</li>
							<li>Hypoallergenic and soap free</li>
							<li>MULTIActif complex</li>
							<li>Fragrance free &amp; suitable for sensitive skin</li>
						</ul>
						<p class="review mt2"><a href="{{ __('links.ultimate-care-pure-wash') }}#BVRRContainer" class="bTrack" target="_blank" rel="nofollow" data-track="ultimate-care-pure-wash" data-type="reviews">Read Reviews</a></p>
					</div>
				</div>
			</div>
			<div class="newrow"></div>
			<div class="col odd how-to-use">
				<div class="inner">
					<ul data-reveal="0" class="node">
						<li data-id="1">
							@include('main.range.partials._arrow')
							<h5>How to use</h5>
							<div class="reveal">
								<p>Use femfresh Ultimate Care Pure Wash daily to cleanse your intimate skin. Lather up and then rinse thoroughly with warm water.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._arrow')
							<h5>Ingredients</h5>
							<div class="reveal">
								<p>Aqua, Sodium Laureth Sulfate, Sodium Chloride, Cocamidopropyl Betaine, Triethyl Citrate, Glycerin, Polyquaternium-39, Dichlorobenzyl Alcohol, Lactic Acid, Prropylene Glycol, Sodium Lactate, Hamamelis Virginiana Leaf Extract, Nelumbo Nucifera Flower Extract, 2-Bromo-2-Nitropropane-1,3-Diol, Sodium Benzoate, Calendula Officinalis Flower Extract, Maltodextrin, Bambusa Vulgaris Leaf Extract, Aloe Barbadensis Extract, Sodium Hydroxide, Sodium Carbonate</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col even faqs">
				<div class="inner">
					<h3>FAQs</h3>
					<ul data-reveal="0">
						<li data-id="1">
							@include('main.range.partials._plus_minus')
							<h5>How often can I use femfresh intimate washes?</h5>
							<div class="reveal">
								<p>Each <a href="{!! route('range') !!}#washes">femfresh wash</a> is suitable for everyday use.</p>
							</div>
						</li>

						<li data-id="2">
							@include('main.range.partials._plus_minus')
							<h5>Can I use femfresh on other parts of my body?</h5>
							<div class="reveal">
								<p>femfresh <a href="{!! route('range') !!}#washes">intimate washes</a> are <a href="{!! route('about-ph-balance') !!}">pH-balanced</a> and specifically designed for use on your intimate skin but they can also be used safely on other external parts of the body.</p>
							</div>
						</li>

						<li data-id="3">
							@include('main.range.partials._plus_minus')
							<h5>How come there are different washes in the femfresh range? What are they for?</h5>
							<div class="reveal">
								<p>All our intimate washes are pH-balanced and specifically designed for use on your intimate skin, so you can choose an intimate wash to suit you!</p>
								<p>femfresh intimate washes come in four different formulas so you can choose the perfect care to suit your intimate skin. The intimate washes range includes <a href="{!! route('daily-intimate-wash') !!}">Daily Intimate Wash</a> with a hint of soothing aloe vera to keep your intimate skin happy all-day long.</p>
								<p>The Ultimate Care range consists of <a href="{!! route('ultimate-care-pure-wash') !!}">Pure Wash</a>, <a href="{!! route('ultimate-care-soothing-wash') !!}">Soothing Wash</a> and <a href="{!! route('ultimate-care-active-wash') !!}">Active Wash</a>. The Pure Wash with lactic acid and caring aloe vera is fragrance free and specially formulated for those with sensitive skin. Our Soothing Wash has a moisturising formula with a probiotic complex, and is enriched with aloe vera and cranberry extracts best known for their healing and hydrating properties. The Active Wash provides all day, feel good confidence, with ginseng and antioxidants that work in harmony with your body to provide up to 24 hours of freshness.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="container dr-sara bgw" data-ref="dr-sara-pure-wash">
		<div class="inner-fix">
			<div class="text">
				<div class="box h2">Dr<br/>Sara<br/>says</div>
				<q>&ldquo;femfresh is great because it&rsquo;s specially designed to maintain the natural pH-balance of the intimate skin. Ultimate Care Pure Wash is suitable for sensitive skin and, along with the other <a href="{!! route('range') !!}#washes">femfresh washes</a>, safely maintains pH-balance and freshens your intimate area&rdquo;</q>
				<cite>Dr Sara - femfresh Gynaecologist</cite>
			</div>
		</div>
	</div>

	{{-- 'daily-wash','soothing-wash','active-wash','pure-wash','ultimate-care-active-fresh-deodorant','freshness-deodorant','25-wipes','10-wipes','2-in-1-shower-shave-cream','post-shave-balm' --}}
	@include('main.components._ymal', ['products' => ['soothing-wash','2-in-1-shower-shave-cream','25-wipes']])

	@include('main.components._range', ['first' => '', 'textshown' => false, 'bg' => 'bgw'])
@endsection

@section('components')

@endsection
