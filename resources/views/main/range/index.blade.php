@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
	@include('main.components._range', ['first' => 'first', 'textshown' => true, 'bg' => 'bg11'])

	<div class="container bgw" data-ref="range-solutions">
		<div class="anchor"></div>
		<div class="inner-fix">
			<h2 id="solutions" class="h2">Femfresh Solutions&trade;</h2>
			<p>Femfresh Solutions&trade; is a range of feminine treatments that will complement your daily hygiene routine and address specific needs of your intimate area.</p>
			<p>Specially designed for women who suffer from unpleasant odours, vaginal itch, irritation or dryness of the intimate skin. They work in harmony with your body to balance and maintain a healthy vaginal pH and relieve feminine discomfort.</p>
			<div class="range-group">
				 
				<div class="col">
					<div class="inner" data-theme="1">
						<img src="/images/range/product-external-comfort-gel.jpg?{{{$version}}}" alt="external-comfort-gel"/>
						<a href="{!! route('external-comfort-gel') !!}" class="blockout"></a>
						<h3>External comfort gel</h3>
						<a href="{!! route('external-comfort-gel') !!}" class="btn">View products</a>
					</div>
				</div>
				<div class="col">
					<div class="inner" data-theme="2">
						<img src="/images/range/product-odour-eliminating-gel.jpg?{{{$version}}}" alt="odour-eliminating-gel"/>
						<a href="{!! route('odour-eliminating-gel') !!}" class="blockout"></a>
						<h3>Odour eliminating gel</h3>
						<a href="{!! route('odour-eliminating-gel') !!}" class="btn">View products</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container bgw" data-ref="range-washes">
		<div class="anchor"></div>
		<div class="inner-fix">
			<h2 id="washes" class="h2">washes</h2>
			<div class="range-group">
				<div class="border"></div>

				<div class="col">
					<div class="inner" data-theme="1">
						<img src="/images/range/product-daily-wash.jpg?{{{$version}}}" alt="daily wash"/>
						<a href="{!! route('daily-intimate-wash') !!}" class="blockout"></a>
						<h3>daily <br class="hidden-sm hidden-md"/>wash</h3>
						<a href="{!! route('daily-intimate-wash') !!}" class="btn">Find out more</a>
					</div>
				</div>
				<div class="col">
					<div class="inner" data-theme="2">
						<img src="/images/range/product-soothing-wash.jpg?{{{$version}}}" alt="soothing wash"/>
						<a href="{!! route('ultimate-care-soothing-wash') !!}" class="blockout"></a>
						<h3>soothing <br class="hidden-sm hidden-md"/>wash</h3>
						<a href="{!! route('ultimate-care-soothing-wash') !!}" class="btn">Find out more</a>
					</div>
				</div>
				<div class="col">
					<div class="inner" data-theme="3">
						<img src="/images/range/product-active-wash.jpg?{{{$version}}}" alt="active wash"/>
						<a href="{!! route('ultimate-care-active-wash') !!}" class="blockout"></a>
						<h3>active <br class="hidden-sm hidden-md"/>wash</h3>
						<a href="{!! route('ultimate-care-active-wash') !!}" class="btn">Find out more</a>
					</div>
				</div>
				{{--
					<div class="col">
					<div class="inner" data-theme="4">
						<img src="/images/range/product-pure-wash.jpg?{{{$version}}}" alt="pure wash"/>
						<a href="{!! route('ultimate-care-pure-wash') !!}" class="blockout"></a>
						<h3>pure <br class="hidden-sm hidden-md"/>wash</h3>
						<a href="{!! route('ultimate-care-pure-wash') !!}" class="btn">Find out more</a>
					</div>
				</div>
				--}}

				<div class="col">
					<div class="inner" data-theme="5">
						<img src="/images/range/product-body-wash.jpg?{{{$version}}}" alt="0% body wash"/>
						<a href="{!! route('body-wash') !!}" class="blockout"></a>
						<h3>body <br class="hidden-sm hidden-md"/>wash</h3>
						<a href="{!! route('body-wash') !!}" class="btn">Find out more</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container bgw" data-ref="range-wipes">
		<div class="anchor"></div>
		<div class="inner-fix">
			<h2 id="wipes" class="h2">wipes</h2>
			<div class="range-group">
				<div class="border"></div>
				<div class="col">
					<div class="inner" data-theme="1">
						<img src="/images/range/product-25-wipes.jpg?{{{$version}}}" alt="25 freshening &amp; soothing wipes"/>
						<a href="{!! route('25-wipes') !!}" class="blockout"></a>
						<h3>Daily Wipes</h3>
						<a href="{!! route('25-wipes') !!}" class="btn">Find out more</a>
					</div>
				</div>
				<div class="col">
					<div class="inner" data-theme="1">
						<img src="/images/range/product-10-wipes.jpg?{{{$version}}}" alt="10 freshening &amp; soothing wipes"/>
						<a href="{!! route('10-wipes') !!}" class="blockout"></a>
						<h3>0% Sensitive Wipes</h3>
						<a href="{!! route('10-wipes') !!}" class="btn">Find out more</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container bgw" data-ref="range-sprays">
		<div class="anchor"></div>
		<div class="inner-fix">
			<h2 id="sprays" class="h2">sprays</h2>
			<div class="range-group">
				<div class="border"></div>
				<div class="col" data-title-size="2">
					<div class="inner" data-theme="1">
						<img src="/images/range/product-freshness-deodorant.jpg?{{{$version}}}" alt="freshness deodorant"/>
						<a href="{!! route('freshness-deodorant') !!}" class="blockout"></a>
						<h3>freshness <br/>deodorant</h3>
						<a href="{!! route('freshness-deodorant') !!}" class="btn">Find out more</a>
					</div>
				</div>
				<div class="col" data-title-size="2">
					<div class="inner" data-theme="2">
						<img src="/images/range/product-ultimate-care-active-fresh-deodorant.jpg?{{{$version}}}" alt="ultimate care active fresh deodorant"/>
						<a href="{!! route('ultimate-care-active-fresh-deodorant') !!}" class="blockout"></a>
						<h3>active fresh <br/>deodorant</h3>
						<a href="{!! route('ultimate-care-active-fresh-deodorant') !!}" class="btn">Find out more</a>
					</div>
				</div>
			</div>
		</div>
	</div>


	@include('main.components._dr-sara-ph-balance', ['shifted' => 'shifted', 'version' => 'range'])

	@include('main.components._vulva-or-vagina', ['shifted' => 'shifted'])

	@include('main.components._instagram', ['shifted' => 'shifted'])
@endsection

@section('components')

@endsection
