@extends('main.layouts.main')

@section('header')

@endsection

@section('content')

	<div class="container first" data-ref="contact">
		<div class="inner-fix">
			<div class="form">
				<h1 class="h2">contact us</h1>

				<div class="intro twoCol">
					<p class="lc">Got a question about femfresh products? Get in touch via our contact form below or call the femfresh team on 0800 121 6080.</p>
					<p class="rc">For press enquiries contact Capsule Communications Ltd<br>Tel: 0207 580 4312<br>Email. <a href="mailto:hello@capsulecomms.com">hello@capsulecomms.com</a></p>
				</div>

				<!-- display this message only if form validation fails -->
				<!-- <p class="mandatory {{ ($errors->any()) ? 'show-mandatory' : '' }}"><span>* Mandatory fields</span>: Failure to fill in these fields may prevent Church&amp; Dwight Co., Inc., including its affiliates Sofibel SAS, Church &amp; Dwight UK, Limited from processing your request.</p> -->

				@if (session('response'))
					<p class="h3 mt2 mb2">{{ session('response.message') }}</p>
				@else


				{!! Form::open(array('url' => [route('contact')], 'method' => 'POST', 'id' => 'supportForm') )!!}

				<div class="formRow  {{ $errors->has('title') ? 'has-error' : '' }}">
					{!! Form::label('title', 'Title: *') !!}
					{!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
					{!! $errors->first('title', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow {{ $errors->has('firstname') ? 'has-error' : '' }}">
					{!! Form::label('firstname', 'First Name: *') !!}
					{!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
					{!! $errors->first('firstname', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow {{ $errors->has('lastname') ? 'has-error' : '' }}">
					{!! Form::label('lastname', 'Last Name: *') !!}
					{!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
					{!! $errors->first('lastname', '<small class="error">:message</small>') !!}
				</div>
				<div class="formRow  {{ $errors->has('email') ? 'has-error' : '' }}">
					{!! Form::label('email', 'Email Address: *') !!}
					{!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
					{!! $errors->first('email', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('company', 'Company:') !!}
					{!! Form::text('company',null,['placeholder' => '', 'id' => 'company', 'class' => 'input']) !!}
					{!! $errors->first('company', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow  {{ $errors->has('address') ? 'has-error' : '' }}">
					{!! Form::label('address', 'Address: *') !!}
					{!! Form::text('address',null,['placeholder' => '', 'id' => 'address', 'class' => 'input']) !!}
					{!! $errors->first('address', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow">
					{!! Form::label('address2', 'Address 2:') !!}
					{!! Form::text('address2',null,['placeholder' => '', 'id' => 'address2', 'class' => 'input']) !!}
					{!! $errors->first('address2', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow {{ $errors->has('city') ? 'has-error' : '' }}">
					{!! Form::label('city', 'City: *') !!}
					{!! Form::text('city',null,['placeholder' => '', 'id' => 'city', 'class' => 'input']) !!}
					{!! $errors->first('city', '<small class="error">:message</small>') !!}
				</div>

				{{-- <div class="formRow">
					{!! Form::label('county', 'County:') !!}
					{!! Form::text('county',null,['placeholder' => '', 'id' => 'county', 'class' => 'input']) !!}
					{!! $errors->first('county', '<small class="error">:message</small>') !!}
				</div> --}}

				<div class="formRow {{ $errors->has('postcode') ? 'has-error' : '' }}">
					{!! Form::label('postcode', 'Postcode: *') !!}
					{!! Form::text('postcode',null,['placeholder' => '', 'id' => 'postcode', 'class' => 'input']) !!}
					{!! $errors->first('postcode', '<small class="error">:message</small>') !!}
				</div>

				<div class="formRow {{ $errors->has('country') ? 'has-error' : '' }}">
					{!! Form::label('country', 'Country: *') !!}
					{!! Form::select('country', $countryList, null, ['id' => 'country', 'class' => 'input'] ) !!}
					{{-- {!! Form::text('country',null,['placeholder' => '', 'id' => 'country', 'class' => 'input']) !!} --}} 
					{!! $errors->first('country', '<small class="error">:message</small>') !!}
				</div>

				<div class="conditionals cond_country @if (old('country', '') !== 'other') hide @endif">
					<div class="formRow {{ $errors->has('country_other') ? 'has-error' : '' }}">
						{!! Form::label('country_other', 'Country other: *') !!}
						{!! Form::text('country_other',null,['placeholder' => '', 'id' => 'country_other', 'class' => 'input']) !!}
						{!! $errors->first('country_other', '<small class="error">:message</small>') !!}
					</div>
				</div>

				<div class="formRow {{ $errors->has('phone') ? 'has-error' : '' }}">
					{!! Form::label('phone', 'Phone Number: *') !!}
					{!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
					{!! $errors->first('phone', '<small class="error">:message</small>') !!}
				</div>


				<!-- reason for enquiry -->
				<div class="formRow  {{ $errors->has('reason') ? 'has-error' : '' }}" >
					{!! Form::label('reason', 'Reason for enquiry: *') !!}
					{!! Form::select('reason', $reasonsArr, null, ['id' => 'reason', 'class' => 'input'] ) !!}
					{!! $errors->first('reason', '<small class="error">:message</small>') !!}
				</div>

				<!-- product enquiry -->
				<div class="conditionals cond_reason cond_product @if (old('reason', '') !== 'product') hide @endif">
					<div class="formRow {{ $errors->has('p_p1') ? 'has-error' : '' }}">
						{!! Form::label('p_p1', 'Product: *') !!}
						{!! Form::text('p_p1',null,['placeholder' => '', 'id' => 'p_p1', 'class' => 'input']) !!}
						{!! $errors->first('p_p1', '<small class="error">:message</small>') !!}
					</div>
					<div class="formRow {{ $errors->has('p_p2') ? 'has-error' : '' }}">
						{!! Form::label('p_p2', 'Comment or Question: *') !!}
						{!! Form::textarea('p_p2',null,['placeholder' => '', 'id' => 'p_p2', 'rows' => '4', 'class' => 'input']) !!}
						{!! $errors->first('p_p2', '<small class="error">:message</small>') !!}
					</div>
				</div>

				<!-- company enquiry -->
				<div class="conditionals cond_reason cond_company @if (old('reason', '') !== 'company') hide @endif">
					<div class="formRow">
						{!! Form::label('c_p1', 'Department to Contact:') !!}
						{!! Form::text('c_p1',null,['placeholder' => '', 'id' => 'c_p1', 'class' => 'input']) !!}
						{!! $errors->first('c_p1', '<small class="error">:message</small>') !!}
					</div>
					<div class="formRow  {{ $errors->has('c_p2') ? 'has-error' : '' }}">
						{!! Form::label('c_p2', 'Comment or Question: *') !!}
						{!! Form::textarea('c_p2',null,['placeholder' => '', 'id' => 'c_p2', 'rows' => '4', 'class' => 'input']) !!}
						{!! $errors->first('c_p2', '<small class="error">:message</small>') !!}
					</div>
				</div>

				<!-- product complaint -->
				<div class="conditionals cond_reason cond_complaint @if (old('reason', '') !== 'complaint') hide @endif">

					<div class="formRow">
						{!! Form::label('copx', 'Product:') !!}
						{!! Form::select('copx', $productArr, null, ['id' => 'copx', 'class' => 'input'] ) !!}
						{!! $errors->first('copx', '<small class="error">:message</small>') !!}
					</div>

					<div class="formRow">
						{!! Form::label('co_p1', 'Product Bar Code:') !!}
						{!! Form::text('co_p1',null,['placeholder' => '', 'id' => 'co_p1', 'class' => 'input']) !!}
						<p class="infonote">Numbers shown under the barcode.</p>
						{!! $errors->first('co_p1', '<small class="error">:message</small>') !!}
					</div>
					<div class="formRow">
						{!! Form::label('co_p2', 'Product Lot / Batch Code:') !!}
						{!! Form::text('co_p2',null,['placeholder' => '', 'id' => 'co_p2', 'class' => 'input']) !!}
						<p class="infonote">Lot Codes are printed at the bottom or sides of the product packages and vary in length from 5-15 digits.</p>
						{!! $errors->first('co_p2', '<small class="error">:message</small>') !!}
					</div>
					<div class="formRow">
						{!! Form::label('co_p3', 'Expiry Date (if applicable):') !!}
						{!! Form::text('co_p3',null,['placeholder' => '', 'id' => 'co_p3', 'class' => 'input']) !!}
						{!! $errors->first('co_p3', '<small class="error">:message</small>') !!}
					</div>
					<div class="formRow {{ $errors->has('co_p4') ? 'has-error' : '' }}">
						{!! Form::label('co_p4', 'Complaint: *') !!}
						{!! Form::textarea('co_p4',null,['placeholder' => '', 'id' => 'c_p4', 'rows' => '4', 'class' => 'input']) !!}
						{!! $errors->first('co_p4', '<small class="error">:message</small>') !!}
					</div>
				</div>

				<div class="formRow checkbox {{ $errors->has('privacy') ? 'has-error' : '' }}">
					<input type="checkbox" id="privacy" name="privacy" value="accepted" @if ( old('privacy', '') === 'accepted' ) checked="checked" @endif>
					<label for="privacy">I have read and agreed to femfresh's <a href="/privacy-policy" target="_blank">Privacy Policy</a></label>
					{!! $errors->first('privacy', '<small class="error">:message</small>') !!}
				</div>

				@if ( old('reason', '') !== '') 
					<div class="formRow tar">
						{!! Form::submit('Submit', array('class' => 'btn', 'id' => 'bSubmit', 'onClick' => "ga('send', 'event', { eventCategory: 'Contact', eventAction: 'Submit', eventLabel: 'Contact Form'});")) !!}
					</div>
				@else
					<div class="formRow tar">
						{!! Form::submit('Submit', array('class' => 'btn', 'id' => 'bSubmit', 'disabled' => 'disabled', 'onClick' => "ga('send', 'event', { eventCategory: 'Contact', eventAction: 'Submit', eventLabel: 'Contact Form'});")) !!}
					</div>
				@endif

				{!! Form::close() !!}

				@endif
			</div>
		</div>
	</div>
@endsection

@section('components')

@endsection
