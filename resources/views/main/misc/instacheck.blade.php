@extends('main.layouts.main')

@section('header')

@endsection

@section('content')
<style type="text/css">
	.posts{
		list-style-type:none;
		padding:0;
		display:flex;
		flex-wrap:wrap;
	}
	.posts li{
		flex-basis:33.3%;
		box-sizing:border-box;
		padding:4px;
		position:relative;
		
	}
	.posts li a{
		position:absolute;
		left:0px;
		top:0px;
		width:100%;
		height:100%;
		z-index:100;
	}
	.posts li a span{
		overflow:hidden;
		text-indent:-99999em;
		display:block;
	}
	.posts li[data-type="IMAGE"] figure,
	.posts li[data-type="VIDEO"] figure{
		overflow:hidden;
		width:100%;
		padding-top:100%;
		position:relative;
	}
	.posts li[data-type="IMAGE"] figure > img,
	.posts li[data-type="VIDEO"] figure > img{
		width:100%;
		height:auto;
		position:absolute;
		left:0px;
		top:0px;
		transition: all 0.1s ease-in-out;
	}
	.posts li[data-type="IMAGE"]:hover img{
		transform:scale(1.2);
	}
	.posts li[data-type="VIDEO"]:hover img{
		transform:scale(1.2);
	}

	.posts li > a{
		background-repeat:no-repeat;
		background-position: 95% 5%;
		background-size:32px 32px;
		background-color:rgba(255,255,255,0);
	}
	.posts li[data-type="VIDEO"] > a{
		background-image:url('/images/icons/video.svg');
	}
	.posts li[data-type="CAROUSEL_ALBUM"] > a{
		background-image:url('/images/icons/carousel.svg');
	}
	.posts li[data-type="CAROUSEL_ALBUM"]{
		overflow:hidden;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"] > figure{
		opacity:1;
		transition: all 0.1s ease-in-out;
		overflow:hidden;
		width:calc(100% - 8px);
		padding-top:100%;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"] > figure > img{
		width:calc(100% - 8px);
		height:auto;
		position:absolute;
		left:4px;
		top:4px;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"]:hover > figure{
		opacity:0;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"] ul.frames{
		position:absolute;
		left:4px;
		top:4px;
		width:calc(100% - 8px);
		list-style-type:none;		
		padding-left:0;
		margin:0;
		display:flex;
		flex-wrap: wrap;
		opacity:0;
		transition: all 0.1s ease-in-out;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"]:hover ul.frames{
		opacity:1;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"] ul.frames li{
		flex-basis:20%;
		width:20%;
		padding:0;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"] ul.frames li img{
		width:100%;
	}
	.container[data-ref=policy] .inner-fix .panel ul.posts li[data-type="CAROUSEL_ALBUM"] ul.frames li.active{
	}
</style>

	

	<div class="container first" data-ref="policy">
		<div class="inner-fix tal">
			<div class="panel">
				<h1>instacheck</h1>

				{{-- <pre>{{ print_r($data,true) }}</pre> --}}

				@if ( count($data) > 0)

				<p>Posts: {{ count($data) }}</p>
				<ul class="posts">
				@foreach ($data as $item)

				<li data-id="{{ $item['id'] }}" data-type="{{ $item['type'] }}" data-timestamp="">
					@if ( $item['type'] === 'IMAGE')
					<figure><img alt="{!! $item['caption'] !!}" src="{{ $item['image'] }}"></figure>
					<a href="{{ $item['permalink'] }}" target="_blank" title="{!! $item['caption'] !!}"><span>view</span></a>
					@endif

					@if ( $item['type'] === 'CAROUSEL_ALBUM')
					<figure><img alt="{!! $item['caption'] !!}" src="{{ $item['image'] }}"></figure>
					<ul class="frames">
						<?php 
							$chidlren = $item['frames']
						?>
						@foreach ($chidlren as $childindex => $child)
							<li @if ($childindex === 0) class="active" @endif><img id="{{ $child['id'] }}" alt="{{ $child['id'] }}" src="{{ $child['image'] }}"></li>
						@endforeach
					</ul>
					<a href="{{ $item['permalink'] }}" target="_blank" title="{!! $item['caption'] !!}"><span>view</span></a>
					@endif

					@if ( $item['type'] === 'VIDEO')
					<figure><img alt="{!! $item['caption'] !!}" src="{{ $item['image'] }}"></figure>
					<a href="{{ $item['permalink'] }}" target="_blank" title="{!! $item['caption'] !!}"><span>view</span></a>
					@endif
				</li>
				@endforeach
				</ul>

				@else

				<p>no posts found</p>

				@endif


			</div>
		</div>
	</div>
@endsection

@section('components')
	
@endsection