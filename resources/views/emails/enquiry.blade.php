FemFresh Contact Us / enquiry form

F1: UKWEB

Imp_title: {{ $title }}

Imp_first_name: {{ $firstname }}

Imp_last_name: {{ $lastname }}

Imp_email: {{ $email }}

Imp_company: {{ $company }}

Imp_address: {{ $address }}

Imp_address2: {{ $address2 }}

Imp_city: {{ $city }}

Imp_postcode: {{ $postcode }}

Imp_country: {{ $country }}

Imp_phone: {{ $phone }}

@if ( $reason === 'product')
General Product Enquiry

Imp_product: {{ $product }}

Imp_comment/question: {{ $comments }}

@elseif  ( $reason === 'company')
General Company Enquiry

Imp_department: {{ $department }}

Imp_comment/question: {{ $comments }}

@elseif  ( $reason === 'complaint')
Product Complaint

Imp_product: {{ $product }} 

Imp_barcode: {{ $barcode }}

Imp_batchcode: {{ $batchcode }}

Imp_expirydate: {{ $expirydate }}

Imp_complaint: {{ $comments }}
@endif

