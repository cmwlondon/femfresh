<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
    <title style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">femfresh question for Dr Sara</title>
    <!-- page template from http://assets.wildbit.com/postmark/templates/dist/welcome.html -->

    <!-- 
    The style block is collapsed on page load to save you some scrolling.
    Postmark automatically inlines all CSS properties for maximum email client 
    compatibility. You can just update styles here, and Postmark does the rest.
    -->
    <style type="text/css" rel="stylesheet" media="all" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
    /* Base ------------------------------ */
    
    *:not(br):not(tr):not(html) {
      font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
      box-sizing: border-box;
    }
    
    body {
      width: 100% !important;
      height: 100%;
      margin: 0;
      line-height: 1.4;
      background-color: #F2F4F6;
      color: #74787E;
      -webkit-text-size-adjust: none;
    }
    
    p,
    ul,
    ol,
    blockquote {
      line-height: 1.4;
      text-align: left;
    }
    
    a {
      color: #3869D4;
    }
    
    a img {
      border: none;
    }
    /* Layout ------------------------------ */
    
    .email-wrapper {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #e7f2d5;
    }
    
    .email-content {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    /* Masthead ----------------------- */
    
    .email-masthead {
      padding: 25px 0;
      text-align: center;
    }
    
    .email-masthead_logo {
      width: 94px;
    }
    
    .email-masthead_name {
      font-size: 48px;
      font-weight: bold;
      color: #4a7c4b;
      text-decoration: none;
    }
    /* Body ------------------------------ */
    
    .email-body {
      width: 100%;
      margin: 0;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      border-top: 1px solid #EDEFF2;
      border-bottom: 1px solid #EDEFF2;
      background-color: #FFFFFF;
    }
    
    .email-body_inner {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #FFFFFF;
    }
    
    .email-footer {
      width: 570px;
      margin: 0 auto;
      padding: 0;
      -premailer-width: 570px;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .email-footer p {
      color: #4a7c4b;
    }
    
    .body-action {
      width: 100%;
      margin: 30px auto;
      padding: 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      text-align: center;
    }
    
    .body-sub {
      margin-top: 25px;
      padding-top: 25px;
      border-top: 1px solid #EDEFF2;
    }
    
    .content-cell {
      padding: 35px;
    }
    
    .preheader {
      display: none !important;
    }
    /* Attribute list ------------------------------ */
    
    .attributes {
      margin: 0 0 21px;
    }
    
    .attributes_content {
      background-color: #EDEFF2;
      padding: 16px;
    }
    
    .attributes_item {
      padding: 0;
    }
    /* Related Items ------------------------------ */
    
    .related {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .related_item {
      padding: 10px 0;
      color: #74787E;
      font-size: 15px;
      line-height: 18px;
    }
    
    .related_item-title {
      display: block;
      margin: .5em 0 0;
    }
    
    .related_item-thumb {
      display: block;
      padding-bottom: 10px;
    }
    
    .related_heading {
      border-top: 1px solid #EDEFF2;
      text-align: center;
      padding: 25px 0 10px;
    }
    /* Discount Code ------------------------------ */
    
    .discount {
      width: 100%;
      margin: 0;
      padding: 24px;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
      background-color: #EDEFF2;
      border: 2px dashed #9BA2AB;
    }
    
    .discount_heading {
      text-align: center;
    }
    
    .discount_body {
      text-align: center;
      font-size: 15px;
    }
    /* Social Icons ------------------------------ */
    
    .social {
      width: auto;
    }
    
    .social td {
      padding: 0;
      width: auto;
    }
    
    .social_icon {
      height: 20px;
      margin: 0 8px 10px 8px;
      padding: 0;
    }
    /* Data table ------------------------------ */
    
    .purchase {
      width: 100%;
      margin: 0;
      padding: 35px 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_content {
      width: 100%;
      margin: 0;
      padding: 25px 0 0 0;
      -premailer-width: 100%;
      -premailer-cellpadding: 0;
      -premailer-cellspacing: 0;
    }
    
    .purchase_item {
      padding: 10px 0;
      color: #74787E;
      font-size: 15px;
      line-height: 18px;
    }
    
    .purchase_heading {
      padding-bottom: 8px;
      border-bottom: 1px solid #EDEFF2;
    }
    
    .purchase_heading p {
      margin: 0;
      color: #9BA2AB;
      font-size: 12px;
    }
    
    .purchase_footer {
      padding-top: 15px;
      border-top: 1px solid #EDEFF2;
    }
    
    .purchase_total {
      margin: 0;
      text-align: right;
      font-weight: bold;
      color: #2F3133;
    }
    
    .purchase_total--label {
      padding: 0 15px 0 0;
    }
    /* Utilities ------------------------------ */
    
    .align-right {
      text-align: right;
    }
    
    .align-left {
      text-align: left;
    }
    
    .align-center {
      text-align: center;
    }
    /*Media Queries ------------------------------ */
    
    @media only screen and (max-width: 600px) {
      .email-body_inner,
      .email-footer {
        width: 100% !important;
      }
    }
    
    @media only screen and (max-width: 500px) {
      .button {
        width: 100% !important;
      }
    }
    /* Buttons ------------------------------ */
    
    .button {
      background-color: #3869D4;
      border-top: 10px solid #3869D4;
      border-right: 18px solid #3869D4;
      border-bottom: 10px solid #3869D4;
      border-left: 18px solid #3869D4;
      display: inline-block;
      color: #FFF;
      text-decoration: none;
      border-radius: 3px;
      box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16);
      -webkit-text-size-adjust: none;
    }
    
    .button--green {
      background-color: #22BC66;
      border-top: 10px solid #22BC66;
      border-right: 18px solid #22BC66;
      border-bottom: 10px solid #22BC66;
      border-left: 18px solid #22BC66;
    }
    
    .button--red {
      background-color: #FF6136;
      border-top: 10px solid #FF6136;
      border-right: 18px solid #FF6136;
      border-bottom: 10px solid #FF6136;
      border-left: 18px solid #FF6136;
    }
    /* Type ------------------------------ */
    
    h1 {
      margin-top: 0;
      color: #2F3133;
      font-size: 19px;
      font-weight: bold;
      text-align: left;
    }
    
    h2 {
      margin-top: 0;
      color: #2F3133;
      font-size: 16px;
      font-weight: bold;
      text-align: left;
    }
    
    h3 {
      margin-top: 0;
      color: #2F3133;
      font-size: 14px;
      font-weight: bold;
      text-align: left;
    }
    
    p {
      margin-top: 0;
      color: #74787E;
      font-size: 16px;
      line-height: 1.5em;
      text-align: left;
    }
    
    p.sub {
      font-size: 12px;
    }
    
    p.center {
      text-align: center;
    }
    </style>
  </head>
  <body style="height: 100%;margin: 0;line-height: 1.4;background-color: #F2F4F6;color: #74787E;-webkit-text-size-adjust: none;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;width: 100% !important;">
    <span class="preheader" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;display: none !important;">The following question was submitted for Dr Frankie</span>
    <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" style="width: 100%;margin: 0;padding: 0;-premailer-width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;background-color: #e7f2d5;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
      <tr>
        <td align="center" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
          <table class="email-content" width="100%" cellpadding="0" cellspacing="0" style="width: 100%;margin: 0;padding: 0;-premailer-width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
            <tr>
              <td class="email-masthead" style="padding: 25px 0;text-align: center;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                <a href="https://femfresh.co.uk" class="email-masthead_name" style="color: #4a7c4b;font-size: 48px;font-weight: bold;text-decoration: none;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">femfresh</a>
              </td>
            </tr>
            <!-- Email Body -->
            <tr>
              <td class="email-body" width="100%" cellpadding="0" cellspacing="0" style="width: 100%;margin: 0;padding: 0;-premailer-width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;border-top: 1px solid #EDEFF2;border-bottom: 1px solid #EDEFF2;background-color: #FFFFFF;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                <table class="email-body_inner" align="center" width="570" height="570" cellpadding="0" cellspacing="0" style="width: 570px;margin: 0 auto;padding: 0;-premailer-width: 570px;-premailer-cellpadding: 0;-premailer-cellspacing: 0;background-color: #FFFFFF;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                  <!-- Body content -->
                  <tr>
                    <td class="content-cell" style="padding: 35px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                      <h1 style="margin-top: 0;color: #2F3133;font-size: 19px;font-weight: bold;text-align: left;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">The following question was submitted for Dr Frankie</h1>
                      <p style="line-height: 1.5em;text-align: left;margin-top: 0;color: #74787E;font-size: 16px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">{{ $question }}</p>
                      <h2 style="margin-top: 0;color: #2F3133;font-size: 19px;font-weight: bold;text-align: left;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">By</h2>
                      <p style="line-height: 1.5em;text-align: left;margin-top: 0;color: #74787E;font-size: 16px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">{{ $name }}</p>
                      <p style="line-height: 1.5em;text-align: left;margin-top: 0;color: #74787E;font-size: 16px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">{{ $email }}</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" style="width: 570px;margin: 0 auto;padding: 0;-premailer-width: 570px;-premailer-cellpadding: 0;-premailer-cellspacing: 0;text-align: center;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                  <tr>
                    <td class="content-cell" align="center" style="padding: 35px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                      <p class="sub align-center" style="line-height: 1.5em;text-align: center;margin-top: 0;color: #4a7c4b;font-size: 12px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">&copy; femfresh 2020</p>
                      <p class="sub align-center" style="line-height: 1.5em;text-align: center;margin-top: 0;color: #4a7c4b;font-size: 12px;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;box-sizing: border-box;">
                        Church &amp; Dwight UK Ltd
                      </p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>