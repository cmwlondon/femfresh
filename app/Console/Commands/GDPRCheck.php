<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Support;
use Carbon\Carbon;

class GDPRCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gdpr:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks supports table for entries over 3 months old and obscures the data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Support $support)
    {
        $data = $support->where('firstname','<>','GDPR')->whereDate('created_at', '<=', Carbon::today()->subMonths(3)->toDateString() )->get();
        foreach($data as $key => $entry) {
            $entry->title = NULL;
            $entry->firstname = 'GDPR';
            $entry->lastname = NULL;
            $entry->company = NULL;
            $entry->address = NULL;
            $entry->city = NULL;
            $entry->postcode = NULL;
            $entry->country = NULL;
            $entry->phone = NULL;
            $entry->email = $entry->created_at->timestamp . '@gdpr.com';
            $entry->reason = NULL;
            $entry->product = NULL;
            $entry->comments = NULL;
            $entry->department= NULL;
            $entry->barcode = NULL;
            $entry->batchcode = NULL;
            $entry->expirydate = NULL;
            $entry->privacy = NULL;

            $entry->save();
            $this->line($key);
        }
    }
}
