<?php namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class QuestionRequest extends FormRequest {

	public function __construct() {

	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		
		$rules = [
			'name'			=> ['nullable','max:255','regex:/^([a-zA-Z\s])+$/i'],
			'email'			=> ['required','email'],
			'question'		=> ['required','max:1000',"regex:/^([a-zA-Z0-9\s\.\,\;\:\?\!\'\"\(\)\-\_\&\%\/\\\])+$/i"],
			'g-recaptcha-response' => 'required|recaptcha',

			// 'terms'			=> 'required'
		];

		return $rules;
	}

	public function messages()
	{
	    return [
	    	'name.regex' => 'Please use only upper/lower case letters and spaces in the name field',
	    	'email.required' => 'Please supply a valid email address so we can contact you',
	    	'email.email' => 'Please supply a valid email address so we can contact you',
	    	'question.required' => 'Please add a question to ask Dr Frankie',
	    	'question.regex' => 'Please use only upper/lower case letters, spaces and punctuation marks in the question field',
	    	'g-recaptcha-response.required' => 'Please complete the captcha',

	    	// 'terms.required' => 'Please indicate your acceptance of the femfresh Terms &amp; Conditions'
	    ];
	}

	public function withValidator($validator)
	{
	}
}
