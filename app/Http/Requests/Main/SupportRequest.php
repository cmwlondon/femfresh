<?php namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class SupportRequest extends FormRequest {

	public function __construct() {

	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		
		// non conditional validation
		$rules = [
			'title'				=> 'required|max:255',
			'firstname'			=> 'required|max:255',
			'lastname'      	=> 'required|max:255',
			'address'      		=> 'required|max:255',
			'address2'      	=> 'max:255',
			'city'      		=> 'required|max:255',
			'postcode'			=> 'required|max:20',
			'country'			=> 'required|max:255',
			'phone'				=> 'required|max:20',
			'email'				=> 'required|email',
			'reason'			=> 'required',
			'privacy'			=> 'required'
		];

		// conditional validation
		// country 'other' free text field
		if ( Request::input('country') === 'other' ) {
			$rules['country_other'] = 'required';
		}

		// check values of contact-reason select field
		// 'reason' : '','product','company','complaint'
	    switch( Request::input('reason') ) {
	    	case "product" : {
	    		// general product enquiry

	    		// product *
	    		$rules['p_p1'] = 'required';
	    		// comment / question *
	    		$rules['p_p2'] = 'required';

	    	} break;
	    	case "company" : {
	    		// general company enquiry

	    		// department
	    		// comment / question *
	    		$rules['c_p2'] = 'required';

	    	} break;
	    	case "complaint" : {
	    		// product complaint

	    		// product select:['Daily intimate range','ultimate pure wash']
	    		// $rules['copx'] = 'required';
	    		// product bar code
	    		// product lot / batch code
	    		// expiry date
	    		// complaint details *
	    		$rules['co_p4'] = 'required';

	    	} break;
	    }

		return $rules;
	}

	public function messages()
	{
	    return [
	    	'privacy.required' => 'Please indicate that you have read and accepted the privacy policy.',
		    'p_p1.required' => 'The Product name is required.',
		    'p_p2.required' => 'The Comment / question is required.',
		    'c_p2.required' => 'The Comment / question is required.',
		    'co_p4.required' => 'The complaint details are required.',
		    'country_other.required' => 'Please supply the name of your country'
	    ];
	}

}
