<?php namespace App\Http\Controllers\Main;

use App;
use Session;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;


class MainController extends Controller {

	public function __construct() {
		$this->uri = Request::path();
		// Context now pulled from config so we can use it in error pages.
		$this->context = config('context');
		$this->context['site_url'] = url('',[]);
		$this->context['jsVars']['site_url'] = url('',[]);

		$this->context['meta']['title'] = '';
		$this->context['meta']['desc'] = '';
		$this->context['meta']['pagetype'] = '';
		$this->context['meta']['keywords'] = '';
		$this->context['meta']['keyphrases'] = '';
		$this->context['meta']['indexable'] = '';

		$this->context['meta']['link'] = url($this->uri);
		$this->context['meta']['image'] = url('');
		$this->context['meta']['fbid'] = env('FACEBOOK_APP_ID', '');

		$this->context['image_path'] = 'images/';
		$this->context['$pageViewJS'] = '';
		$this->context['pageViewCSS'] = ['/css/main/styles.css'];//,'/css/main/print.css'
		$this->context['section'] = Request::segment(1);
		$this->context['articles'] = config('articles');
		$this->context['blogitems'] = config('blogitems');
		$this->context['version'] = time();

		if (Session('response') !== NULL) {
			$this->context['response'] = Session('response');
		}

		// Check for meta in the config
		$meta = config('meta');

		if (array_key_exists($this->uri,$meta)) {
			foreach($this->context['meta'] as $key => $value) {
				if (array_key_exists($key,$meta[$this->uri])) {
					if ($key == "title" && $meta[$this->uri][$key] == "") {
						// Ignore blank titles;
					} elseif ($key == "image" && $meta[$this->uri][$key] != "") {
						$this->context['meta']['image'] = url('images/sharing/'.$meta[$this->uri][$key]);
					} else {
						$this->context['meta'][$key] = $meta[$this->uri][$key];
					}
				}
			}
		}

		// echo "<p>".$this->uri."</p>";
		// echo "<pre>".print_r($this->context['meta'],true)."</pre>";
		// die();
	}

	public function checkCache($preview) {
		// Caching currently makes the cookie notice filck on and off, perhaps there's a workaround.

		// if (App::environment('production')) {
		// 	if ($preview != true) {
		// 		$exclusions = ["contact"];
		// 		if (!in_array($this->uri, $exclusions)) {
		// 			if (Cache::has($this->uri)) {
		// 				return Cache::get($this->uri);
		// 			}
		// 		}
		// 	}
		// }
		return false;
	}

	public function saveCache($html) {
		if (App::environment('production','staging','testing')) {
			Cache::add($this->uri, $html, 60);
		}
	}

	public function forgetCache() {
		if (Cache::has($this->uri)) {
			Cache::forget($this->uri);
		}
	}

	public function clearCache() {
		Cache::flush();
	}

	
}
