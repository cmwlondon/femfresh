<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Illuminate\Http\Request;
use Cookie;

use App\Http\Requests\Main\SupportRequest;
use App\Models\Support;
use App\Http\Requests\Main\QuestionRequest;
use App\Models\Question;

use App\Models\QuizEntries;
use App\Models\Page;
// use Illuminate\Support\Facades\Mail;
use App\Mail\SysMail;
use App\Mail\Enquiry;
use App;
use DB;
use Mail;
use App\Helpers\Contracts\InstagramContract;

/*
Guzzle HTTP client
https://docs.guzzlephp.org/en/stable/quickstart.html
https://docs.guzzlephp.org/en/stable/quickstart.html#exceptions
*/
use GuzzleHttp\Client;
// use GuzzleHttp\Psr7; // no support for PSR-7 in Laravel
// use GuzzleHttp\Exception\RequestException;

/*
page metadata (title/desc/keywords) in  //config/meta.php
*/

class PageController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(InstagramContract $instagram)
	{
		parent::__construct();
		$this->context['cookie'] = isset($_COOKIE['FemfreshDisclaimerCookie']);

		$this->context['instagram'] = $instagram->getReady();
		// $this->context['instagram'] = array();
		$this->context['jsVars']['instagram_bank'] = count($this->context['instagram']);
		$this->context['pageViewJS']	= '';

		// 'Contact Us' form select options
		// /config/contact_us.php
		$this->context['titlesArr'] = config('contact_us.titlesArr');		
		$this->context['reasonsArr'] = config('contact_us.reasonsArr');		
		$this->context['productArr'] = config('contact_us.productArr');
		$this->context['countryList'] = config('contact_us.countryList');
	}


	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{	
		$this->context['pageViewJS']	= 'pages/home.min';
		return $this->show('home', $request);
	}

	public function range(Request $request)
	{
		$url = $request->segment(2);
		$url = ($url == "") ? "index" : $url;
		$url = 'range.'.$url;
		$this->context['pageViewJS']	= 'pages/range.min';
		return $this->show($url, $request);
	}

	public function about(Request $request)
	{
		$url = $request->segment(2);
		$url = ($url == "") ? "index" : $url;
		$url = 'about.'.$url;
		return $this->show($url, $request);
	}

	public function knowledge(Request $request)
	{
		$url = $request->segment(2);
		$url = ($url == "") ? "index" : $url;

		if ($url == 'gynae-advice') {
			$this->context['pageViewJS']	= 'pages/dr-sara.min';
		}

		if ($url == 'about-ph-balance') {
			$this->context['pageViewJS']	= 'pages/ph-balance.min';
		}

		if ($url == 'interactive-quiz') {

			$this->context['avgScore'] = round(QuizEntries::avg('score'));

			$this->context['pageViewJS']	= 'pages/quiz.min';
		}

		if ($url == 'faqs') {
			$this->context['pageViewJS']	= 'pages/faqs.min';
		}

		if ($url == 'she-talks') {
			$toplay = $request->play;
			$this->context['jsVars']['toplay'] = ($toplay == null) ? "1" : $toplay;

			$this->context['pageViewJS']	= 'pages/videos.min';
		}

		$url = 'knowledge.'.$url;

		return $this->show($url, $request);
	}

	public function faqs(Request $request)
	{
		$this->context['pageViewJS']	= 'pages/faqs.min';
		return $this->show('faqs.index', $request);
	}

	public function expertadvice(Request $request)
	{
		$this->context['pageViewJS']	= 'pages/dr-sara.min';
		return $this->show('expertadvice.index', $request);
	}

	public function expertadvice_submit(QuestionRequest $request, Question $question)
	{

		// Prepare the email to send.
		$details = array(
			'name'			=> ($request->name !== NULL) ? $request->name: 'Name not supplied',
			'email'			=> $request->email,
			'question'		=> $request->question,
			// 'terms'			=> $request->terms,
			'terms'			=> 'accepted',
			'brand'			=> 'femfresh'
		);

		$dbInsert = $details;
		$dbInsert['question'] = addslashes($dbInsert['question']);

		// save form data into db 'femfresh_2018.drsaraquestions'
		$question->fill( $dbInsert );
		$question->save();

		// Mail::to(Config('mail.mail-sys-to'))->send(new SysMail($details));
		Mail::send(['html' => 'emails.drsara'], $details, function($message) use($details)
		{
			$message->from('no-reply@femfresh.co.uk', 'femfresh');
			// $message->to( 'hockey.richard@gmail.com' );
			$message->to( env('MAIL_QUESTION') ); // address in list of authorised test addresses for sandbox
			// $message->bcc( env('MAIL_ADMIN_BCC') );
		    $message->subject('A question for Dr Frankie');
		});

		$this->response = array('response_status' => 'success', 'message' => 'Thank you for submitting your question. Dr Frankie will respond to your question within the next 7 working days.');
		return redirect()->route('drfrankie')->with('response', $this->response);
		// return redirect('/knowledge-hub/gynae-advice/#question-anchor')->with('response', $this->response);
	}
	
	public function article(Request $request)
	{
		$url = $request->segment(3);
		if (!array_key_exists($url,$this->context['articles'])) {
			abort(404);
		}
		$this->context['articleSlug'] = $url;
		$url = 'knowledge.articles.'.$url;

		return $this->show($url, $request);
	}

	public function blog(Request $request)
	{
		// read blog itme key from URL /knowledge-hub/ph-balance 'ph-balance'
		$url = $request->segment(2);
		if (!array_key_exists($url, $this->context['blogitems'])) {
			abort(404);
		}

		// set metadata
		// update /config/blogitem.php to set metadata description/keywords/keyphrases for all blog posts
		/*
		$this->context['meta']['title'] = $this->context['blogitems'][$url]['title'];
		$this->context['meta']['desc'] = 'expert advice blog post: '. $this->context['blogitems'][$url]['title'];
		$this->context['meta']['keywords'] = 'expert advice blog post: '.$this->context['blogitems'][$url]['title'];
		$this->context['meta']['keyphrases'] = 'expert advice blog post: '.$this->context['blogitems'][$url]['title'];
		*/
		$blogitemMeta = $this->context['blogitems'][$url]['meta'];
		$this->context['meta']['title'] =  $blogitemMeta['title'];
		$this->context['meta']['desc'] = "Dr Frankie's Expert Advice: ".$blogitemMeta['desc'];
		$this->context['meta']['keywords'] = "dr,frankie,expert,advice,".$blogitemMeta['keywords'];
		$this->context['meta']['keyphrases'] = "Dr Frankie's Expert Advice,".$blogitemMeta['keyphrases'];


		$this->context['blogSlug'] = $url;
		$url = 'knowledge.blog.'.$url;
		return $this->show($url, $request);
	}

	public function misc(Request $request)
	{
		$url = $request->segment(1);
		$url = ($url == "") ? "index" : $url;
		$url = 'misc.'.$url;
		return $this->show($url, $request);
	}

	public function mailtest(Request $request)
	{
		$this->context['time'] = date("Y-m-d H:i:s");

		$details = array(
			'title'			=> 'Mr',
			'firstname'		=> 'Albert',
			'lastname'		=> 'Einstein',
			'phone'			=> '7542087301',
			'email'			=> 'albert.einstain@gmail.com',
			'product'		=> 'replens mailgun test product',
			'comments'		=> 'replens meilgun test comment',
			'mfgcode'		=> 'femfresh',
			'timestamp' 	=> date("Y-m-d H:i:s"),
			'alpha' => '',
			'beta' => '',
			'deltaa' => '',
			'gamma' => '',
			'omega' => '',
			'epsilon' => '',
			'phi' => ''
		);

		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
			$message->from('no-reply@femfresh.co.uk', 'femfresh');
			$message->to( env('MAIL_ADMIN_TO') ); // address in list of authorised test addresses for sandbox
		    $message->subject('UK ChurchDwight Enquiry');
		});
		
		// Mail::to(Config('mail.mail-sys-to'))->send(new SysMail($details));

		return $this->show('misc.mailtest', $request);
	}

	public function instacheck(Request $request, InstagramContract $instagram, Client $guzzleHttpClient)
	{

		// user ID 17841413990341256 Richard Hockey
		$userID = '17841413990341256';
		// 10:21am 25th May 2021 can be renewed after 10:21am 26th May 2021
		// 60-day access token:
		// IGQVJXbURuRkdyZAnRCWi1CdjFuVE54SXdQVjkxN0NSTkhXZADJielJwOXB3ZAnJXY1NHTlBDX0U5N1JaTWVyXzhqRzNMQndlQzRxS0pINGw2OW00NzRRMUMyMERMNzhSNU5UdlJ1RTBR
		$accessToken = 'IGQVJXbURuRkdyZAnRCWi1CdjFuVE54SXdQVjkxN0NSTkhXZADJielJwOXB3ZAnJXY1NHTlBDX0U5N1JaTWVyXzhqRzNMQndlQzRxS0pINGw2OW00NzRRMUMyMERMNzhSNU5UdlJ1RTBR';

		/* 
		----------------------------------------------
		data structure
		----------------------------------------------

		post
			ID
			caption
			permalink
			timestamp
			type 'IMAGE' / 'VIDEO' / 'CAROUSEL_ALBUM'
			media_id
			state 'pending' / 'ready'

		media
			id
			parentid
			haschildren
			type 'IMAGE' / 'VIDEO' / 'CAROUSEL_ALBUM'
			image

			media tabke stores basic media types and carousel album
			IMAGE
				id N
				parentid 0
				haschildren 0
				type 'IMAGE'
				image -> media_url

			ViDEO
				id N
				parentid 0
				haschildren 0
				type 'VIDEO'
				image -> thumbnail_url

			CAROUSEL_ALBUM
			PARENT
				id N (pid)
				parentid 0
				haschildren 1
				type 'IMAGE'
				image -> media_url

			CAROUSEL_ALBUM
			CHILD IMAGE
				id N
				parentid (pid)
				haschildren 0
				type 'IMAGE'
				image -> media_url

			CAROUSEL_ALBUM
			CHILD VIDEO
				id N
				parentid (pid)
				haschildren 0
				type 'IMAGE'
				image -> thumbnail_url

		----------------------------------------------
		process:
		----------------------------------------------

		CRON job runs daily[
			get access token date from db
					if access token age is over 58 days ago
						use existing access token to request a new 60-day token
							store new access token in db
							update access token date

			use acesss token to get latest posts from account [USER ID]
			iterate over latest posts, startting from oldest, checking media id against entries in the db 'posts'
				if the media id is not in the db
					insert into db 'posts' marked 'pending'
		]

		CRON job runs hourly
		[
			find first item in db 'posts' marked pending
				get [MEDIA ID] from post
					get post information
						has children?
						 	iterate over children using [MEDIA ID]s
								post type:
									VIDEO
										use thumbnail_url
									IMAGE
										use media_url
								process, storing results locally	 		
					post type:
						VIDEO
							use thumbnail_url
						IMAGE
							use media_url
						CAROUSEL_ALBUM
							use media_url
				 	process, storing results locally

					change state of post from 'pending' to 'done'
		]

		----------------------------------------------
		*/

		/*
		https://graph.instagram.com/[user id]/media
		?fields=id,media_type,permalink,children
		&access_token=[access token]
		*/

		/*
		----------------------------------------------------------------------------
		*/

		// set up guzzle Http Client
		$client = new $guzzleHttpClient([
		    // Base URI is used with relative requests
		    'base_uri' => 'https://graph.instagram.com/',
		    // You can set any number of default request options.
		    'timeout'  => 2.0
		]);

		/*
		----------------------------------------------------------------------------
		*/

		/*
		----------------------------------------------------------------------------

		// perform 'GET' request
		// not using standard guzzle PSR-7/exception since laravel 5.x doesn't support PSR-7
		try {
			$response = $client->request('GET', $userID.'/media', [
				'query' => [
			    	'fields' => 'id,media_type,permalink,children,caption',
			    	'access_token' => $accessToken
			    ]
			]);
		} catch (Exception $e) {
			// request error
			// return empty set to test page
	    	$this->context['data'] = [];
	    	return $this->show('misc.instacheck', $request);
		}

		// deal wih response
		$code = $response->getStatusCode();
		$body = $response->getBody();
		$stringBody = (string) $body;
		echo "<p>HTTP response code: $code</p>";

		$mediaItems = json_decode($stringBody);
		// echo "<pre>".print_r($mediaItems->data, true)."</pre>";

		$mediaItemIDs = [];
		foreach( $mediaItems->data AS $mediaItem) {
			$mediaItemIDs[] = [
				'id' => $mediaItem->id
			];
		}

		// process data for display in test page

		// add handling for differnt types: IMAGE, VIDEO, CAROUSEL_ALBUM 
		foreach($mediaItemIDs AS $index => $item){
			try {
				$response = $client->request('GET', $item['id'], [
					'query' => [
				    	'fields' => 'id,media_type,media_url,permalink,children,caption',
				    	'access_token' => $accessToken
				    ]
				]);
			} catch (Exception $e) {
				// request error
				// return empty set to test page
		    	$this->context['data'] = [];
		    	return $this->show('misc.instacheck', $request);
			}
	
			$body = $response->getBody();
			$stringBody = (string) $body;
			$mediaData = json_decode($stringBody);

			$mediaItemIDs[$index]['type'] = $mediaData->media_type;
			$mediaItemIDs[$index]['caption'] = ( isset($mediaData->caption) && $mediaData->caption !== '' ) ? $mediaData->caption : 'NO CAPTION';
			$mediaItemIDs[$index]['image'] = $mediaData->media_url;
			$mediaItemIDs[$index]['permalink'] = $mediaData->permalink;
		};
		$this->context['data'] = $mediaItemIDs;

		----------------------------------------------------------------------------
		*/

		$this->context['data'] = [
		    [
		        "id" => 17861517071526759,
		        "type" => "CAROUSEL_ALBUM",
		        "caption" => "",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/192728524_356403489151560_7860228639863739453_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=QD9Cz-rZqtcAX8YUylc&_nc_ht=scontent.cdninstagram.com&oh=0bbda101fe433387b93d65690a337818&oe=60BB8CB3",
		        "frames" => [
					[
					    "id" => 18229830796017665, // VIDEO id, thumbnail_url
					    "image" => "https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/192728524_356403489151560_7860228639863739453_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=AzZmKHVNsBgAX8nXcnm&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=a3d4b4e0a41d6395636360647f006571&oe=60B59DF3"
					],
					[
					    "id" => 17938464142518609, // IMAGE id,media_url
					    "image" => "https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/191792122_232840724847861_1377183305540598812_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=WkOO0u03DC4AX-jQxUy&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=bb114ba920a8f40eaaf0068ac0ed128a&oe=60B559DB"
					],
					[
					    "id" => 17977237948366637, // IMAGE id,media_url
					    "image" => "https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/192321188_295961532233078_3790839041121926828_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=wRtZ1gb0TTkAX9-L0N0&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=4731e3542d8e192d3553fa020ad955c3&oe=60B64F45"

					],
					[
					    "id" => 17894123657029402, // IMAGE id,media_url
					    "image" => "https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/192583229_199610755346951_9050409790656142366_n.jpg?_nc_cat=102&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=93HRt-ZzCW4AX8vKPJ9&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=1c7b9083452057cc401d2b2ec5b6b518&oe=60B68185"
					],
					[
					    "id" => 17905955392932271, // IMAGE id,media_url
					    "image" => "https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/192114689_176063431005075_7998080731118549313_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=b_wYA1maGogAX-iI1E7&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=64901d469c6f6cc81147e3bedeb5bf56&oe=60B5612E"
					],
					[
					    "id" => 18228407794018897, // IMAGE id,media_url
					    "image" => "https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/192120590_495248731794698_1827804719771551792_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=52CKrL3l1FcAX9NJ_vC&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=b4f54e438dcd8de6d22c2fb7674bde0c&oe=60B55C0D"
					],
					[
					    "id" => 17904709234910767, // IMAGE id,media_url
					    "image" => "https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/191854685_339424620852180_2026396240336927421_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=1v36kX1RrdsAX_JmO-4&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=44586ac2ae00de4eb75ca4be760bdb4c&oe=60B61DA8"
					]
		        ],
		        "permalink" => "https://www.instagram.com/p/CPaeTYDBmfy/"
		    ],
		    [
		        "id" => 17915381860686706,
		        "type" => "VIDEO",
		        "caption" => "Christmas Tree December 2020",
		        "image" => "https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/191451004_221212299548265_2322670811754055019_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=Dzd5O-ScHC4AX8uOMu5&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=af0b8b9afe1f2f17802ff50b59dbb034&oe=60B5263D",
		        "video" => "https://video-lhr8-2.cdninstagram.com/v/t50.2886-16/193333397_384404772890090_7416655170943917334_n.mp4?_nc_cat=101&vs=18176841895117807_1430010240&_nc_vs=HBksFQAYJEdKVUloZ3ZxSmZBMW5WMEJBQllkclRXVlFPMW1ia1lMQUFBRhUAAsgBABUAGCRHQWJZY3d0UFJtbC1relVCQUMwQ21vNnVZNVlNYmtZTEFBQUYVAgLIAQAoABgAGwGIB3VzZV9vaWwBMRUAACae\u00252F7Sgtq\u00252FaQBUCKAJDMywXQDRMzMzMzM0YEmRhc2hfYmFzZWxpbmVfMV92MREAdeoHAA\u00253D\u00253D&ccb=1-3&_nc_sid=59939d&efg=eyJ2ZW5jb2RlX3RhZyI6InZ0c192b2RfdXJsZ2VuLjcyMC5mZWVkIn0\u00253D&_nc_ohc=5eaS6s8_Mk0AX_GTziT&_nc_ht=video-lhr8-2.cdninstagram.com&oh=da0291986b3100e14417ecf7ca3da5f3&oe=60B3D48A&_nc_rid=07bc9f1481", // not accessible from outside instagram
		        "permalink" => "https://www.instagram.com/p/CPXyfUWhKoS/"
		    ],
		    [
		        "id" => 17985432520326796,
		        "type" => "IMAGE",
		        "caption" => "Fiat Panda gone",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/149665219_445249809954711_4397773684637739820_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=lHGDe_R8ntAAX_O_zRc&_nc_ht=scontent.cdninstagram.com&oh=df1c9f5017281e60946908e924daf81f&oe=60D417FB",
		        "permalink" => "https://www.instagram.com/p/CLO6erTlT9z/"
		    ],
		    [
		        "id" => 17890184875941629,
		        "type" => "IMAGE",
		        "caption" => "NO CAPTION",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/149506215_2793130024338301_8808800811403846173_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=qdZocLrBss0AX-cQisJ&_nc_ht=scontent.cdninstagram.com&oh=a28a687867644663345035a4aae0f61e&oe=60D19BEC",
		        "permalink" => "https://www.instagram.com/p/CLO6ZBNFzkl/"
		    ],
		    [
		        "id" => 17895609820878533,
		        "type" => "IMAGE",
		        "caption" => "NO CAPTION",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/149179760_2930393423916974_2419774539069920463_n.jpg?_nc_cat=111&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=PQUoQmlzlbgAX83Vsuh&_nc_ht=scontent.cdninstagram.com&oh=99702a23a4b18eba8ec712fd1ad08668&oe=60D0FE75",
		        "permalink" => "https://www.instagram.com/p/CLO6U71FOir/"
		    ],
		    [
		        "id" => 17898512758826299,
		        "type" => "IMAGE",
		        "caption" => "NO CAPTION",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/149133220_265008581731069_892919966359149008_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=qRvEzXLQ2joAX9ivkrX&_nc_oc=AQnP-i0jPwn30EM3NYRthP75qy_BPn7eqDyHkvkGqIkowf8_4kNBrwloebbUdwO0wY4&_nc_ht=scontent.cdninstagram.com&oh=4b5b3c58629c253825cdcad86e73a857&oe=60D1EF99",
		        "permalink" => "https://www.instagram.com/p/CLO6SSblTLq/"
		    ],
		    [
		        "id" => 17960092639383017,
		        "type" => "IMAGE",
		        "caption" => "NO CAPTION",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/148969513_2759148174399684_3171607463639536556_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=Qigcg7iqs2sAX_EykRf&_nc_ht=scontent.cdninstagram.com&oh=b2128042c2552c5ee800270e094a44c7&oe=60D19FF0",
		        "permalink" => "https://www.instagram.com/p/CLO6PxRlhOn/"
		    ],
		    [
		        "id" => 17873638487229313,
		        "type" => "IMAGE",
		        "caption" => "NO CAPTION",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/149133221_188992899640586_6598683140554637628_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=yH0YZMZ-OgEAX8hoarb&_nc_ht=scontent.cdninstagram.com&oh=dcf51e3665ae8ab4853e61b88dbcccf7&oe=60D26FD4",
		        "permalink" => "https://www.instagram.com/p/CLO6NZ8FgK2/"
		    ],
		    [
		        "id" => 18162172720100640,
		        "type" => "IMAGE",
		        "caption" => "NO CAPTION",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/148670121_788772181738857_5626866500692450621_n.jpg?_nc_cat=102&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=seGUAFdB3GQAX_u0_PO&_nc_ht=scontent.cdninstagram.com&oh=e758469199c0f98d3892342bf542693c&oe=60D1F451",
		        "permalink" => "https://www.instagram.com/p/CLO6KW-Fa_Y/"
		    ],
		    [
		        "id" => 17924974384500173,
		        "type" => "IMAGE",
		        "caption" => "NO CAPTION",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/148826060_2497328443905286_2320199363463991754_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=vmnxfHZ-8u4AX_c7j8r&_nc_ht=scontent.cdninstagram.com&oh=66faa452e577cf83b942f744162b0320&oe=60D3252C",
		        "permalink" => "https://www.instagram.com/p/CLO6FHyl7cC/"
		    ],
		    [
		        "id" => 17851685930179936,
		        "type" => "IMAGE",
		        "caption" => "Ayoade on top",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/116871484_699605157256169_5440671027769862796_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=trevCLUZZN4AX9mdpmU&_nc_ht=scontent.cdninstagram.com&oh=bb0405ddd9df7820a49d6ca378bbe455&oe=60D37CC6",
		        "permalink" => "https://www.instagram.com/p/CDjAcjXp4WF/"
		    ],
		    [
		        "id" => 17903207920509682,
		        "type" => "IMAGE",
		        "caption" => "Oxford Street December 2019",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/117234937_194807732008108_7795949777301609373_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=4yI5N1eD5iIAX-FZONl&_nc_ht=scontent.cdninstagram.com&oh=11e33e1d84839b65856028a26d13a4b1&oe=60D3C527",
		        "permalink" => "https://www.instagram.com/p/CDjAJb6pJc2/"
		    ],
		    [
		        "id" => 17961885631326457,
		        "type" => "IMAGE",
		        "caption" => "Christmas 2019",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/116767495_609892636336035_8084222242275392192_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=2wW3_vlByucAX-FQItL&_nc_ht=scontent.cdninstagram.com&oh=5610ea4e67b36d6df96a4663cdd266ec&oe=60D07A13",
		        "permalink" => "https://www.instagram.com/p/CDi__zaJrIu/"
		    ],
		    [
		        "id" => 17966108083322303,
		        "type" => "CAROUSEL_ALBUM",
		        "caption" => "Tm mod",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/116790135_344600903597650_5246089890279880146_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=QHIBTAzI9-UAX9OPh77&_nc_ht=scontent.cdninstagram.com&oh=981e6db24e45f2d09ab5c0f1cdea48c8&oe=60D1440C",
		        "frames" => [
					["id" => 17871002491888652,
					"image" => 'https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116790135_344600903597650_5246089890279880146_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=XdB75bflkDUAX9FUq-8&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=2d6bbcea45c6005fb55b138a825167c9&oe=60B39A4C'],
					["id" => 17856621797078139,
					"image" => 'https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116616825_112847837054437_7857259688487501295_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=ZQvzfYqOIh8AX_6YSTb&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=e2f5aeba1506286eeb2cc7cc51bb849e&oe=60B51533'],
					["id" => 17891248243603181,
					"image" => 'https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116738418_115981033339992_2134325237808623958_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=ZlkjIH18L3kAX-7D5sp&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=1717b2c0be8b12bf1dee8c275fa1a13f&oe=60B3DBF1'],
					["id" => 18096751444175225,
					"image" => 'https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116923093_318004679396048_289808457873665315_n.jpg?_nc_cat=102&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=DPt2pNMZlBQAX93BjHM&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=338a3e1dfa1f52d9e6c08b59f95a581a&oe=60B520EF'],
					["id" => 17873513626818918,
					"image" => 'https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116789123_319692359411362_8069066582701086126_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=ErBFGuirMJoAX-3ysmc&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=9503717090425fb4a5af3878ffed4529&oe=60B3ED53'],
					["id" => 17866190563957597,
					"image" => 'https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/117089347_193204648823338_4789956995636418535_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=PiKPLvDwU1YAX8PEZut&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=b6ac8ca73f393a3302fb0d2de928767f&oe=60B4A19A'],
					["id" => 17888216005624319,
					"image" => 'https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116598182_2645496275716769_8852233542887187206_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=D6DnLcThsrkAX9RfONo&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=e9cd6f6f881611141812e7da334173b1&oe=60B4163E'],
					["id" => 18074329234212488,
					"image" => 'https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116436384_2005572242908055_2702361083880087904_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=wpJFd0Q7gWoAX-VG34q&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=4353aefaecb2d7ba2c43cdd95e2aa909&oe=60B4FF15'],
					["id" => 18108454957155922,
					"image" => 'https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116911455_3070269109675166_177887185391654867_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=JiTLqk34JssAX-fpc5n&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=2dd666960c8fc7d666d6559257b42619&oe=60B48334'],
					["id" => 17936507368380857,
					"image" => 'https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/117232085_777503722993427_8350882245015089926_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=YUk7gTzxZC8AX8MYtE_&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=cfa68d76351e4763a8faf198e1a1a1e1&oe=60B39B19']
		        ],
		        "permalink" => "https://www.instagram.com/p/CDbYBmKJVAt/"
		    ],
		    [
		        "id" => 17881585063725098,
		        "type" => "IMAGE",
		        "caption" => "Green man",
		        "image" => "https://scontent.cdninstagram.com/v/t51.29350-15/117115033_117951646454727_4347349911753455026_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=XBj9DIDYFn0AX9VTCmh&_nc_ht=scontent.cdninstagram.com&oh=829e7ffc8cba3f94f82f101880bca064&oe=60D24DC4",
		        "permalink" => "https://www.instagram.com/p/CDbXyG9JpVM/"
		    ]
		];

		/*
		----------------------------------------------------------------------------

		// child image media ids of Tm mod CAROUSEL_ALBUM 17966108083322303
		$children = [
			["id" => 17871002491888652,
			"image" => https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116790135_344600903597650_5246089890279880146_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=XdB75bflkDUAX9FUq-8&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=2d6bbcea45c6005fb55b138a825167c9&oe=60B39A4C],
			["id" => 17856621797078139,
			"image" => https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116616825_112847837054437_7857259688487501295_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=ZQvzfYqOIh8AX_6YSTb&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=e2f5aeba1506286eeb2cc7cc51bb849e&oe=60B51533],
			["id" => 17891248243603181,
			"image" => https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116738418_115981033339992_2134325237808623958_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=ZlkjIH18L3kAX-7D5sp&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=1717b2c0be8b12bf1dee8c275fa1a13f&oe=60B3DBF1],
			["id" => 18096751444175225,
			"image" => https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116923093_318004679396048_289808457873665315_n.jpg?_nc_cat=102&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=DPt2pNMZlBQAX93BjHM&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=338a3e1dfa1f52d9e6c08b59f95a581a&oe=60B520EF],
			["id" => 17873513626818918,
			"image" => https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116789123_319692359411362_8069066582701086126_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=ErBFGuirMJoAX-3ysmc&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=9503717090425fb4a5af3878ffed4529&oe=60B3ED53],
			["id" => 17866190563957597,
			"image" => https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/117089347_193204648823338_4789956995636418535_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=PiKPLvDwU1YAX8PEZut&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=b6ac8ca73f393a3302fb0d2de928767f&oe=60B4A19A],
			["id" => 17888216005624319,
			"image" => https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/116598182_2645496275716769_8852233542887187206_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=D6DnLcThsrkAX9RfONo&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=e9cd6f6f881611141812e7da334173b1&oe=60B4163E],
			["id" => 18074329234212488,
			"image" => https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116436384_2005572242908055_2702361083880087904_n.jpg?_nc_cat=105&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=wpJFd0Q7gWoAX-VG34q&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=4353aefaecb2d7ba2c43cdd95e2aa909&oe=60B4FF15],
			["id" => 18108454957155922,
			"image" => https://scontent-lhr8-2.cdninstagram.com/v/t51.29350-15/116911455_3070269109675166_177887185391654867_n.jpg?_nc_cat=101&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=JiTLqk34JssAX-fpc5n&_nc_ht=scontent-lhr8-2.cdninstagram.com&oh=2dd666960c8fc7d666d6559257b42619&oe=60B48334],
			["id" => 17936507368380857,
			"image" => https://scontent-lhr8-1.cdninstagram.com/v/t51.29350-15/117232085_777503722993427_8350882245015089926_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=8ae9d6&_nc_ohc=YUk7gTzxZC8AX8MYtE_&_nc_ht=scontent-lhr8-1.cdninstagram.com&oh=cfa68d76351e4763a8faf198e1a1a1e1&oe=60B39B19]
		];

		$children = [
			[
				"id" => "18229830796017665"
			],
			[
				"id" => "18000019144328211"
			],
			[
				"id" => "17938464142518609"
			],
			[
				"id" => "17977237948366637"
			],
			[
				"id" => "17894123657029402"
			],
			[
				"id" => "17905955392932271"
			],
			[
				"id" => "18228407794018897"
			],
			[
				"id" => "17904709234910767"
			]
		];

		// iterate over child image ids to get media_url for each
		foreach($children AS $index => $item){
			try {
				$response = $client->request('GET', $item['id'], [
					'query' => [
				    	'fields' => 'id,media_type,media_url',
				    	'access_token' => $accessToken
				    ]
				]);
			} catch (Exception $e) {
				// request error
				// return empty set to test page
		    	$this->context['data'] = [];
		    	return $this->show('misc.instacheck', $request);
			}
	
			$body = $response->getBody();
			$stringBody = (string) $body;
			$mediaData = json_decode($stringBody);

			$children[$index]['image'] = $mediaData->media_url;
		};

		echo "<pre>".print_r($children,true)."</pre>";
		die();

		----------------------------------------------------------------------------
		*/

		$this->context['alpha'] = 'alpha';
		return $this->show('misc.instacheck', $request);
	}

	public function contact(Request $request)
	{
		// $this->context['titlesArr'] = ['' => 'Select', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];
		
		$url = 'misc.contact';
		$this->context['pageViewJS'] = 'pages/contactForm.min';
		return $this->show($url, $request);
	}

	public function contact_submit(SupportRequest $request, Support $support)
	{

		// if (App::environment() != 'local') {

			// Prepare the email to send.
			$details = array(
				'title'			=> $request->title,
				'firstname'		=> $request->firstname,
				'lastname'		=> $request->lastname,
				'email'			=> $request->email,
				'company'		=> $request->company,
				'address'		=> $request->address,
				'address2'		=> $request->address2,
				'city'			=> $request->city,
				'postcode'		=> $request->postcode,
				'country'		=> $request->country, // select list
				'phone'			=> $request->phone,
				'reason'		=> $request->reason,
				'privacy'		=> $request->privacy,
				'brand'			=> 'femfresh'
			);

			// handle conditional fields
			// country 'Other'
			if ($request->country === 'other' ) {
				$details['country'] = $request->country_other;
			}

			// reason for enquiry
			switch( $request->reason ) {
				case 'product' : {
					$details['product'] = $request->p_p1;
					$details['comments'] = $request->p_p2;
				} break;
				case 'company' : {
					$details['department'] = $request->c_p1;
					$details['comments'] = $request->c_p2;
				} break;
				case 'complaint' : {
					$details['product'] = $request->copx;
					$details['barcode'] = $request->co_p1;
					$details['batchcode'] = $request->co_p2;
					$details['expirydate'] = $request->co_p3;
					$details['comments'] = $request->co_p4;
				} break;
			}

			// save form data into db 'femfresh_2018.supports'
			$support->fill( $details );
			$support->save();

			Mail::to(Config('mail.mail-sys-to'))->send(new SysMail($details));
			
			// $mail = Mail::to(Config('mail.mail-admin-to'));
			
			// if (Config('mail.mail-admin-cc') != "") {
			// 	$mail->cc(Config('mail.mail-admin-cc'));
			// }
			// if (Config('mail.mail-admin-bcc') != "") {
			// 	$mail->bcc(Config('mail.mail-admin-bcc'));
			// }
			// $mail->send(new Enquiry($details));
		// }

		$this->response = array('response_status' => 'success', 'message' => 'Thank you for contacting femfresh');

		return redirect()->route('contact')->with('response', $this->response);
	}

	public function search(Request $request) {
		DB::enableQueryLog();
		$results = null;
		if ($request->terms) {
			$terms = urldecode($request->terms);
			
			$results = Page::search($terms)->get();
		}
		dd($results);
	}


	private function show($url, $request)
	{
		if ($request->forget) {
			$this->forgetCache();
		}

		$cache = $this->checkCache($request->preview);
		if (!$cache) {
			
			if ($request->preview) {
				$this->context['nocache'] = true;
			}

			$cache = view('main.'.$url, $this->context)->render();

			if (!$request->preview) {
				$this->saveCache($cache);
			} else {
				$this->forgetCache();
			}
		}

		// $cache = view('main.'.$url, $this->context)->render();
		return $cache;
	}
}
