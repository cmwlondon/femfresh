<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Services\SitemapPackager;
use App\Models\Page;
use Symfony\Component\DomCrawler\Crawler;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application sitemap screen to the user.
	 *
	 * @return Response
	 */
	public function sitemap(SitemapPackager $packager)
	{	
		$pkg_response = $packager->create(); // Update the Sitemap		
		return $pkg_response;
	}

	/**
	 * Flush the cache
	 *
	 * @return Response
	 */
	public function flush()
	{	
		return $this->clearCache();
	}


	/**
	 * Flush the database store of page raw text
	 *
	 * @return Response
	 */
	public function purge() {
		$meta = config('meta');
        foreach($meta as $slug => $page) {
            if ($page['indexable'] == true) {
	            $details = $this->fetchPageDetails($slug,$meta);
	            $page = Page::firstOrCreate(
				    ['url' => $details['url']]
				);
				$page->meta_title = $details['meta_title'];
				$page->meta_description = $details['meta_description'];
				$page->rawtext = $details['rawtext'];
				$page->save();
			}
        }
        return 'Regenerated raw text for RAKE';
	}

	private function fetchPageDetails($slug,$meta) {
		$details = [
			'url' => $slug,
			'meta_title' => $meta[$slug]['title'],
			'meta_description' => $meta[$slug]['desc'],
			'rawtext' => ''
		];
		$slug = ($slug == '/') ? '' : $slug;

		$client = new \GuzzleHttp\Client([ 'verify' => false ]);
		$url = (env('APP_URL', url(''))) . '/' . $slug.'?forget=true';
		$response = $client->request('GET', $url);
		
		if ($response->getStatusCode() == 200) {

			$crawler = new Crawler($response->getBody()->getContents());
			$crawler = $crawler->filter('#page');
			$stripped = strip_tags(str_replace(["&nbsp;","<br/>","<br>","&amp;"],[" "," "," ","&"],$crawler->html()));

			$mixed_search = ["‘","’","…","...",":","\"","“","”","·"];
			$mixed_replace = ["","","","","","","","",""];

			$stripped = str_replace($mixed_search,$mixed_replace, $stripped);
			$details['rawtext'] = $stripped;
		}

		return $details;
	}
}
