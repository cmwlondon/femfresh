<?php namespace App\Http\Controllers\Api;

use App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use App\Mail\DrSaraMail;

use Illuminate\Http\JsonResponse;
use App\Models\QuizEntries;

class ApiController extends Controller {

	public function __construct() {
		
	}

	public function question(Request $request)
	{	
		// Prepare the email to send.
		$details = array(
						'question'			=> $request->input('question')
					);
		//if (App::environment() != 'local') {
			Mail::to(Config('mail.mail-admin-to'))->send(new DrSaraMail($details));
		//}

		$output = array('response_status' => 'success', 'message' => 'Thank you for submitting your question.');

		return new JsonResponse($output, 200);
	}


	public function quiz(Request $request, QuizEntries $quizentries)
	{	
		$quizentries->fill($request->all());
		$quizentries->save();

		$output = array('response_status' => 'success', 'message' => '');

		return new JsonResponse($output, 200);
	}
}
