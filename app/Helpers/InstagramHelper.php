<?php

namespace app\Helpers;

use App\Helpers\Contracts\InstagramContract;

use App\Models\Instagram as InstagramModel;

use GuzzleHttp\Client;
use Tinify;
use Illuminate\Support\Facades\Storage;
// use Vinkla\Instagram\Instagram;

use Illuminate\Support\Facades\Cache;

class InstagramHelper implements InstagramContract
{
    public function getReady() {

   		//$cache_id = 'ff_instagram';
		//if (Cache::has($cache_id)) {
		// 	return cache($cache_id);
		//} else {
    		// get 'ready' posts from instagram table 
    		$ready = InstagramModel::where('state', 'ready')->latest()->limit(20)->get();
    		//cache([$cache_id => $ready], 30);

    		return $ready;
    	//}
    }

    public function fetch()
    {	

    	// vinkla/instagram endpoint closed 29/7/2020

		// Get most recent Image from the database
		// use this as cutoff for items in instagram timeline
		$latest = InstagramModel::orderBy('created_time', 'desc')
           ->take(1)
           ->get()
           ->first();

        if ($latest == null) {
        	$last_time = 0;
        } else {
        	$last_time = $latest->created_time;
        }

	   	// get instagram posts for appropriate user account 'femfresh_uk' (https://www.instagram.com/femfresh_uk/) in JSON format
	   	// needless to say, if Instagram change their access policies or JSON format at some point in the future this will need to be updated
	   	$response = file_get_contents('https://www.instagram.com/femfresh_uk/');

	   	/*
	   	REGEX HTML from instagram to find _sharedData variable
		<script type="text\/javascript">window[.]_sharedData = {[\s\S]*};<\/script>	   	
	   	*/

		preg_match('/<script type="text\/javascript">window[.]_sharedData = (\{[\s\S]*\});<\/script>/', $response, $matches);

		$media = json_decode($matches[1]); 
		$userdata = $media->entry_data->ProfilePage[0]->graphql->user;

		if( count($userdata->edge_owner_to_timeline_media->edges) > 0 )
		{
			foreach( $userdata->edge_owner_to_timeline_media->edges AS $post) {
				// if post is newer than latest post in db, insert into db
	   			if ($last_time < $post->node->taken_at_timestamp) {

		   			$image = new InstagramModel;
		   			$image->id_str = $post->node->id.'_5720822920'; // [item id]_[user id]
		   			$image->code = ''; // $item->code;
		   			$image->media_remote = $post->node->thumbnail_src;
		   			$image->media_local = '';
		   			$image->black = false;
		   			$image->state = 'pending';
		   			$image->link = 'https://www.instagram.com/p/'.$post->node->shortcode;
		   			$image->type = 'image'; // not sure where to find the post type in the JSON format so default to 'image' for the moment
		   			$image->created_time = $post->node->taken_at_timestamp;

		   			$image->save();
	   			}
			}

		}

		// process FIRST 'pending' item in 'instagrams' table
		return $this->process();
    }

    /* --------------- ERROR in test() */

    public function test()
    {	
	   	// get instagram posts for appropriate user account 'femfresh_uk'
    	// https://www.instagram.com/femfresh_uk/


	   	/*
	   	// changes to instagram API mean the returned HTML does not include the relevant JSON data
		   	$response = file_get_contents('https://www.instagram.com/femfresh_uk/');
		   	// the rest of the code relies on the JSON data being present
		   	// currently errors

		   	// REGEX HTML from instagram to find _sharedData variable
			// <script type="text\/javascript">window[.]_sharedData = {[\s\S]*};<\/script>	   	

			preg_match('/<script type="text\/javascript">window[.]_sharedData = (\{[\s\S]*\});<\/script>/', $response, $matches);

			$media = json_decode($matches[1]); 
			$userdata = $media->entry_data->ProfilePage[0]->graphql->user;

			$posts = [];
			foreach( $userdata->edge_owner_to_timeline_media->edges AS $post) {
				$posts[] = [
					'id' => $post->node->id,
					'video' => ($post->node->is_video) ? 'yes' : 'no',
					'thumbnail' => $post->node->thumbnail_src,
					'caption' => $post->node->accessibility_caption,
					'timestamp' => $post->node->taken_at_timestamp,
					'url' => $post->node->shortcode
				];
			};
		   	// return $posts;
		*/

		// manually pull JSON data from femfresh instagram page and store in json file: /storage/app/public/instagram.js
		// ** ONLY CONTAINS LATEST 12 POSTS **
		// not working - latest items retreived but thumbnail image URLs contain a timestamp which renders them useless
    	$server_path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
	   	$response = file_get_contents($server_path.'instagram.js');
		$media = json_decode($response); 
		$userdata = $media->entry_data->ProfilePage[0]->graphql->user;

		// echo "<pre>".print_r($userdata, true)."</pre>";
		// die();

		$posts = [];
		foreach( $userdata->edge_owner_to_timeline_media->edges AS $post) {
			$posts[] = [
				'id' => $post->node->id,
				'video' => ($post->node->is_video) ? 'yes' : 'no',
				'thumbnail' => $post->node->thumbnail_src,
				'caption' => $post->node->accessibility_caption,
				'timestamp' => $post->node->taken_at_timestamp,
				'url' => $post->node->shortcode
			];
		};

	   	return $posts;
    }

    /* --------------- */

    private function process() {
    	$server_path = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
    	$pending = InstagramModel::where('state', 'pending')->get();

    	if ( count($pending) > 0 ) {
	    	foreach($pending as $image) {
	    		if ($image->type == "video") {
	   				$image->black = $this->check_if_black($image->media_remote);
	   			}

	   			$image->media_local = $image->id_str . ".jpg";

	   			$source = Tinify::fromUrl($image->media_remote);
				$source->resize(["method" => "cover", "width" => 320, "height" => 320])->toFile($server_path . 'instagram/small_' . $image->media_local);
				$source->resize(["method" => "cover", "width" => 480, "height" => 480])->toFile($server_path . 'instagram/medium_' . $image->media_local);
				$source->resize(["method" => "cover", "width" => 640, "height" => 640])->toFile($server_path . 'instagram/large_' . $image->media_local);

				$image->state = 'ready';
				$image->save();
				return $image->id_str.' / '.$image->media_remote;
	    	}
    	} else {
    		return 'none';
    	}
    }

    private function check_if_black($src){

	    $img = imagecreatefromjpeg($src); // 403 error here 

	    list($width_orig, $height_orig)=getimagesize($src);
	    for($i=0;$i<20;$i++){
	        $rand_width=rand ( 0 , $width_orig-1 );
	        $rand_height=rand ( 0 , $height_orig-1 );
	        $rgb = imagecolorat($img, $rand_width, $rand_height);
	        if($rgb!=0){
	            return false;
	        }
	    }
	    return true;
	}   
}
