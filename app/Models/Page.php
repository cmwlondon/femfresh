<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Page extends Model
{
    protected $fillable = [
        'url','meta_title','meta_description','rawtext'
    ];

    /**
     * Scope search
     */
    public function scopeSearch($query,$terms)
    {
        $terms = strtolower($terms);
        $len = strlen($terms);
        $query->select(DB::raw("
          `url`,
          `meta_title`,
          `meta_description`,
          (
			((length(LOWER(`rawtext`))-length(replace(LOWER(`rawtext`) ,'".$terms."','')))/".$len.") + 
      ((length(LOWER(`url`))-length(replace(LOWER(`url`) ,'".$terms."','')))/".$len.") + 
			((length(LOWER(`meta_title`))-length(replace(LOWER(`meta_title`) ,'".$terms."','')))/".$len.") + 
			((length(LOWER(`meta_description`))-length(replace(LOWER(`meta_description`) ,'".$terms."','')))/".$len.")

			) as `COUNT`
        "));
        $query->whereRaw("
        	LOWER(`rawtext`) like '%".$terms."%'
        	OR LOWER(`meta_title`) like '%".$terms."%'
        	OR LOWER(`meta_description`) like '%".$terms."%'
        	");
        $query->orderby('COUNT','desc');



        return $query;
    }
}
