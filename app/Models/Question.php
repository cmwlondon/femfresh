<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'drsaraquestions';


    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','email','question','terms','brand'];

}
