<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    protected $fillable = ['id_str','code','media_remote','media_local','state','link','type','created_time'];
}
