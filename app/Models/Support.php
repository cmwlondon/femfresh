<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Support extends Model {

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supports';


    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title','firstname','lastname','company','address','address2','city','postcode','country','phone','email','reason','product','comments','department','barcode','batchcode','expirydate','privacy','brand'];

}
